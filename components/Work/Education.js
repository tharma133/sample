/* eslint-disable @next/next/no-img-element */
import React, { useEffect } from 'react'
import PageLinks from '../PageLinks'
import styles from '../../styles/Work.module.css'
import Video from '../Video'
import { work_links } from '../../utils/data'
import AOS from 'aos'
import 'aos/dist/aos.css'
import Footer from '../Footer'
import ImageOutlinedIcon from '@material-ui/icons/ImageOutlined'
import CheckCircleOutlineOutlinedIcon from '@material-ui/icons/CheckCircleOutlineOutlined'

function Education() {
  useEffect(() => {
    // setTimeout(() => {
    AOS.init({ duration: 500, easing: 'linear' })
    // }, 500)
  }, [])

  return (
    <section className={styles.work}>
      <div className={styles.work_container}>
        <div
          className={styles.work_left}
          data-aos-delay='500'
          data-aos='zoom-in'
        >
          <Video />
          <div
            className={styles.work_bottom_content}
            data-aos='fade-up'
            data-aos-anchor-placement='bottom bottom'
            data-aos-delay='1000'
          >
            <div className={styles.work_text}>
              <h2>Education</h2>
              <h4>
                In this day and age with Zettabytes of data generated every day,
                skipping Digital transformation is no longer an option
              </h4>
            </div>
          </div>
        </div>
        <PageLinks links={work_links} />

        <div className='svg_14'>
          <img src='/svg/svg14.svg' alt='mobile' />
        </div>
      </div>
      <div className={styles.edu}>
        <div>
          <h2 data-aos='fade-up'>edu vr</h2>
          <p data-aos='fade-up'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore
            voluptate, obcaecati maiores voluptatum suscipit voluptas?
            Voluptatem tempora voluptates ratione aliquid, molestias maxime
            voluptatum esse officia hic atque, ea, aliquam reiciendis. Accusamus
            beatae harum nesciunt quia nam fuga autem iure similique hic
            accusantium eum, commodi labore exercitationem est in dolore magnam?
          </p>
        </div>
      </div>
      <div className={styles.education_images}>
        <div className={styles.education_images_container}>
          <div className={styles.education_images_top} data-aos='fade-up'>
            <div className={styles.education_icons}>
              <ImageOutlinedIcon />
              <h3>what</h3>
            </div>
            <div className={styles.education_icons}>
              <ImageOutlinedIcon />
              <h3>How</h3>
            </div>
            <div className={styles.education_icons}>
              <ImageOutlinedIcon />
              <h3>where</h3>
            </div>
            <div className={styles.education_icons}>
              <ImageOutlinedIcon />
              <h3>why</h3>
            </div>
          </div>
          <div className={styles.education_images_bottom} data-aos='fade-up'>
            <div className={styles.education_bottom_image}>
              <ImageOutlinedIcon />
            </div>
            <div className={styles.education_bottom_content}>
              <h2>impact</h2>
              <div className={styles.education_bottom_content_text}>
                <div className={styles.education_bottom_text}>
                  <CheckCircleOutlineOutlinedIcon />
                  <h3>Here goes feature number 1</h3>
                </div>
                <div className={styles.education_bottom_text}>
                  <CheckCircleOutlineOutlinedIcon />
                  <h3>Here goes feature number 2</h3>
                </div>
                <div className={styles.education_bottom_text}>
                  <CheckCircleOutlineOutlinedIcon />
                  <h3>Here goes feature number 3</h3>
                </div>
              </div>
              <div className={styles.btn}>
                <button>button</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </section>
  )
}

export default Education
