import React from 'react'
import LinkedInIcon from '@material-ui/icons/LinkedIn'
import FacebookIcon from '@material-ui/icons/Facebook'
import TwitterIcon from '@material-ui/icons/Twitter'
import InstagramIcon from '@material-ui/icons/Instagram'

function Footer() {
  return (
    <section className='footer'>
      <div className='footer_container'>
        <div className='footer_company'>
          <h3>company</h3>
          <ul>
            <li>About</li>
            <li>Services</li>
            <li>Products</li>
            <li>Careers</li>
            <li>Blog</li>
          </ul>
        </div>
        <div className='footer_services'>
          <h3>services</h3>
          <ul>
            <li>AR/ VR/ MR</li>
            <li>Computer vision</li>
            <li>enterprises solution</li>
            <li>mobility</li>
            <li>More</li>
          </ul>
        </div>
        <div className='footer_others'>
          <div className='footer_get_in_touch'>
            <h3>Get in Touch</h3>
            <div className='footer_social_links'>
              <h3>Follow Us on</h3>
              <div className='footer_social_icons'>
                <button>
                  <FacebookIcon />{' '}
                </button>
                <button>
                  <InstagramIcon />{' '}
                </button>
                <button>
                  <LinkedInIcon />{' '}
                </button>
                <button>
                  <TwitterIcon />{' '}
                </button>
              </div>
            </div>
          </div>
          <div className='footer_links'>
            <ul>
              <li>home</li>
              <li>about</li>
              <li>services</li>
              <li>contact</li>
              <li>terms &#38; condition</li>
              <li>privacy policy</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Footer
