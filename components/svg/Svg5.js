import React from 'react'

function Svg5() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='138.86'
      height='100.271'
      viewBox='0 0 138.86 181.166'
    >
      <g
        id='Group_5235'
        data-name='Group 5235'
        transform='translate(-12.364 -173.077)'
      >
        <g
          id='Group_5178'
          data-name='Group 5178'
          transform='translate(12.364 173.077)'
        >
          <g id='Group_5074' data-name='Group 5074'>
            <path
              id='Path_9471'
              data-name='Path 9471'
              d='M96.742,304.886,38.489,338.519V221.764l58.254-33.633Z'
              transform='translate(0.571 -165.623)'
              fill='#bcbcbc'
            />
            <path
              id='Path_9472'
              data-name='Path 9472'
              d='M94.486,302.109l-54.7,31.58V221.8l54.7-31.58Z'
              transform='translate(1.214 -164.59)'
              fill='#6f90e2'
            />
            <path
              id='Path_9473'
              data-name='Path 9473'
              d='M12.364,195.544V312.3l39.059,22.551V218.095Z'
              transform='translate(-12.364 -161.953)'
              fill='#d3d3d3'
            />
            <path
              id='Path_9474'
              data-name='Path 9474'
              d='M18.681,201.815l-4.763-2.75V306.891l4.763,2.75Z'
              transform='translate(-11.595 -160.21)'
              fill='#9b9b9b'
            />
            <path
              id='Path_9475'
              data-name='Path 9475'
              d='M23.275,204.429l-4.763-2.75V309.505l4.763,2.75Z'
              transform='translate(-9.32 -158.916)'
              fill='#9b9b9b'
            />
            <path
              id='Path_9476'
              data-name='Path 9476'
              d='M27.924,206.988l-4.763-2.75v108.1l4.763,2.75Z'
              transform='translate(-7.018 -157.649)'
              fill='#9b9b9b'
            />
            <path
              id='Path_9477'
              data-name='Path 9477'
              d='M70.617,173.077l-58.254,33.59,39.059,22.551,58.254-33.633Z'
              transform='translate(-12.364 -173.077)'
              fill='#cdcdcd'
            />
            <path
              id='Path_9478'
              data-name='Path 9478'
              d='M69.464,174.416l-54.8,31.616,35.584,20.545,54.8-31.655Z'
              transform='translate(-11.226 -172.414)'
              fill='#fff'
            />
          </g>
          <g
            id='Group_5093'
            data-name='Group 5093'
            transform='translate(41.002 25.63)'
          >
            <g
              id='Group_5075'
              data-name='Group 5075'
              transform='translate(2.061 3.698)'
            >
              <path
                id='Path_9479'
                data-name='Path 9479'
                d='M80.5,192.693l-16.25,9.382,16.25,9.364Z'
                transform='translate(-29.74 -192.693)'
                fill='#507cd3'
              />
              <path
                id='Path_9480'
                data-name='Path 9480'
                d='M41.167,237.667l50.759-29.305v-.029l-16.25-9.364L41.167,218.891Z'
                transform='translate(-41.167 -189.586)'
                fill='#8fa6ef'
              />
            </g>
            <g id='Group_5092' data-name='Group 5092'>
              <g
                id='Group_5079'
                data-name='Group 5079'
                transform='translate(36.03 6.541)'
              >
                <g
                  id='Group_5076'
                  data-name='Group 5076'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9481'
                    data-name='Path 9481'
                    d='M73.6,209.437l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-66.827 -194.178)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9482'
                    data-name='Path 9482'
                    d='M68.5,196.325l-.079.045V207.9l4.8,2.8v-14l-2.61-1.522C69.891,195.576,69.163,195.948,68.5,196.325Z'
                    transform='translate(-68.417 -194.305)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9483'
                    data-name='Path 9483'
                    d='M72.545,194.733l-1.235-.139c-.454.315-.939.6-1.428.872l2.61,1.522,1.972-1.139Z'
                    transform='translate(-67.692 -194.594)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5077'
                  data-name='Group 5077'
                  transform='translate(3.386 1.672)'
                >
                  <path
                    id='Path_9484'
                    data-name='Path 9484'
                    d='M71.335,210.752l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-64.562 -195.199)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9485'
                    data-name='Path 9485'
                    d='M66.152,197.542v11.693l4.8,2.8v-14l-2.7-1.576C67.562,196.838,66.867,197.208,66.152,197.542Z'
                    transform='translate(-66.152 -195.343)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9486'
                    data-name='Path 9486'
                    d='M68.922,196.06q-.684.388-1.365.765l2.7,1.576,1.972-1.139-2.667-1.55C69.345,195.829,69.128,195.944,68.922,196.06Z'
                    transform='translate(-65.456 -195.712)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5078'
                  data-name='Group 5078'
                  transform='translate(0 3.572)'
                >
                  <path
                    id='Path_9487'
                    data-name='Path 9487'
                    d='M69.071,212.068,67.1,213.207v-14l1.973-1.139Z'
                    transform='translate(-62.297 -196.447)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9488'
                    data-name='Path 9488'
                    d='M63.887,198.057V210.62l4.8,2.8v-14l-3.065-1.787A17.547,17.547,0,0,1,63.887,198.057Z'
                    transform='translate(-63.887 -196.662)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9489'
                    data-name='Path 9489'
                    d='M65.453,197.817c-.13.049-.268.091-.405.135l3.065,1.787,1.972-1.138L67.3,196.983C66.7,197.284,66.089,197.571,65.453,197.817Z'
                    transform='translate(-63.312 -196.983)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5083'
                data-name='Group 5083'
                transform='translate(23.966 12.747)'
              >
                <g
                  id='Group_5080'
                  data-name='Group 5080'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9490'
                    data-name='Path 9490'
                    d='M65.531,214.12l-1.972,1.139v-14l1.972-1.139Z'
                    transform='translate(-58.758 -198.066)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9491'
                    data-name='Path 9491'
                    d='M61.765,199.558c-.463.286-.936.565-1.417.833v12.343l4.8,2.8v-14Z'
                    transform='translate(-60.348 -198.342)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9492'
                    data-name='Path 9492'
                    d='M63.126,198.745c-.579.42-1.2.824-1.83,1.216l3.383,1.974,1.972-1.139Z'
                    transform='translate(-59.879 -198.745)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5081'
                  data-name='Group 5081'
                  transform='translate(3.386 2.048)'
                >
                  <path
                    id='Path_9493'
                    data-name='Path 9493'
                    d='M63.267,215.435l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-56.493 -199.463)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9494'
                    data-name='Path 9494'
                    d='M58.083,201.462v12.607l4.8,2.8v-14l-3.478-2.029Q58.74,201.164,58.083,201.462Z'
                    transform='translate(-58.083 -199.758)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9495'
                    data-name='Path 9495'
                    d='M58.968,201.194l3.478,2.029,1.972-1.139-3.386-1.969C60.349,200.5,59.659,200.855,58.968,201.194Z'
                    transform='translate(-57.645 -200.115)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5082'
                  data-name='Group 5082'
                  transform='translate(0 3.871)'
                >
                  <path
                    id='Path_9496'
                    data-name='Path 9496'
                    d='M61,216.751l-1.973,1.139v-14L61,202.748Z'
                    transform='translate(-54.228 -200.634)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9497'
                    data-name='Path 9497'
                    d='M55.818,203.182v12.2l4.8,2.8v-14l-3.47-2.024C56.721,202.483,56.275,202.835,55.818,203.182Z'
                    transform='translate(-55.818 -200.927)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9498'
                    data-name='Path 9498'
                    d='M58.507,201.337c-.031.015-.063.025-.094.04-.262.172-.52.344-.783.519-.3.2-.606.426-.921.667l3.47,2.024,1.972-1.139-3.636-2.114Z'
                    transform='translate(-55.377 -201.334)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5087'
                data-name='Group 5087'
                transform='translate(11.845 20.135)'
              >
                <g
                  id='Group_5084'
                  data-name='Group 5084'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9499'
                    data-name='Path 9499'
                    d='M57.424,219l-1.973,1.139v-14L57.424,205Z'
                    transform='translate(-50.652 -203.038)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9500'
                    data-name='Path 9500'
                    d='M53.853,204.516q-.789.491-1.612.928v12.134l4.8,2.8v-14Z'
                    transform='translate(-52.241 -203.277)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9501'
                    data-name='Path 9501'
                    d='M55.109,203.687a19.948,19.948,0,0,1-1.79,1.239l3.188,1.86,1.974-1.139Z'
                    transform='translate(-51.707 -203.687)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5085'
                  data-name='Group 5085'
                  transform='translate(3.386 2.068)'
                >
                  <path
                    id='Path_9502'
                    data-name='Path 9502'
                    d='M55.159,220.317l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-48.387 -204.454)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9503'
                    data-name='Path 9503'
                    d='M51.118,205.956c-.38.182-.761.378-1.142.574v12.388l4.8,2.8v-14l-3.31-1.932Z'
                    transform='translate(-49.976 -204.717)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9504'
                    data-name='Path 9504'
                    d='M50.972,206.137l3.31,1.932,1.974-1.139-3.2-1.86C52.377,205.438,51.684,205.791,50.972,206.137Z'
                    transform='translate(-49.483 -205.07)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5086'
                  data-name='Group 5086'
                  transform='translate(0 3.902)'
                >
                  <path
                    id='Path_9505'
                    data-name='Path 9505'
                    d='M52.894,221.633l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-46.122 -205.637)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9506'
                    data-name='Path 9506'
                    d='M47.711,207.21V220.3l4.8,2.8v-14l-3.653-2.132A5.743,5.743,0,0,1,47.711,207.21Z'
                    transform='translate(-47.711 -205.966)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9507'
                    data-name='Path 9507'
                    d='M48.521,207.285c-.013.006-.03.006-.043.012l3.653,2.132L54.1,208.29,50.677,206.3A18.847,18.847,0,0,1,48.521,207.285Z'
                    transform='translate(-47.331 -206.297)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5091'
                data-name='Group 5091'
                transform='translate(2.024 26.769)'
              >
                <g
                  id='Group_5088'
                  data-name='Group 5088'
                  transform='translate(4.53)'
                >
                  <path
                    id='Path_9508'
                    data-name='Path 9508'
                    d='M49.355,223.685l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-42.583 -207.353)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9509'
                    data-name='Path 9509'
                    d='M45.5,209.087c-.425.339-.87.655-1.325.951v12.279l4.8,2.8v-14Z'
                    transform='translate(-44.172 -207.647)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9510'
                    data-name='Path 9510'
                    d='M46.5,208.124a8.582,8.582,0,0,1-1.441,1.44l3.475,2.027,1.974-1.139Z'
                    transform='translate(-43.733 -208.124)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5089'
                  data-name='Group 5089'
                  transform='translate(1.144 2.356)'
                >
                  <path
                    id='Path_9511'
                    data-name='Path 9511'
                    d='M47.09,225l-1.973,1.139v-14L47.09,211Z'
                    transform='translate(-40.318 -209.058)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9512'
                    data-name='Path 9512'
                    d='M41.907,211.208v12.385l4.8,2.8v-14l-3.265-1.906A9.09,9.09,0,0,1,41.907,211.208Z'
                    transform='translate(-41.907 -209.312)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9513'
                    data-name='Path 9513'
                    d='M43.313,210.652c-.123.075-.253.148-.38.22l3.265,1.906,1.974-1.139L44.836,209.7C44.325,210.035,43.808,210.35,43.313,210.652Z'
                    transform='translate(-41.399 -209.7)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5090'
                  data-name='Group 5090'
                  transform='translate(0 4.269)'
                >
                  <path
                    id='Path_9514'
                    data-name='Path 9514'
                    d='M44.825,226.316l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-40.295 -210.319)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9515'
                    data-name='Path 9515'
                    d='M41.142,212.077v14l2.557,1.492v-14Z'
                    transform='translate(-41.142 -210.436)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9516'
                    data-name='Path 9516'
                    d='M41.558,211.161a1.818,1.818,0,0,1-.416.007v1.452l2.557,1.492,1.974-1.139-3.43-1.994A5.082,5.082,0,0,1,41.558,211.161Z'
                    transform='translate(-41.142 -210.979)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <path
                id='Path_9517'
                data-name='Path 9517'
                d='M92.608,193.917l-51.893,29.9-.927-2.022,54.7-31.58Z'
                transform='translate(-39.788 -190.219)'
                fill='#6f90e2'
              />
            </g>
          </g>
          <g
            id='Group_5112'
            data-name='Group 5112'
            transform='translate(41.002 47.544)'
          >
            <g
              id='Group_5094'
              data-name='Group 5094'
              transform='translate(2.061 3.698)'
            >
              <path
                id='Path_9518'
                data-name='Path 9518'
                d='M80.5,207.35l-16.25,9.382L80.5,226.1Z'
                transform='translate(-29.74 -207.35)'
                fill='#507cd3'
              />
              <path
                id='Path_9519'
                data-name='Path 9519'
                d='M41.167,252.324l50.759-29.306v-.029l-16.25-9.364L41.167,233.548Z'
                transform='translate(-41.167 -204.243)'
                fill='#8fa6ef'
              />
            </g>
            <g id='Group_5111' data-name='Group 5111'>
              <g
                id='Group_5098'
                data-name='Group 5098'
                transform='translate(36.03 6.54)'
              >
                <g
                  id='Group_5095'
                  data-name='Group 5095'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9520'
                    data-name='Path 9520'
                    d='M73.6,224.094l-1.973,1.139v-14L73.6,210.09Z'
                    transform='translate(-66.827 -208.835)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9521'
                    data-name='Path 9521'
                    d='M68.5,210.984l-.079.043v11.532l4.8,2.8v-14l-2.61-1.522C69.891,210.233,69.163,210.605,68.5,210.984Z'
                    transform='translate(-68.417 -208.962)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9522'
                    data-name='Path 9522'
                    d='M72.545,209.39l-1.235-.139c-.454.315-.939.6-1.428.872l2.61,1.522,1.972-1.138Z'
                    transform='translate(-67.692 -209.251)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5096'
                  data-name='Group 5096'
                  transform='translate(3.386 1.672)'
                >
                  <path
                    id='Path_9523'
                    data-name='Path 9523'
                    d='M71.335,225.41l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-64.562 -209.855)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9524'
                    data-name='Path 9524'
                    d='M66.152,212.2v11.693l4.8,2.8v-14l-2.7-1.576C67.562,211.5,66.867,211.865,66.152,212.2Z'
                    transform='translate(-66.152 -210)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9525'
                    data-name='Path 9525'
                    d='M68.922,210.719q-.684.386-1.365.764l2.7,1.576,1.972-1.139-2.667-1.55C69.345,210.486,69.128,210.6,68.922,210.719Z'
                    transform='translate(-65.456 -210.369)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5097'
                  data-name='Group 5097'
                  transform='translate(0 3.572)'
                >
                  <path
                    id='Path_9526'
                    data-name='Path 9526'
                    d='M69.071,226.725,67.1,227.864v-14l1.973-1.139Z'
                    transform='translate(-62.297 -211.104)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9527'
                    data-name='Path 9527'
                    d='M63.887,212.714v12.565l4.8,2.8v-14l-3.065-1.788A17.53,17.53,0,0,1,63.887,212.714Z'
                    transform='translate(-63.887 -211.319)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9528'
                    data-name='Path 9528'
                    d='M65.453,212.474c-.13.049-.268.091-.405.135l3.065,1.788,1.972-1.139L67.3,211.64C66.7,211.941,66.089,212.228,65.453,212.474Z'
                    transform='translate(-63.312 -211.64)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5102'
                data-name='Group 5102'
                transform='translate(23.966 12.746)'
              >
                <g
                  id='Group_5099'
                  data-name='Group 5099'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9529'
                    data-name='Path 9529'
                    d='M65.531,228.777l-1.972,1.139v-14l1.972-1.139Z'
                    transform='translate(-58.758 -212.723)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9530'
                    data-name='Path 9530'
                    d='M61.765,214.215c-.463.286-.936.565-1.417.834v12.342l4.8,2.8v-14Z'
                    transform='translate(-60.348 -212.999)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9531'
                    data-name='Path 9531'
                    d='M63.126,213.4c-.579.42-1.2.824-1.83,1.216l3.383,1.974,1.972-1.138Z'
                    transform='translate(-59.879 -213.402)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5100'
                  data-name='Group 5100'
                  transform='translate(3.386 2.05)'
                >
                  <path
                    id='Path_9532'
                    data-name='Path 9532'
                    d='M63.267,230.093l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-56.493 -214.121)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9533'
                    data-name='Path 9533'
                    d='M58.083,216.119v12.607l4.8,2.8v-14l-3.478-2.029Q58.74,215.821,58.083,216.119Z'
                    transform='translate(-58.083 -214.416)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9534'
                    data-name='Path 9534'
                    d='M58.968,215.851l3.478,2.029,1.972-1.139-3.386-1.968C60.349,215.153,59.659,215.512,58.968,215.851Z'
                    transform='translate(-57.645 -214.773)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5101'
                  data-name='Group 5101'
                  transform='translate(0 3.871)'
                >
                  <path
                    id='Path_9535'
                    data-name='Path 9535'
                    d='M61,231.408l-1.973,1.139v-14L61,217.4Z'
                    transform='translate(-54.228 -215.291)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9536'
                    data-name='Path 9536'
                    d='M55.818,217.839v12.2l4.8,2.8v-14l-3.47-2.024C56.721,217.14,56.275,217.493,55.818,217.839Z'
                    transform='translate(-55.818 -215.584)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9537'
                    data-name='Path 9537'
                    d='M58.507,216l-.094.039c-.262.172-.52.345-.783.519-.3.2-.606.426-.921.667l3.47,2.024,1.972-1.139-3.636-2.114Z'
                    transform='translate(-55.377 -215.991)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5106'
                data-name='Group 5106'
                transform='translate(11.845 20.137)'
              >
                <g
                  id='Group_5103'
                  data-name='Group 5103'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9538'
                    data-name='Path 9538'
                    d='M57.424,233.659,55.451,234.8v-14l1.973-1.139Z'
                    transform='translate(-50.652 -217.696)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9539'
                    data-name='Path 9539'
                    d='M53.853,219.173q-.789.491-1.612.93v12.134l4.8,2.8v-14Z'
                    transform='translate(-52.241 -217.935)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9540'
                    data-name='Path 9540'
                    d='M55.109,218.345a19.935,19.935,0,0,1-1.79,1.238l3.188,1.86,1.974-1.138Z'
                    transform='translate(-51.707 -218.345)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5104'
                  data-name='Group 5104'
                  transform='translate(3.386 2.066)'
                >
                  <path
                    id='Path_9541'
                    data-name='Path 9541'
                    d='M55.159,234.975l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-48.387 -219.111)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9542'
                    data-name='Path 9542'
                    d='M51.118,220.613c-.38.182-.761.38-1.142.576v12.387l4.8,2.8v-14l-3.31-1.932Z'
                    transform='translate(-49.976 -219.374)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9543'
                    data-name='Path 9543'
                    d='M50.972,220.794l3.31,1.932,1.974-1.139-3.2-1.86C52.377,220.095,51.684,220.448,50.972,220.794Z'
                    transform='translate(-49.483 -219.727)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5105'
                  data-name='Group 5105'
                  transform='translate(0 3.902)'
                >
                  <path
                    id='Path_9544'
                    data-name='Path 9544'
                    d='M52.894,236.29l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-46.122 -220.295)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9545'
                    data-name='Path 9545'
                    d='M47.711,221.868v13.09l4.8,2.8v-14l-3.653-2.132A5.851,5.851,0,0,1,47.711,221.868Z'
                    transform='translate(-47.711 -220.624)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9546'
                    data-name='Path 9546'
                    d='M48.521,221.943c-.013,0-.03.006-.043.01l3.653,2.132,1.974-1.139-3.427-1.991A19.375,19.375,0,0,1,48.521,221.943Z'
                    transform='translate(-47.331 -220.955)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5110'
                data-name='Group 5110'
                transform='translate(2.024 26.769)'
              >
                <g
                  id='Group_5107'
                  data-name='Group 5107'
                  transform='translate(4.53)'
                >
                  <path
                    id='Path_9547'
                    data-name='Path 9547'
                    d='M49.355,238.342l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-42.583 -222.01)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9548'
                    data-name='Path 9548'
                    d='M45.5,223.744c-.425.339-.87.655-1.325.951v12.281l4.8,2.8v-14Z'
                    transform='translate(-44.172 -222.304)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9549'
                    data-name='Path 9549'
                    d='M46.5,222.781a8.583,8.583,0,0,1-1.441,1.44l3.475,2.027,1.974-1.138Z'
                    transform='translate(-43.733 -222.781)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5108'
                  data-name='Group 5108'
                  transform='translate(1.144 2.356)'
                >
                  <path
                    id='Path_9550'
                    data-name='Path 9550'
                    d='M47.09,239.658,45.117,240.8v-14l1.973-1.139Z'
                    transform='translate(-40.318 -223.715)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9551'
                    data-name='Path 9551'
                    d='M41.907,225.864v12.385l4.8,2.8v-14l-3.265-1.9A8.973,8.973,0,0,1,41.907,225.864Z'
                    transform='translate(-41.907 -223.968)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9552'
                    data-name='Path 9552'
                    d='M43.313,225.309c-.123.076-.253.148-.38.221l3.265,1.9,1.974-1.139-3.336-1.939C44.325,224.693,43.808,225.009,43.313,225.309Z'
                    transform='translate(-41.399 -224.357)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5109'
                  data-name='Group 5109'
                  transform='translate(0 4.268)'
                >
                  <path
                    id='Path_9553'
                    data-name='Path 9553'
                    d='M44.825,240.974l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-40.295 -224.975)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9554'
                    data-name='Path 9554'
                    d='M41.142,226.734v14l2.557,1.492v-14Z'
                    transform='translate(-41.142 -225.092)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9555'
                    data-name='Path 9555'
                    d='M41.558,225.82a1.742,1.742,0,0,1-.416.006v1.452L43.7,228.77l1.974-1.139-3.43-1.994A5.332,5.332,0,0,1,41.558,225.82Z'
                    transform='translate(-41.142 -225.636)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <path
                id='Path_9556'
                data-name='Path 9556'
                d='M92.608,208.574l-51.893,29.9-.927-2.022,54.7-31.58Z'
                transform='translate(-39.788 -204.876)'
                fill='#6f90e2'
              />
            </g>
          </g>
          <g
            id='Group_5131'
            data-name='Group 5131'
            transform='translate(41.002 69.369)'
          >
            <g
              id='Group_5113'
              data-name='Group 5113'
              transform='translate(2.061 3.698)'
            >
              <path
                id='Path_9557'
                data-name='Path 9557'
                d='M80.5,221.948l-16.25,9.382,16.25,9.364Z'
                transform='translate(-29.74 -221.948)'
                fill='#507cd3'
              />
              <path
                id='Path_9558'
                data-name='Path 9558'
                d='M41.167,266.922l50.759-29.306v-.029l-16.25-9.364L41.167,248.147Z'
                transform='translate(-41.167 -218.841)'
                fill='#8fa6ef'
              />
            </g>
            <g id='Group_5130' data-name='Group 5130'>
              <g
                id='Group_5117'
                data-name='Group 5117'
                transform='translate(36.03 6.54)'
              >
                <g
                  id='Group_5114'
                  data-name='Group 5114'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9559'
                    data-name='Path 9559'
                    d='M73.6,238.692l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-66.827 -223.433)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9560'
                    data-name='Path 9560'
                    d='M68.5,225.582l-.079.043v11.532l4.8,2.8v-14l-2.61-1.522C69.891,224.831,69.163,225.2,68.5,225.582Z'
                    transform='translate(-68.417 -223.56)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9561'
                    data-name='Path 9561'
                    d='M72.545,223.988l-1.235-.139c-.454.315-.939.6-1.428.872l2.61,1.522,1.972-1.139Z'
                    transform='translate(-67.692 -223.849)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5115'
                  data-name='Group 5115'
                  transform='translate(3.386 1.672)'
                >
                  <path
                    id='Path_9562'
                    data-name='Path 9562'
                    d='M71.335,240.007l-1.973,1.139v-14L71.335,226Z'
                    transform='translate(-64.562 -224.454)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9563'
                    data-name='Path 9563'
                    d='M66.152,226.8v11.693l4.8,2.8v-14l-2.7-1.576C67.562,226.093,66.867,226.463,66.152,226.8Z'
                    transform='translate(-66.152 -224.598)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9564'
                    data-name='Path 9564'
                    d='M68.922,225.317q-.684.386-1.365.764l2.7,1.576,1.972-1.139-2.667-1.55C69.345,225.084,69.128,225.2,68.922,225.317Z'
                    transform='translate(-65.456 -224.967)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5116'
                  data-name='Group 5116'
                  transform='translate(0 3.572)'
                >
                  <path
                    id='Path_9565'
                    data-name='Path 9565'
                    d='M69.071,241.323,67.1,242.462v-14l1.973-1.139Z'
                    transform='translate(-62.297 -225.702)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9566'
                    data-name='Path 9566'
                    d='M63.887,227.312v12.565l4.8,2.8v-14l-3.065-1.788A17.547,17.547,0,0,1,63.887,227.312Z'
                    transform='translate(-63.887 -225.917)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9567'
                    data-name='Path 9567'
                    d='M65.453,227.072c-.13.049-.268.091-.405.135l3.065,1.788,1.972-1.139L67.3,226.238C66.7,226.539,66.089,226.826,65.453,227.072Z'
                    transform='translate(-63.312 -226.238)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5121'
                data-name='Group 5121'
                transform='translate(23.966 12.747)'
              >
                <g
                  id='Group_5118'
                  data-name='Group 5118'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9568'
                    data-name='Path 9568'
                    d='M65.531,243.375l-1.972,1.139v-14l1.972-1.139Z'
                    transform='translate(-58.758 -227.321)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9569'
                    data-name='Path 9569'
                    d='M61.765,228.813c-.463.286-.936.565-1.417.833v12.343l4.8,2.8v-14Z'
                    transform='translate(-60.348 -227.597)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9570'
                    data-name='Path 9570'
                    d='M63.126,228c-.579.42-1.2.824-1.83,1.216l3.383,1.974,1.972-1.139Z'
                    transform='translate(-59.879 -228)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5119'
                  data-name='Group 5119'
                  transform='translate(3.386 2.048)'
                >
                  <path
                    id='Path_9571'
                    data-name='Path 9571'
                    d='M63.267,244.691l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-56.493 -228.718)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9572'
                    data-name='Path 9572'
                    d='M58.083,230.717v12.607l4.8,2.8v-14l-3.478-2.029Q58.74,230.419,58.083,230.717Z'
                    transform='translate(-58.083 -229.013)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9573'
                    data-name='Path 9573'
                    d='M58.968,230.449l3.478,2.029,1.972-1.139-3.386-1.969C60.349,229.751,59.659,230.11,58.968,230.449Z'
                    transform='translate(-57.645 -229.37)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5120'
                  data-name='Group 5120'
                  transform='translate(0 3.871)'
                >
                  <path
                    id='Path_9574'
                    data-name='Path 9574'
                    d='M61,246.006l-1.973,1.139v-14L61,232Z'
                    transform='translate(-54.228 -229.889)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9575'
                    data-name='Path 9575'
                    d='M55.818,232.437v12.2l4.8,2.8v-14l-3.47-2.024C56.721,231.738,56.275,232.091,55.818,232.437Z'
                    transform='translate(-55.818 -230.182)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9576'
                    data-name='Path 9576'
                    d='M58.507,230.592c-.031.015-.063.025-.094.04-.262.172-.52.345-.783.519-.3.2-.606.426-.921.667l3.47,2.024L62.15,232.7l-3.636-2.114Z'
                    transform='translate(-55.377 -230.589)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5125'
                data-name='Group 5125'
                transform='translate(11.845 20.137)'
              >
                <g
                  id='Group_5122'
                  data-name='Group 5122'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9577'
                    data-name='Path 9577'
                    d='M57.424,248.257,55.451,249.4v-14l1.973-1.139Z'
                    transform='translate(-50.652 -232.294)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9578'
                    data-name='Path 9578'
                    d='M53.853,233.771q-.789.491-1.612.928v12.134l4.8,2.8v-14Z'
                    transform='translate(-52.241 -232.533)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9579'
                    data-name='Path 9579'
                    d='M55.109,232.943a19.922,19.922,0,0,1-1.79,1.238l3.188,1.86L58.48,234.9Z'
                    transform='translate(-51.707 -232.943)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5123'
                  data-name='Group 5123'
                  transform='translate(3.386 2.066)'
                >
                  <path
                    id='Path_9580'
                    data-name='Path 9580'
                    d='M55.159,249.573l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-48.387 -233.709)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9581'
                    data-name='Path 9581'
                    d='M51.118,235.211c-.38.182-.761.38-1.142.576v12.387l4.8,2.8v-14l-3.31-1.932C51.348,235.1,51.235,235.154,51.118,235.211Z'
                    transform='translate(-49.976 -233.971)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9582'
                    data-name='Path 9582'
                    d='M50.972,235.392l3.31,1.932,1.974-1.139-3.2-1.86C52.377,234.693,51.684,235.046,50.972,235.392Z'
                    transform='translate(-49.483 -234.325)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5124'
                  data-name='Group 5124'
                  transform='translate(0 3.901)'
                >
                  <path
                    id='Path_9583'
                    data-name='Path 9583'
                    d='M52.894,250.888l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-46.122 -234.892)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9584'
                    data-name='Path 9584'
                    d='M47.711,236.465v13.091l4.8,2.8v-14l-3.653-2.132A5.743,5.743,0,0,1,47.711,236.465Z'
                    transform='translate(-47.711 -235.221)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9585'
                    data-name='Path 9585'
                    d='M48.521,236.54c-.013.006-.03.008-.043.012l3.653,2.132,1.974-1.139-3.427-1.993A18.852,18.852,0,0,1,48.521,236.54Z'
                    transform='translate(-47.331 -235.552)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5129'
                data-name='Group 5129'
                transform='translate(2.024 26.769)'
              >
                <g
                  id='Group_5126'
                  data-name='Group 5126'
                  transform='translate(4.53)'
                >
                  <path
                    id='Path_9586'
                    data-name='Path 9586'
                    d='M49.355,252.94l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-42.583 -236.608)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9587'
                    data-name='Path 9587'
                    d='M45.5,238.342c-.425.339-.87.655-1.325.951v12.279l4.8,2.8v-14Z'
                    transform='translate(-44.172 -236.902)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9588'
                    data-name='Path 9588'
                    d='M46.5,237.379a8.583,8.583,0,0,1-1.441,1.44l3.475,2.027,1.974-1.138Z'
                    transform='translate(-43.733 -237.379)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5127'
                  data-name='Group 5127'
                  transform='translate(1.144 2.356)'
                >
                  <path
                    id='Path_9589'
                    data-name='Path 9589'
                    d='M47.09,254.256l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-40.318 -238.313)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9590'
                    data-name='Path 9590'
                    d='M41.907,240.462v12.385l4.8,2.8v-14l-3.265-1.9A8.976,8.976,0,0,1,41.907,240.462Z'
                    transform='translate(-41.907 -238.566)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9591'
                    data-name='Path 9591'
                    d='M43.313,239.907c-.123.075-.253.148-.38.221l3.265,1.9,1.974-1.139-3.336-1.939C44.325,239.291,43.808,239.605,43.313,239.907Z'
                    transform='translate(-41.399 -238.955)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5128'
                  data-name='Group 5128'
                  transform='translate(0 4.268)'
                >
                  <path
                    id='Path_9592'
                    data-name='Path 9592'
                    d='M44.825,255.571l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-40.295 -239.574)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9593'
                    data-name='Path 9593'
                    d='M41.142,241.332v14l2.557,1.492v-14Z'
                    transform='translate(-41.142 -239.69)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9594'
                    data-name='Path 9594'
                    d='M41.558,240.418a1.74,1.74,0,0,1-.416.006v1.452l2.557,1.492,1.974-1.139-3.43-1.994A5.327,5.327,0,0,1,41.558,240.418Z'
                    transform='translate(-41.142 -240.234)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <path
                id='Path_9595'
                data-name='Path 9595'
                d='M92.608,223.172l-51.893,29.9-.927-2.022,54.7-31.58Z'
                transform='translate(-39.788 -219.474)'
                fill='#6f90e2'
              />
            </g>
          </g>
          <g
            id='Group_5150'
            data-name='Group 5150'
            transform='translate(41.002 91.218)'
          >
            <g
              id='Group_5132'
              data-name='Group 5132'
              transform='translate(2.061 3.698)'
            >
              <path
                id='Path_9596'
                data-name='Path 9596'
                d='M80.5,236.561l-16.25,9.382,16.25,9.364Z'
                transform='translate(-29.74 -236.561)'
                fill='#507cd3'
              />
              <path
                id='Path_9597'
                data-name='Path 9597'
                d='M41.167,281.536,91.926,252.23V252.2l-16.25-9.364L41.167,262.76Z'
                transform='translate(-41.167 -233.455)'
                fill='#8fa6ef'
              />
            </g>
            <g id='Group_5149' data-name='Group 5149'>
              <g
                id='Group_5136'
                data-name='Group 5136'
                transform='translate(36.03 6.541)'
              >
                <g
                  id='Group_5133'
                  data-name='Group 5133'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9598'
                    data-name='Path 9598'
                    d='M73.6,253.306l-1.973,1.139v-14L73.6,239.3Z'
                    transform='translate(-66.827 -238.048)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9599'
                    data-name='Path 9599'
                    d='M68.5,240.194l-.079.045v11.532l4.8,2.8v-14l-2.61-1.522C69.891,239.445,69.163,239.817,68.5,240.194Z'
                    transform='translate(-68.417 -238.174)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9600'
                    data-name='Path 9600'
                    d='M72.545,238.6l-1.235-.139c-.454.315-.939.6-1.428.872l2.61,1.522,1.972-1.139Z'
                    transform='translate(-67.692 -238.463)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5134'
                  data-name='Group 5134'
                  transform='translate(3.386 1.672)'
                >
                  <path
                    id='Path_9601'
                    data-name='Path 9601'
                    d='M71.335,254.621l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-64.562 -239.068)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9602'
                    data-name='Path 9602'
                    d='M66.152,241.411V253.1l4.8,2.8v-14l-2.7-1.576C67.562,240.707,66.867,241.077,66.152,241.411Z'
                    transform='translate(-66.152 -239.212)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9603'
                    data-name='Path 9603'
                    d='M68.922,239.929q-.684.388-1.365.766l2.7,1.576,1.972-1.139-2.667-1.55C69.345,239.7,69.128,239.813,68.922,239.929Z'
                    transform='translate(-65.456 -239.581)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5135'
                  data-name='Group 5135'
                  transform='translate(0 3.57)'
                >
                  <path
                    id='Path_9604'
                    data-name='Path 9604'
                    d='M69.071,255.937,67.1,257.076v-14l1.973-1.139Z'
                    transform='translate(-62.297 -240.315)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9605'
                    data-name='Path 9605'
                    d='M63.887,241.926v12.563l4.8,2.8v-14L65.623,241.5A17.547,17.547,0,0,1,63.887,241.926Z'
                    transform='translate(-63.887 -240.53)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9606'
                    data-name='Path 9606'
                    d='M65.453,241.687c-.13.049-.268.091-.405.135l3.065,1.787,1.972-1.138L67.3,240.851C66.7,241.153,66.089,241.439,65.453,241.687Z'
                    transform='translate(-63.312 -240.851)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5140'
                data-name='Group 5140'
                transform='translate(23.966 12.747)'
              >
                <g
                  id='Group_5137'
                  data-name='Group 5137'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9607'
                    data-name='Path 9607'
                    d='M65.531,257.989l-1.972,1.139v-14l1.972-1.139Z'
                    transform='translate(-58.758 -241.935)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9608'
                    data-name='Path 9608'
                    d='M61.765,243.427c-.463.286-.936.565-1.417.833V256.6l4.8,2.8v-14Z'
                    transform='translate(-60.348 -242.211)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9609'
                    data-name='Path 9609'
                    d='M63.126,242.614c-.579.42-1.2.824-1.83,1.216l3.383,1.974,1.972-1.139Z'
                    transform='translate(-59.879 -242.614)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5138'
                  data-name='Group 5138'
                  transform='translate(3.386 2.048)'
                >
                  <path
                    id='Path_9610'
                    data-name='Path 9610'
                    d='M63.267,259.3l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-56.493 -243.332)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9611'
                    data-name='Path 9611'
                    d='M58.083,245.331v12.607l4.8,2.8v-14l-3.478-2.029Q58.74,245.033,58.083,245.331Z'
                    transform='translate(-58.083 -243.627)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9612'
                    data-name='Path 9612'
                    d='M58.968,245.063l3.478,2.029,1.972-1.139-3.386-1.969C60.349,244.365,59.659,244.724,58.968,245.063Z'
                    transform='translate(-57.645 -243.984)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5139'
                  data-name='Group 5139'
                  transform='translate(0 3.871)'
                >
                  <path
                    id='Path_9613'
                    data-name='Path 9613'
                    d='M61,260.62l-1.973,1.139v-14L61,246.617Z'
                    transform='translate(-54.228 -244.503)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9614'
                    data-name='Path 9614'
                    d='M55.818,247.051v12.2l4.8,2.8v-14l-3.47-2.024C56.721,246.352,56.275,246.7,55.818,247.051Z'
                    transform='translate(-55.818 -244.796)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9615'
                    data-name='Path 9615'
                    d='M58.507,245.206c-.031.015-.063.025-.094.039-.262.173-.52.347-.783.52-.3.2-.606.426-.921.667l3.47,2.024,1.972-1.139L58.514,245.2Z'
                    transform='translate(-55.377 -245.203)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5144'
                data-name='Group 5144'
                transform='translate(11.845 20.135)'
              >
                <g
                  id='Group_5141'
                  data-name='Group 5141'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9616'
                    data-name='Path 9616'
                    d='M57.424,262.871l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-50.652 -246.907)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9617'
                    data-name='Path 9617'
                    d='M53.853,248.385q-.789.491-1.612.928v12.134l4.8,2.8v-14Z'
                    transform='translate(-52.241 -247.146)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9618'
                    data-name='Path 9618'
                    d='M55.109,247.556a19.947,19.947,0,0,1-1.79,1.239l3.188,1.86,1.974-1.139Z'
                    transform='translate(-51.707 -247.556)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5142'
                  data-name='Group 5142'
                  transform='translate(3.386 2.068)'
                >
                  <path
                    id='Path_9619'
                    data-name='Path 9619'
                    d='M55.159,264.186l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-48.387 -248.323)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9620'
                    data-name='Path 9620'
                    d='M51.118,249.825c-.38.182-.761.378-1.142.574v12.388l4.8,2.8v-14l-3.31-1.932C51.348,249.71,51.235,249.767,51.118,249.825Z'
                    transform='translate(-49.976 -248.585)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9621'
                    data-name='Path 9621'
                    d='M50.972,250.006l3.31,1.932,1.974-1.139-3.2-1.86C52.377,249.307,51.684,249.66,50.972,250.006Z'
                    transform='translate(-49.483 -248.939)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5143'
                  data-name='Group 5143'
                  transform='translate(0 3.902)'
                >
                  <path
                    id='Path_9622'
                    data-name='Path 9622'
                    d='M52.894,265.5l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-46.122 -249.506)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9623'
                    data-name='Path 9623'
                    d='M47.711,251.079V264.17l4.8,2.8v-14l-3.653-2.132A5.743,5.743,0,0,1,47.711,251.079Z'
                    transform='translate(-47.711 -249.835)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9624'
                    data-name='Path 9624'
                    d='M48.521,251.154c-.013.006-.03.006-.043.012l3.653,2.132,1.974-1.139-3.427-1.993A18.847,18.847,0,0,1,48.521,251.154Z'
                    transform='translate(-47.331 -250.166)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5148'
                data-name='Group 5148'
                transform='translate(2.024 26.769)'
              >
                <g
                  id='Group_5145'
                  data-name='Group 5145'
                  transform='translate(4.53)'
                >
                  <path
                    id='Path_9625'
                    data-name='Path 9625'
                    d='M49.355,267.554l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-42.583 -251.222)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9626'
                    data-name='Path 9626'
                    d='M45.5,252.956c-.425.339-.87.655-1.325.951v12.279l4.8,2.8v-14Z'
                    transform='translate(-44.172 -251.516)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9627'
                    data-name='Path 9627'
                    d='M46.5,251.993a8.582,8.582,0,0,1-1.441,1.44l3.475,2.027,1.974-1.139Z'
                    transform='translate(-43.733 -251.993)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5146'
                  data-name='Group 5146'
                  transform='translate(1.144 2.356)'
                >
                  <path
                    id='Path_9628'
                    data-name='Path 9628'
                    d='M47.09,268.87l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-40.318 -252.927)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9629'
                    data-name='Path 9629'
                    d='M41.907,255.077v12.385l4.8,2.8v-14l-3.265-1.906A8.978,8.978,0,0,1,41.907,255.077Z'
                    transform='translate(-41.907 -253.181)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9630'
                    data-name='Path 9630'
                    d='M43.313,254.521c-.123.075-.253.147-.38.22l3.265,1.906,1.974-1.139-3.336-1.939C44.325,253.9,43.808,254.219,43.313,254.521Z'
                    transform='translate(-41.399 -253.569)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5147'
                  data-name='Group 5147'
                  transform='translate(0 4.269)'
                >
                  <path
                    id='Path_9631'
                    data-name='Path 9631'
                    d='M44.825,270.185l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-40.295 -254.188)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9632'
                    data-name='Path 9632'
                    d='M41.142,255.946v14l2.557,1.492v-14Z'
                    transform='translate(-41.142 -254.305)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9633'
                    data-name='Path 9633'
                    d='M41.558,255.03a1.818,1.818,0,0,1-.416.007v1.452l2.557,1.492,1.974-1.139-3.43-1.994A5.091,5.091,0,0,1,41.558,255.03Z'
                    transform='translate(-41.142 -254.848)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <path
                id='Path_9634'
                data-name='Path 9634'
                d='M92.608,237.786l-51.893,29.9-.927-2.022,54.7-31.58Z'
                transform='translate(-39.788 -234.088)'
                fill='#6f90e2'
              />
            </g>
          </g>
          <g
            id='Group_5169'
            data-name='Group 5169'
            transform='translate(41.002 113.045)'
          >
            <g
              id='Group_5151'
              data-name='Group 5151'
              transform='translate(2.061 3.698)'
            >
              <path
                id='Path_9635'
                data-name='Path 9635'
                d='M80.5,251.161l-16.25,9.382,16.25,9.364Z'
                transform='translate(-29.74 -251.161)'
                fill='#507cd3'
              />
              <path
                id='Path_9636'
                data-name='Path 9636'
                d='M41.167,296.135l50.759-29.305V266.8l-16.25-9.364L41.167,277.36Z'
                transform='translate(-41.167 -248.054)'
                fill='#8fa6ef'
              />
            </g>
            <g id='Group_5168' data-name='Group 5168'>
              <g
                id='Group_5155'
                data-name='Group 5155'
                transform='translate(36.03 6.54)'
              >
                <g
                  id='Group_5152'
                  data-name='Group 5152'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9637'
                    data-name='Path 9637'
                    d='M73.6,267.9l-1.973,1.139v-14L73.6,253.9Z'
                    transform='translate(-66.827 -252.646)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9638'
                    data-name='Path 9638'
                    d='M68.5,254.795l-.079.045v11.53l4.8,2.8v-14l-2.61-1.522C69.891,254.046,69.163,254.416,68.5,254.795Z'
                    transform='translate(-68.417 -252.773)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9639'
                    data-name='Path 9639'
                    d='M72.545,253.2l-1.235-.139c-.454.315-.939.6-1.428.872l2.61,1.522,1.972-1.138Z'
                    transform='translate(-67.692 -253.062)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5153'
                  data-name='Group 5153'
                  transform='translate(3.386 1.672)'
                >
                  <path
                    id='Path_9640'
                    data-name='Path 9640'
                    d='M71.335,269.221l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-64.562 -253.666)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9641'
                    data-name='Path 9641'
                    d='M66.152,256.01V267.7l4.8,2.8v-14l-2.7-1.576C67.562,255.306,66.867,255.677,66.152,256.01Z'
                    transform='translate(-66.152 -253.811)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9642'
                    data-name='Path 9642'
                    d='M68.922,254.53q-.684.386-1.365.764l2.7,1.576,1.972-1.139-2.667-1.55C69.345,254.3,69.128,254.413,68.922,254.53Z'
                    transform='translate(-65.456 -254.18)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5154'
                  data-name='Group 5154'
                  transform='translate(0 3.572)'
                >
                  <path
                    id='Path_9643'
                    data-name='Path 9643'
                    d='M69.071,270.536,67.1,271.675v-14l1.973-1.139Z'
                    transform='translate(-62.297 -254.915)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9644'
                    data-name='Path 9644'
                    d='M63.887,256.525V269.09l4.8,2.8v-14L65.623,256.1A17.539,17.539,0,0,1,63.887,256.525Z'
                    transform='translate(-63.887 -255.13)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9645'
                    data-name='Path 9645'
                    d='M65.453,256.285c-.13.051-.268.091-.405.135l3.065,1.788,1.972-1.139L67.3,255.451C66.7,255.751,66.089,256.039,65.453,256.285Z'
                    transform='translate(-63.312 -255.451)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5159'
                data-name='Group 5159'
                transform='translate(23.966 12.748)'
              >
                <g
                  id='Group_5156'
                  data-name='Group 5156'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9646'
                    data-name='Path 9646'
                    d='M65.531,272.588l-1.972,1.139v-14l1.972-1.139Z'
                    transform='translate(-58.758 -256.535)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9647'
                    data-name='Path 9647'
                    d='M61.765,258.026c-.463.286-.936.565-1.417.834V271.2l4.8,2.8V260Z'
                    transform='translate(-60.348 -256.812)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9648'
                    data-name='Path 9648'
                    d='M63.126,257.214c-.579.42-1.2.822-1.83,1.214l3.383,1.975,1.972-1.139Z'
                    transform='translate(-59.879 -257.214)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5157'
                  data-name='Group 5157'
                  transform='translate(3.386 2.048)'
                >
                  <path
                    id='Path_9649'
                    data-name='Path 9649'
                    d='M63.267,273.9l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-56.493 -257.932)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9650'
                    data-name='Path 9650'
                    d='M58.083,259.93v12.607l4.8,2.8v-14L59.406,259.3Q58.74,259.632,58.083,259.93Z'
                    transform='translate(-58.083 -258.227)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9651'
                    data-name='Path 9651'
                    d='M58.968,259.662l3.478,2.029,1.972-1.139-3.386-1.968C60.349,258.964,59.659,259.323,58.968,259.662Z'
                    transform='translate(-57.645 -258.584)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5158'
                  data-name='Group 5158'
                  transform='translate(0 3.869)'
                >
                  <path
                    id='Path_9652'
                    data-name='Path 9652'
                    d='M61,275.22l-1.973,1.139v-14L61,261.216Z'
                    transform='translate(-54.228 -259.102)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9653'
                    data-name='Path 9653'
                    d='M55.818,261.65v12.2l4.8,2.8v-14l-3.47-2.024C56.721,260.951,56.275,261.3,55.818,261.65Z'
                    transform='translate(-55.818 -259.395)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9654'
                    data-name='Path 9654'
                    d='M58.507,259.806l-.094.039c-.262.172-.52.345-.783.519-.3.2-.606.426-.921.667l3.47,2.024,1.972-1.139L58.514,259.8Z'
                    transform='translate(-55.377 -259.802)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5163'
                data-name='Group 5163'
                transform='translate(11.845 20.136)'
              >
                <g
                  id='Group_5160'
                  data-name='Group 5160'
                  transform='translate(6.773)'
                >
                  <path
                    id='Path_9655'
                    data-name='Path 9655'
                    d='M57.424,277.47l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-50.652 -261.507)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9656'
                    data-name='Path 9656'
                    d='M53.853,262.984q-.789.491-1.612.93v12.134l4.8,2.8v-14Z'
                    transform='translate(-52.241 -261.746)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9657'
                    data-name='Path 9657'
                    d='M55.109,262.156a19.941,19.941,0,0,1-1.79,1.238l3.188,1.86,1.974-1.138Z'
                    transform='translate(-51.707 -262.156)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5161'
                  data-name='Group 5161'
                  transform='translate(3.386 2.068)'
                >
                  <path
                    id='Path_9658'
                    data-name='Path 9658'
                    d='M55.159,278.786l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-48.387 -262.923)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9659'
                    data-name='Path 9659'
                    d='M51.118,264.424c-.38.184-.761.38-1.142.576v12.387l4.8,2.8v-14l-3.31-1.932C51.348,264.309,51.235,264.367,51.118,264.424Z'
                    transform='translate(-49.976 -263.186)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9660'
                    data-name='Path 9660'
                    d='M50.972,264.605l3.31,1.932,1.974-1.139-3.2-1.858C52.377,263.905,51.684,264.258,50.972,264.605Z'
                    transform='translate(-49.483 -263.539)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5162'
                  data-name='Group 5162'
                  transform='translate(0 3.902)'
                >
                  <path
                    id='Path_9661'
                    data-name='Path 9661'
                    d='M52.894,280.1l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-46.122 -264.106)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9662'
                    data-name='Path 9662'
                    d='M47.711,265.679v13.09l4.8,2.8v-14l-3.653-2.132A5.853,5.853,0,0,1,47.711,265.679Z'
                    transform='translate(-47.711 -264.435)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9663'
                    data-name='Path 9663'
                    d='M48.521,265.754c-.013,0-.03.006-.043.01l3.653,2.132,1.974-1.139-3.427-1.991A19.381,19.381,0,0,1,48.521,265.754Z'
                    transform='translate(-47.331 -264.766)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <g
                id='Group_5167'
                data-name='Group 5167'
                transform='translate(2.024 26.769)'
              >
                <g
                  id='Group_5164'
                  data-name='Group 5164'
                  transform='translate(4.53)'
                >
                  <path
                    id='Path_9664'
                    data-name='Path 9664'
                    d='M49.355,282.153l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-42.583 -265.821)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9665'
                    data-name='Path 9665'
                    d='M45.5,267.555c-.425.339-.87.655-1.325.951v12.281l4.8,2.8v-14Z'
                    transform='translate(-44.172 -266.115)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9666'
                    data-name='Path 9666'
                    d='M46.5,266.592a8.583,8.583,0,0,1-1.441,1.44l3.475,2.029,1.974-1.139Z'
                    transform='translate(-43.733 -266.592)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5165'
                  data-name='Group 5165'
                  transform='translate(1.144 2.358)'
                >
                  <path
                    id='Path_9667'
                    data-name='Path 9667'
                    d='M47.09,283.469l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-40.318 -267.527)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9668'
                    data-name='Path 9668'
                    d='M41.907,269.677V282.06l4.8,2.8v-14l-3.265-1.9A9.1,9.1,0,0,1,41.907,269.677Z'
                    transform='translate(-41.907 -267.781)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9669'
                    data-name='Path 9669'
                    d='M43.313,269.121c-.123.075-.253.147-.38.22l3.265,1.9,1.974-1.138-3.336-1.939C44.325,268.5,43.808,268.819,43.313,269.121Z'
                    transform='translate(-41.399 -268.169)'
                    fill='#ffd392'
                  />
                </g>
                <g
                  id='Group_5166'
                  data-name='Group 5166'
                  transform='translate(0 4.27)'
                >
                  <path
                    id='Path_9670'
                    data-name='Path 9670'
                    d='M44.825,284.785l-1.973,1.139v-14l1.973-1.139Z'
                    transform='translate(-40.295 -268.788)'
                    fill='#fdc66f'
                  />
                  <path
                    id='Path_9671'
                    data-name='Path 9671'
                    d='M41.142,270.545v14l2.557,1.492v-14Z'
                    transform='translate(-41.142 -268.905)'
                    fill='#ffa748'
                  />
                  <path
                    id='Path_9672'
                    data-name='Path 9672'
                    d='M41.558,269.63a1.744,1.744,0,0,1-.416.006v1.452L43.7,272.58l1.974-1.139-3.43-1.993A5.325,5.325,0,0,1,41.558,269.63Z'
                    transform='translate(-41.142 -269.448)'
                    fill='#ffd392'
                  />
                </g>
              </g>
              <path
                id='Path_9673'
                data-name='Path 9673'
                d='M92.608,252.385l-51.893,29.9-.927-2.022,54.7-31.58Z'
                transform='translate(-39.788 -248.688)'
                fill='#6f90e2'
              />
            </g>
          </g>
          <g
            id='Group_5171'
            data-name='Group 5171'
            transform='translate(22.858 50.652)'
          >
            <g id='Group_5170' data-name='Group 5170'>
              <path
                id='Path_9674'
                data-name='Path 9674'
                d='M41.631,215.026l-13.979-8.071v17.724l13.979,8.07Z'
                transform='translate(-27.653 -206.955)'
                fill='#fff'
              />
              <path
                id='Path_9675'
                data-name='Path 9675'
                d='M39.5,215.153l-10.775-6.221v13.662L39.5,228.815Z'
                transform='translate(-27.122 -205.977)'
                fill='#8fa6ef'
              />
            </g>
          </g>
          <g
            id='Group_5174'
            data-name='Group 5174'
            transform='translate(22.858 72.42)'
          >
            <g id='Group_5172' data-name='Group 5172'>
              <path
                id='Path_9676'
                data-name='Path 9676'
                d='M41.631,229.585l-13.979-8.07v17.724l13.979,8.07Z'
                transform='translate(-27.653 -221.515)'
                fill='#fff'
              />
              <path
                id='Path_9677'
                data-name='Path 9677'
                d='M39.5,229.713l-10.775-6.221v13.662L39.5,243.375Z'
                transform='translate(-27.122 -220.536)'
                fill='#8fa6ef'
              />
            </g>
            <g
              id='Group_5173'
              data-name='Group 5173'
              transform='translate(0 22.624)'
            >
              <path
                id='Path_9678'
                data-name='Path 9678'
                d='M41.631,244.718l-13.979-8.071v17.724l13.979,8.07Z'
                transform='translate(-27.653 -236.647)'
                fill='#fff'
              />
              <path
                id='Path_9679'
                data-name='Path 9679'
                d='M39.5,244.845l-10.775-6.221v13.662L39.5,258.507Z'
                transform='translate(-27.122 -235.669)'
                fill='#8fa6ef'
              />
            </g>
          </g>
          <g
            id='Group_5177'
            data-name='Group 5177'
            transform='translate(22.858 117.63)'
          >
            <g id='Group_5175' data-name='Group 5175'>
              <path
                id='Path_9680'
                data-name='Path 9680'
                d='M41.631,259.824l-13.979-8.071v17.724l13.979,8.071Z'
                transform='translate(-27.653 -251.754)'
                fill='#fff'
              />
              <path
                id='Path_9681'
                data-name='Path 9681'
                d='M39.5,259.952l-10.775-6.221v13.662L39.5,273.614Z'
                transform='translate(-27.122 -250.775)'
                fill='#8fa6ef'
              />
            </g>
            <g
              id='Group_5176'
              data-name='Group 5176'
              transform='translate(0 22.624)'
            >
              <path
                id='Path_9682'
                data-name='Path 9682'
                d='M41.631,274.957l-13.979-8.071V284.61l13.979,8.071Z'
                transform='translate(-27.653 -266.886)'
                fill='#fff'
              />
              <path
                id='Path_9683'
                data-name='Path 9683'
                d='M39.5,275.084l-10.775-6.221v13.662L39.5,288.746Z'
                transform='translate(-27.122 -265.907)'
                fill='#8fa6ef'
              />
            </g>
          </g>
        </g>
        <g
          id='Group_5234'
          data-name='Group 5234'
          transform='translate(77.632 208.679)'
        >
          <g
            id='Group_5179'
            data-name='Group 5179'
            transform='translate(24.184)'
          >
            <path
              id='Path_9684'
              data-name='Path 9684'
              d='M85.427,231.907a.136.136,0,0,0,.117-.063.138.138,0,0,0-.04-.191c-.022-.015-2.332-1.574-3.771-6.142a62.106,62.106,0,0,1-1.51-9.628c-.517-4.614-1.053-9.383-2.074-11.689-2.582-5.825-5.631-7.235-5.759-7.292a.139.139,0,1,0-.114.253c.031.013,3.086,1.437,5.619,7.153,1,2.265,1.537,7.013,2.053,11.606a62.474,62.474,0,0,0,1.519,9.681c1.473,4.671,3.787,6.226,3.884,6.29A.133.133,0,0,0,85.427,231.907Z'
              transform='translate(-72.194 -196.889)'
              fill='#1c1c1c'
            />
          </g>
          <g
            id='Group_5180'
            data-name='Group 5180'
            transform='translate(21.035 1.618)'
          >
            <path
              id='Path_9685'
              data-name='Path 9685'
              d='M83.913,232.667a.138.138,0,0,0,.112-.058.14.14,0,0,0-.031-.194,35.315,35.315,0,0,1-5.357-4.91c-2.5-2.914-3.9-6.211-4.149-15.268-.293-10.748-1.773-12.077-4.011-14.088l-.157-.141a.136.136,0,0,0-.2.01.139.139,0,0,0,.009.2l.157.141c2.187,1.965,3.63,3.261,3.92,13.889.251,9.139,1.676,12.478,4.216,15.441a35.687,35.687,0,0,0,5.4,4.955A.142.142,0,0,0,83.913,232.667Z'
              transform='translate(-70.088 -197.972)'
              fill='#1c1c1c'
            />
          </g>
          <g
            id='Group_5181'
            data-name='Group 5181'
            transform='translate(8.735 30.493)'
          >
            <path
              id='Path_9686'
              data-name='Path 9686'
              d='M67.125,235.279a.137.137,0,0,0,.112-.057.139.139,0,0,0-.031-.194,11.179,11.179,0,0,1-2.875-3.8c-1.127-2.2-.975-3.611-.7-6.164.064-.6.138-1.272.2-2.029.333-3.986-1.661-5.654-1.745-5.723a.141.141,0,0,0-.2.019.138.138,0,0,0,.019.2c.019.016,1.965,1.655,1.643,5.486-.063.752-.136,1.426-.2,2.021-.281,2.607-.435,4.044.728,6.321a11.237,11.237,0,0,0,2.96,3.895A.14.14,0,0,0,67.125,235.279Z'
              transform='translate(-61.861 -217.284)'
              fill='#1c1c1c'
            />
          </g>
          <g
            id='Group_5182'
            data-name='Group 5182'
            transform='translate(5.348 32.35)'
          >
            <path
              id='Path_9687'
              data-name='Path 9687'
              d='M65.521,236.018a.139.139,0,0,0,.075-.256c-.021-.013-2.122-1.358-3.081-2.147-1.7-1.4-2.389-3.675-1.981-6.575.052-.371.1-.725.154-1.064.487-3.339.782-5.352-.845-7.4a.139.139,0,1,0-.217.173c1.552,1.95,1.28,3.808.786,7.181-.049.341-.1.7-.154,1.068-.534,3.793.836,5.807,2.081,6.83.972.8,3.086,2.151,3.107,2.166A.149.149,0,0,0,65.521,236.018Z'
              transform='translate(-59.595 -218.527)'
              fill='#1c1c1c'
            />
          </g>
          <g
            id='Group_5183'
            data-name='Group 5183'
            transform='translate(0 35.524)'
          >
            <path
              id='Path_9688'
              data-name='Path 9688'
              d='M65.879,237.362a.138.138,0,0,0,.13-.093.14.14,0,0,0-.084-.178,42.1,42.1,0,0,1-4.617-1.972c-1.766-.952-2.307-2.924-2.234-8.111.078-5.472-2.76-6.32-2.881-6.354a.14.14,0,0,0-.075.269c.027.007,2.752.84,2.678,6.081-.076,5.315.5,7.347,2.38,8.359a42.346,42.346,0,0,0,4.654,1.99A.135.135,0,0,0,65.879,237.362Z'
              transform='translate(-56.018 -220.65)'
              fill='#1c1c1c'
            />
          </g>
          <g
            id='Group_5184'
            data-name='Group 5184'
            transform='translate(21.035 23.292)'
          >
            <path
              id='Path_9689'
              data-name='Path 9689'
              d='M76.152,229.682a2.418,2.418,0,0,0,2.075-1.328,2.283,2.283,0,0,1,1.468-1.239,2.825,2.825,0,0,1,2.017.487.139.139,0,0,0,.164-.224,3.1,3.1,0,0,0-2.238-.535,2.541,2.541,0,0,0-1.654,1.377,2.045,2.045,0,0,1-2.165,1.153c-1.032-.193-1.769-1.192-1.972-2.67-.362-2.625-.481-4.917-.57-6.59-.042-.812-.076-1.453-.126-1.894-.543-4.844-2.784-5.707-2.88-5.741a.139.139,0,0,0-.094.262c.021.007,2.175.857,2.7,5.511.049.432.084,1.07.126,1.878.088,1.677.208,3.977.57,6.613.221,1.6,1.042,2.69,2.2,2.906A2.082,2.082,0,0,0,76.152,229.682Z'
              transform='translate(-70.088 -212.469)'
              fill='#1c1c1c'
            />
          </g>
          <g
            id='Group_5186'
            data-name='Group 5186'
            transform='translate(31.428 130.593)'
          >
            <path
              id='Path_9690'
              data-name='Path 9690'
              d='M96.179,289.957s-9.388-5.3-13.894-5.638a4.647,4.647,0,0,0-4.246,7.484,26.717,26.717,0,0,0,11.019,6.72c4.122,1.462,5.892.317,8.41-1.936C100.43,293.937,96.179,289.957,96.179,289.957Z'
              transform='translate(-77.039 -284.206)'
              fill='#1c1c1c'
            />
            <path
              id='Path_9691'
              data-name='Path 9691'
              d='M95.581,288.088s-7.372-4.623-13.614-3.738c-2.087.3-2.917.7-3.6,2.053-1.235,2.45-.752,4.182.99,5.5a31.994,31.994,0,0,0,9.107,5.306c4.556,1.848,6.249.721,8.663-1.645C99.951,292.8,95.581,288.088,95.581,288.088Z'
              transform='translate(-76.721 -284.237)'
              fill='#fff'
            />
            <g
              id='Group_5185'
              data-name='Group 5185'
              transform='translate(4.504)'
            >
              <path
                id='Path_9692'
                data-name='Path 9692'
                d='M92.082,294.659a22.934,22.934,0,0,1-6.119-1.872c-3.76-1.654-6.2-3.276-5.883-5.819a2.773,2.773,0,0,1,2.721-2.732l.991.066c-.1.01-2.652.65-2.917,2.764-.221,1.778,1.3,3.176,5.411,4.985a22.593,22.593,0,0,0,5.883,1.809Z'
                transform='translate(-80.052 -284.237)'
                fill='#d3d3d3'
              />
            </g>
          </g>
          <g
            id='Group_5188'
            data-name='Group 5188'
            transform='translate(48.957 119.472)'
          >
            <path
              id='Path_9693'
              data-name='Path 9693'
              d='M107.9,282.52s-9.388-5.3-13.894-5.64a4.647,4.647,0,0,0-4.245,7.484,26.711,26.711,0,0,0,11.017,6.72c4.123,1.462,5.894.317,8.411-1.936C112.153,286.5,107.9,282.52,107.9,282.52Z'
              transform='translate(-88.764 -276.768)'
              fill='#1c1c1c'
            />
            <path
              id='Path_9694'
              data-name='Path 9694'
              d='M107.305,280.649s-7.372-4.623-13.614-3.738c-2.086.3-2.915.7-3.6,2.053-1.235,2.45-.752,4.182.99,5.5a32.03,32.03,0,0,0,9.107,5.308c4.556,1.846,6.249.719,8.663-1.646C111.675,285.36,107.305,280.649,107.305,280.649Z'
              transform='translate(-88.445 -276.799)'
              fill='#fff'
            />
            <g
              id='Group_5187'
              data-name='Group 5187'
              transform='translate(4.503 0.001)'
            >
              <path
                id='Path_9695'
                data-name='Path 9695'
                d='M103.806,287.22a22.937,22.937,0,0,1-6.119-1.872c-3.76-1.654-6.2-3.276-5.883-5.819a2.773,2.773,0,0,1,2.721-2.73l.993.064c-.1.01-2.654.65-2.918,2.764-.22,1.778,1.3,3.176,5.411,4.985a22.575,22.575,0,0,0,5.885,1.809Z'
                transform='translate(-91.776 -276.799)'
                fill='#d3d3d3'
              />
            </g>
          </g>
          <g
            id='Group_5190'
            data-name='Group 5190'
            transform='translate(17.798 59.678)'
          >
            <path
              id='Path_9696'
              data-name='Path 9696'
              d='M85.576,251.8s-8.417-11.026-11.539-13.827-7.024-.366-5.924,5.013,6.726,14.876,9.16,16.579,6.547-.806,7.54-3.38A9.892,9.892,0,0,0,85.576,251.8Z'
              transform='translate(-67.923 -236.806)'
              fill='#ededed'
            />
            <path
              id='Path_9697'
              data-name='Path 9697'
              d='M73.894,238.021c-.7-.263-4.92-1.332-4.6,3.552s3.9,10.3,6.124,13.122a22.781,22.781,0,0,0,4.754,4.641l1.535-.789a26.288,26.288,0,0,1-5.622-6.315c-3.543-5.584-5.578-9.273-4.569-11.843.78-1.986,4.038-.592,4.038-.592Z'
              transform='translate(-67.251 -236.344)'
              fill='#20b6c7'
            />
            <g
              id='Group_5189'
              data-name='Group 5189'
              transform='translate(5.792 4.326)'
            >
              <path
                id='Path_9698'
                data-name='Path 9698'
                d='M78.92,253.959c-.037-.046-3.818-4.756-5.269-7.144s-2.095-4.485-1.773-5.764a1.613,1.613,0,0,1,.736-1.05c1.648-.97,3.383.692,3.475.771l.87,1.154c-.55-.4-2.76-2.216-4.062-1.447a1.082,1.082,0,0,0-.48.706c-.281,1.118.359,3.116,1.71,5.34,1.431,2.356,5.188,7.037,5.227,7.084Z'
                transform='translate(-71.797 -239.699)'
                fill='#d3d3d3'
              />
            </g>
          </g>
          <g
            id='Group_5196'
            data-name='Group 5196'
            transform='translate(8.399 47.546)'
          >
            <path
              id='Path_9699'
              data-name='Path 9699'
              d='M70.355,241.008c-2.3,1.4-2.8-.2-4.289-2.017s-2.136-2.92-.009-4.337S69.47,232.1,71.1,234s3.617,3.856,2.128,4.648S71.045,240.588,70.355,241.008Z'
              transform='translate(-60.123 -226.558)'
              fill='#1c1c1c'
            />
            <g id='Group_5195' data-name='Group 5195'>
              <g
                id='Group_5191'
                data-name='Group 5191'
                transform='translate(11.712 1.577)'
              >
                <path
                  id='Path_9700'
                  data-name='Path 9700'
                  d='M70.788,233a.734.734,0,0,1-.754-.257,8,8,0,0,1-.239-1.468c-.16-1.268.221-1.8.9-1.4.446.265.722,2.673.722,2.673S71.4,232.9,70.788,233Z'
                  transform='translate(-69.327 -229.746)'
                  fill='#fff'
                />
                <path
                  id='Path_9701'
                  data-name='Path 9701'
                  d='M70.594,235.84c.3.283.925-1.673,1-3.213.055-1.186-1.4-.893-1.776.088C69.259,234.158,69.279,234.616,70.594,235.84Z'
                  transform='translate(-69.47 -228.703)'
                  fill='#fff'
                />
                <path
                  id='Path_9702'
                  data-name='Path 9702'
                  d='M71.078,231.376a.964.964,0,1,0,.538,1.251A.964.964,0,0,0,71.078,231.376Z'
                  transform='translate(-69.328 -228.973)'
                  fill='#1c1c1c'
                />
              </g>
              <path
                id='Path_9703'
                data-name='Path 9703'
                d='M70.706,241c-1.731.86-2.434-.715-3.923-2.527s-2.41-2.736-.283-4.153,3.687-2.734,5.318-.834,2.98,4.784,1.455,5.819A29.4,29.4,0,0,1,70.706,241Z'
                transform='translate(-59.848 -226.788)'
                fill='#282828'
              />
              <g
                id='Group_5192'
                data-name='Group 5192'
                transform='translate(2.356 1.682)'
              >
                <path
                  id='Path_9704'
                  data-name='Path 9704'
                  d='M66.248,232.195a.731.731,0,0,1-.716.347s-.41-.256-1.474-1.091c-1.006-.789-1.109-1.438-.339-1.628.5-.124,2.664,1.61,2.664,1.61S66.616,231.7,66.248,232.195Z'
                  transform='translate(-63.212 -229.816)'
                  fill='#fff'
                />
                <path
                  id='Path_9705'
                  data-name='Path 9705'
                  d='M64.58,231.785a.964.964,0,1,1,.964.964A.965.965,0,0,1,64.58,231.785Z'
                  transform='translate(-62.535 -229.318)'
                  fill='#1c1c1c'
                />
                <path
                  id='Path_9706'
                  data-name='Path 9706'
                  d='M67.437,234.564a1.545,1.545,0,0,0,.556-.625,13.978,13.978,0,0,0-.888-1.383c-.765-1.117-1.195-1.549-1.852-.662-.472.637,1.181,2.938,1.181,2.938S66.723,235.077,67.437,234.564Z'
                  transform='translate(-62.244 -229.022)'
                  fill='#fff'
                />
              </g>
              <g
                id='Group_5193'
                data-name='Group 5193'
                transform='translate(0 3.733)'
              >
                <path
                  id='Path_9707'
                  data-name='Path 9707'
                  d='M64.9,233.223a.735.735,0,0,1-.665.438s-.44-.2-1.6-.887c-1.1-.65-1.29-1.28-.552-1.57.483-.19,2.853,1.245,2.853,1.245S65.2,232.68,64.9,233.223Z'
                  transform='translate(-61.637 -231.187)'
                  fill='#fff'
                />
                <path
                  id='Path_9708'
                  data-name='Path 9708'
                  d='M63.173,233.056a.964.964,0,1,1,1.082.828A.964.964,0,0,1,63.173,233.056Z'
                  transform='translate(-60.88 -230.802)'
                  fill='#1c1c1c'
                />
                <path
                  id='Path_9709'
                  data-name='Path 9709'
                  d='M66.191,235.625a5.744,5.744,0,0,0,.737-.894s-.238-.247-1.144-1.253-1.389-1.378-1.923-.414c-.384.694,1.556,2.757,1.556,2.757S65.55,236.227,66.191,235.625Z'
                  transform='translate(-60.56 -230.536)'
                  fill='#fff'
                />
              </g>
              <g
                id='Group_5194'
                data-name='Group 5194'
                transform='translate(4.81)'
              >
                <path
                  id='Path_9710'
                  data-name='Path 9710'
                  d='M67.663,231.334a.737.737,0,0,1-.752.265s-.378-.3-1.343-1.25c-.911-.9-.94-1.553-.154-1.657.513-.067,2.465,1.9,2.465,1.9S68.084,230.88,67.663,231.334Z'
                  transform='translate(-64.854 -228.691)'
                  fill='#fff'
                />
                <path
                  id='Path_9711'
                  data-name='Path 9711'
                  d='M66.069,230.726a.964.964,0,1,1,.851,1.066A.964.964,0,0,1,66.069,230.726Z'
                  transform='translate(-64.255 -228.107)'
                  fill='#1c1c1c'
                />
                <path
                  id='Path_9712'
                  data-name='Path 9712'
                  d='M68.358,233.966a4.572,4.572,0,0,1,1.056-.4s-.295-.591-.928-1.787-1.014-1.673-1.766-.866c-.541.579.842,3.051.842,3.051S67.591,234.4,68.358,233.966Z'
                  transform='translate(-63.992 -227.787)'
                  fill='#fff'
                />
              </g>
            </g>
          </g>
          <g
            id='Group_5198'
            data-name='Group 5198'
            transform='translate(41.216 46.031)'
          >
            <path
              id='Path_9713'
              data-name='Path 9713'
              d='M101.239,242.675S92.821,231.649,89.7,228.849s-7.024-.366-5.924,5.012,6.728,14.878,9.162,16.581,6.547-.806,7.54-3.38A9.875,9.875,0,0,0,101.239,242.675Z'
              transform='translate(-83.586 -227.678)'
              fill='#ededed'
            />
            <path
              id='Path_9714'
              data-name='Path 9714'
              d='M89.559,228.893c-.7-.263-4.922-1.332-4.6,3.552s3.9,10.3,6.124,13.122a22.763,22.763,0,0,0,4.756,4.641l1.534-.789a26.265,26.265,0,0,1-5.62-6.315c-3.545-5.584-5.58-9.273-4.57-11.843.78-1.986,4.038-.592,4.038-.592Z'
              transform='translate(-82.914 -227.216)'
              fill='#20b6c7'
            />
            <g
              id='Group_5197'
              data-name='Group 5197'
              transform='translate(5.793 4.326)'
            >
              <path
                id='Path_9715'
                data-name='Path 9715'
                d='M94.584,244.831c-.039-.046-3.818-4.756-5.269-7.144s-2.095-4.485-1.775-5.764a1.606,1.606,0,0,1,.737-1.05c1.648-.97,3.383.692,3.473.77l.87,1.156c-.55-.4-2.76-2.216-4.061-1.447a1.084,1.084,0,0,0-.481.706c-.28,1.118.36,3.116,1.712,5.34,1.431,2.356,5.188,7.037,5.225,7.084Z'
                transform='translate(-87.46 -230.571)'
                fill='#d3d3d3'
              />
            </g>
          </g>
          <g
            id='Group_5204'
            data-name='Group 5204'
            transform='translate(31.818 33.899)'
          >
            <path
              id='Path_9716'
              data-name='Path 9716'
              d='M86.017,231.88c-2.3,1.4-2.8-.206-4.288-2.017s-2.136-2.921-.009-4.337,3.413-2.552,5.043-.652,3.618,3.856,2.129,4.648S86.709,231.46,86.017,231.88Z'
              transform='translate(-75.786 -217.43)'
              fill='#1c1c1c'
            />
            <g id='Group_5203' data-name='Group 5203'>
              <g
                id='Group_5199'
                data-name='Group 5199'
                transform='translate(0.353 8.878)'
              >
                <path
                  id='Path_9717'
                  data-name='Path 9717'
                  d='M80.154,227.823a.735.735,0,0,0,.173-.777,8.168,8.168,0,0,0-1.129-.969c-1-.8-1.654-.751-1.663.043-.006.517,1.906,2.008,1.906,2.008S79.752,228.3,80.154,227.823Z'
                  transform='translate(-77.536 -225.501)'
                  fill='#fff'
                />
                <path
                  id='Path_9718'
                  data-name='Path 9718'
                  d='M82.846,228.98c.084.407-1.909-.079-3.262-.821-1.041-.57-.034-1.658,1-1.47C82.106,226.966,82.486,227.218,82.846,228.98Z'
                  transform='translate(-76.739 -224.924)'
                  fill='#fff'
                />
                <path
                  id='Path_9719'
                  data-name='Path 9719'
                  d='M78.7,227.188a.965.965,0,1,1,.788,1.112A.964.964,0,0,1,78.7,227.188Z'
                  transform='translate(-76.965 -225.063)'
                  fill='#1c1c1c'
                />
              </g>
              <path
                id='Path_9720'
                data-name='Path 9720'
                d='M86.369,231.868c-1.731.86-2.433-.715-3.922-2.527s-2.412-2.736-.284-4.153,3.688-2.735,5.318-.834,2.98,4.784,1.456,5.819A29.117,29.117,0,0,1,86.369,231.868Z'
                transform='translate(-75.512 -217.66)'
                fill='#282828'
              />
              <g
                id='Group_5200'
                data-name='Group 5200'
                transform='translate(2.355 1.682)'
              >
                <path
                  id='Path_9721'
                  data-name='Path 9721'
                  d='M81.913,223.067a.732.732,0,0,1-.718.347s-.41-.256-1.474-1.091c-1-.789-1.109-1.438-.339-1.628.5-.124,2.664,1.61,2.664,1.61S82.281,222.567,81.913,223.067Z'
                  transform='translate(-78.875 -220.688)'
                  fill='#fff'
                />
                <path
                  id='Path_9722'
                  data-name='Path 9722'
                  d='M80.244,222.657a.964.964,0,1,1,.964.964A.965.965,0,0,1,80.244,222.657Z'
                  transform='translate(-78.198 -220.19)'
                  fill='#1c1c1c'
                />
                <path
                  id='Path_9723'
                  data-name='Path 9723'
                  d='M83.1,225.436a1.543,1.543,0,0,0,.556-.626,14.255,14.255,0,0,0-.887-1.381c-.765-1.117-1.2-1.549-1.852-.662-.472.637,1.18,2.938,1.18,2.938S82.385,225.949,83.1,225.436Z'
                  transform='translate(-77.907 -219.894)'
                  fill='#fff'
                />
              </g>
              <g
                id='Group_5201'
                data-name='Group 5201'
                transform='translate(0 3.733)'
              >
                <path
                  id='Path_9724'
                  data-name='Path 9724'
                  d='M80.564,224.1a.735.735,0,0,1-.665.438s-.44-.2-1.606-.887c-1.1-.652-1.289-1.28-.55-1.57.481-.19,2.853,1.245,2.853,1.245S80.861,223.552,80.564,224.1Z'
                  transform='translate(-77.3 -222.059)'
                  fill='#fff'
                />
                <path
                  id='Path_9725'
                  data-name='Path 9725'
                  d='M78.836,223.928a.965.965,0,1,1,1.082.828A.964.964,0,0,1,78.836,223.928Z'
                  transform='translate(-76.544 -221.674)'
                  fill='#1c1c1c'
                />
                <path
                  id='Path_9726'
                  data-name='Path 9726'
                  d='M81.854,226.5a5.648,5.648,0,0,0,.737-.894s-.238-.247-1.142-1.253-1.389-1.378-1.924-.414c-.384.694,1.558,2.757,1.558,2.757S81.214,227.1,81.854,226.5Z'
                  transform='translate(-76.224 -221.408)'
                  fill='#fff'
                />
              </g>
              <g
                id='Group_5202'
                data-name='Group 5202'
                transform='translate(4.809)'
              >
                <path
                  id='Path_9727'
                  data-name='Path 9727'
                  d='M83.326,222.206a.735.735,0,0,1-.751.265s-.38-.3-1.344-1.25c-.909-.9-.94-1.553-.154-1.657.514-.067,2.467,1.9,2.467,1.9S83.748,221.752,83.326,222.206Z'
                  transform='translate(-80.517 -219.563)'
                  fill='#fff'
                />
                <path
                  id='Path_9728'
                  data-name='Path 9728'
                  d='M81.733,221.6a.964.964,0,1,1,.849,1.066A.963.963,0,0,1,81.733,221.6Z'
                  transform='translate(-79.918 -218.979)'
                  fill='#1c1c1c'
                />
                <path
                  id='Path_9729'
                  data-name='Path 9729'
                  d='M84.023,224.838a4.573,4.573,0,0,1,1.054-.4s-.295-.591-.928-1.787-1.014-1.673-1.766-.867c-.541.58.843,3.053.843,3.053S83.256,225.267,84.023,224.838Z'
                  transform='translate(-79.655 -218.659)'
                  fill='#fff'
                />
              </g>
            </g>
          </g>
          <g
            id='Group_5210'
            data-name='Group 5210'
            transform='translate(58.56 74.374)'
          >
            <g
              id='Group_5206'
              data-name='Group 5206'
              transform='translate(0 23.23)'
            >
              <path
                id='Path_9730'
                data-name='Path 9730'
                d='M108.117,281.981c-.1-6.234.61-14.386-.921-18.3-2.329-1.843-8.47-.3-9.313,1.5-.908,1.939-1.736,6.306-2.1,10.839-.5,6.3-.776,10.6-.477,14.357.351,4.4,4.747,4.591,8.082,3.833,2.742-.622,3.747-1.66,4.4-5.583A38.385,38.385,0,0,0,108.117,281.981Z'
                transform='translate(-95.186 -261.838)'
                fill='#20b6c7'
              />
              <path
                id='Path_9731'
                data-name='Path 9731'
                d='M99.478,263.238c-2.657,1.305-2.99,2.591-1.628,3.546a6.047,6.047,0,0,0,6.67.046c1.991-1.193,2.314-2.947.87-3.724C103.568,262.124,102.876,261.571,99.478,263.238Z'
                transform='translate(-94.256 -262.172)'
                fill='#11a3aa'
              />
              <g
                id='Group_5205'
                data-name='Group 5205'
                transform='translate(1.544 5.325)'
              >
                <path
                  id='Path_9732'
                  data-name='Path 9732'
                  d='M96.331,292.078c-.422-3.394.425-14.419,1.253-18.976.758-4.174,2.1-4.4,4.224-4.415a10,10,0,0,0,1.861-.138,5.5,5.5,0,0,0,3.774-2.815l.1,1.008c-.036.1-.637,1.933-3.732,2.524a10.784,10.784,0,0,1-1.993.151c-1.821.015-2.823.024-3.512,3.815-.809,4.446-1.6,16.066-1.2,19.318Z'
                  transform='translate(-96.219 -265.734)'
                  fill='#11a3aa'
                />
              </g>
              <path
                id='Path_9733'
                data-name='Path 9733'
                d='M102.05,269.94c0,.647.393.945.879.665a2.107,2.107,0,0,0,.879-1.68c0-.647-.393-.945-.879-.664A2.1,2.1,0,0,0,102.05,269.94Z'
                transform='translate(-91.788 -259.21)'
                fill='#282828'
              />
            </g>
            <g
              id='Group_5207'
              data-name='Group 5207'
              transform='translate(4.248)'
            >
              <path
                id='Path_9734'
                data-name='Path 9734'
                d='M100.652,247.235a17.784,17.784,0,0,1,4.551,3.518c2.261,2.871,1.169,11.207.606,14.156a3.647,3.647,0,0,1-1.207,2.219,8.709,8.709,0,0,1-1.658,1c-1,.52-1.776-2.917-2.154-4.769a80.928,80.928,0,0,1-.975-12.294C99.778,248.749,98.619,246.179,100.652,247.235Z'
                transform='translate(-97.322 -246.458)'
                fill='#ededed'
              />
              <path
                id='Path_9735'
                data-name='Path 9735'
                d='M100.609,246.991c3.8,1.971,5.064,4.769,4.881,9.039s0,7.166-.64,10.521c-.629,3.3-4.823,2.213-5.711-2.135a91.926,91.926,0,0,1-1.108-13.985C97.988,247.8,98.294,245.79,100.609,246.991Z'
                transform='translate(-98.028 -246.635)'
                fill='#fff'
              />
            </g>
            <g
              id='Group_5208'
              data-name='Group 5208'
              transform='translate(4.316 7.724)'
            >
              <path
                id='Path_9736'
                data-name='Path 9736'
                d='M104.158,264a3.043,3.043,0,1,1-3.043-3.044A3.042,3.042,0,0,1,104.158,264Z'
                transform='translate(-98.073 -247.268)'
                fill='#1c1c1c'
              />
              <path
                id='Path_9737'
                data-name='Path 9737'
                d='M99.569,251.8v14.712a.806.806,0,0,0,1.612,0V251.8Z'
                transform='translate(-97.332 -251.801)'
                fill='#20b6c7'
              />
            </g>
            <g
              id='Group_5209'
              data-name='Group 5209'
              transform='translate(2.094 1.537)'
            >
              <path
                id='Path_9738'
                data-name='Path 9738'
                d='M103.515,253.859a7.31,7.31,0,0,0-3.053-5.832,2,2,0,0,0-1.9-.206h0l-.009.006a1.587,1.587,0,0,0-.138.081l-1.094.649.26.561a4.217,4.217,0,0,0-.166,1.22,7.31,7.31,0,0,0,3.051,5.831,3.1,3.1,0,0,0,.486.224l.3.656,1.151-.688.01-.006.055-.034h0A2.706,2.706,0,0,0,103.515,253.859Z'
                transform='translate(-96.226 -247.663)'
                fill='#1c1c1c'
              />
              <path
                id='Path_9739'
                data-name='Path 9739'
                d='M102.691,254.333c0,2.247-1.367,3.28-3.051,2.307a7.308,7.308,0,0,1-3.053-5.832c0-2.247,1.367-3.28,3.053-2.307A7.311,7.311,0,0,1,102.691,254.333Z'
                transform='translate(-96.587 -247.429)'
                fill='#61dfe5'
              />
            </g>
          </g>
          <g
            id='Group_5211'
            data-name='Group 5211'
            transform='translate(42.761 73.519)'
          >
            <path
              id='Path_9740'
              data-name='Path 9740'
              d='M84.623,253.509c0,3.9.988,8.5,5.713,10.3,5.375,2.048,10.907.305,14.661-4.17,3.134-3.736,2.878-10.763,2.878-10.763Z'
              transform='translate(-84.617 -244.67)'
              fill='#fff'
            />
            <path
              id='Path_9741'
              data-name='Path 9741'
              d='M93.107,259.749a12.225,12.225,0,0,1-5.946-1.5h0c-2.483-1.434-3.078-3.37-2.083-5.03,1.815-3.025,8.531-5.557,10.8-6.5a12.018,12.018,0,0,1,9.708.9h0c2.053,1.184,2.588,2.667,2.15,4.058C106.549,255.437,99.069,259.749,93.107,259.749Z'
              transform='translate(-84.619 -246.063)'
              fill='#d3d3d3'
            />
          </g>
          <g
            id='Group_5212'
            data-name='Group 5212'
            transform='translate(54.568 43.448)'
          >
            <path
              id='Path_9742'
              data-name='Path 9742'
              d='M104.16,227.822,92.885,247.435a2.789,2.789,0,1,0,4.835,2.779L108.995,230.6Z'
              transform='translate(-92.516 -225.023)'
              fill='#20b6c7'
            />
            <path
              id='Path_9743'
              data-name='Path 9743'
              d='M109.186,231.908a5.956,5.956,0,1,1-5.956-5.958A5.957,5.957,0,0,1,109.186,231.908Z'
              transform='translate(-90.161 -225.95)'
              fill='#1c1c1c'
            />
          </g>
          <g
            id='Group_5213'
            data-name='Group 5213'
            transform='translate(48.101 52.187)'
          >
            <path
              id='Path_9744'
              data-name='Path 9744'
              d='M88.191,234.041v24.91c0,1.855,2.826,3.358,6.312,3.358s6.312-1.5,6.312-3.358v-24.91Z'
              transform='translate(-88.191 -230.683)'
              fill='#1c1c1c'
            />
            <path
              id='Path_9745'
              data-name='Path 9745'
              d='M94.5,231.795c-3.487,0-6.312,1.5-6.312,3.358s2.826,3.356,6.312,3.356,6.312-1.5,6.312-3.356S97.99,231.795,94.5,231.795Z'
              transform='translate(-88.191 -231.795)'
              fill='#282828'
            />
          </g>
          <g
            id='Group_5215'
            data-name='Group 5215'
            transform='translate(38.427 43.825)'
          >
            <path
              id='Path_9746'
              data-name='Path 9746'
              d='M103.7,233.771c0,6.389-4.905,14.4-10.955,17.892s-10.956,1.147-10.956-5.242,4.905-14.4,10.956-17.892S103.7,227.382,103.7,233.771Z'
              transform='translate(-81.684 -225.851)'
              fill='#fff'
            />
            <path
              id='Path_9747'
              data-name='Path 9747'
              d='M110.784,238.618c0,6.389-4.9,14.4-10.955,17.892s-10.955,1.147-10.955-5.242,4.9-14.4,10.955-17.892S110.784,232.229,110.784,238.618Z'
              transform='translate(-78.178 -223.452)'
              fill='#fff'
            />
            <path
              id='Path_9748'
              data-name='Path 9748'
              d='M86.042,237.544s-4.65,2.575-3.86,6.873,9.356,12.986,17.013,10.738S86.042,237.544,86.042,237.544Z'
              transform='translate(-81.537 -220.587)'
              fill='#fff'
            />
            <path
              id='Path_9749'
              data-name='Path 9749'
              d='M89.122,228.809l21.54,12.488a15.371,15.371,0,0,0-6.315-12.856C97.435,223.519,89.122,228.809,89.122,228.809Z'
              transform='translate(-78.056 -226.131)'
              fill='#fff'
            />
            <g
              id='Group_5214'
              data-name='Group 5214'
              transform='translate(0 16.957)'
            >
              <path
                id='Path_9750'
                data-name='Path 9750'
                d='M93.062,249.173a9.138,9.138,0,0,1-4.458-1.18c-4.354-2.37-6.06-4.514-6.722-5.732a2.761,2.761,0,0,1-.036-1.689c0,.028.9,3.4,7.106,6.78,4.509,2.452,8.749.682,16-3.944,4.176-2.667,8.594-5.346,9.258-5.864l-.182,1.009c-.046.024-4.539,2.823-8.683,5.469C100.262,247.264,96.493,249.173,93.062,249.173Z'
                transform='translate(-81.72 -237.544)'
                fill='#d3d3d3'
              />
            </g>
            <path
              id='Path_9751'
              data-name='Path 9751'
              d='M92.243,243.3c0,.647.393.945.879.665A2.107,2.107,0,0,0,94,242.287c0-.647-.393-.945-.879-.664A2.1,2.1,0,0,0,92.243,243.3Z'
              transform='translate(-76.511 -218.62)'
              fill='#282828'
            />
            <path
              id='Path_9752'
              data-name='Path 9752'
              d='M97.241,244.425c0,.647.395.945.879.664A2.1,2.1,0,0,0,99,243.41c0-.647-.393-.945-.879-.665A2.111,2.111,0,0,0,97.241,244.425Z'
              transform='translate(-74.036 -218.064)'
              fill='#282828'
            />
            <path
              id='Path_9753'
              data-name='Path 9753'
              d='M101.015,238.229c0,.647.393.945.879.665a2.1,2.1,0,0,0,.879-1.679c0-.647-.393-.945-.879-.665A2.106,2.106,0,0,0,101.015,238.229Z'
              transform='translate(-72.168 -221.131)'
              fill='#282828'
            />
            <path
              id='Path_9754'
              data-name='Path 9754'
              d='M84.438,235.6a58.526,58.526,0,0,1,7.878,4.85c2.808,2.253,5.041,4.689,10.812,1.622s5.789-5.671,5.788-8.177a7.6,7.6,0,0,0-2.25-5.381c-2.059-2.033-4.385-3.34-11.506-1.245A20.005,20.005,0,0,0,84.438,235.6Z'
              transform='translate(-80.375 -226.202)'
              fill='#d3d3d3'
            />
          </g>
          <g
            id='Group_5222'
            data-name='Group 5222'
            transform='translate(48.511 36.175)'
          >
            <g
              id='Group_5216'
              data-name='Group 5216'
              transform='translate(0 9.041)'
            >
              <path
                id='Path_9755'
                data-name='Path 9755'
                d='M105.028,230.923h-.776c-1.316-1.506-4.177-2.552-7.505-2.552s-6.19,1.047-7.505,2.552h-.776v1.852c0,2.432,3.708,4.4,8.281,4.4s8.281-1.972,8.281-4.4v-1.852Z'
                transform='translate(-88.465 -226.519)'
                fill='#1c1c1c'
              />
              <path
                id='Path_9756'
                data-name='Path 9756'
                d='M96.746,227.132c-4.573,0-8.281,1.972-8.281,4.4s3.708,4.4,8.281,4.4,8.281-1.972,8.281-4.4S101.32,227.132,96.746,227.132Z'
                transform='translate(-88.465 -227.132)'
                fill='#282828'
              />
            </g>
            <g
              id='Group_5217'
              data-name='Group 5217'
              transform='translate(3.415 9.775)'
            >
              <path
                id='Path_9757'
                data-name='Path 9757'
                d='M100.482,229.851h-.456a7.236,7.236,0,0,0-8.821,0h-.456v1.088c0,1.429,2.178,2.589,4.867,2.589s4.867-1.16,4.867-2.589v-1.088Z'
                transform='translate(-90.749 -227.263)'
                fill='#d3d3d3'
              />
              <path
                id='Path_9758'
                data-name='Path 9758'
                d='M95.616,227.623c-2.688,0-4.867,1.159-4.867,2.588s2.178,2.59,4.867,2.59,4.867-1.16,4.867-2.59S98.3,227.623,95.616,227.623Z'
                transform='translate(-90.749 -227.623)'
                fill='#fff'
              />
            </g>
            <g
              id='Group_5218'
              data-name='Group 5218'
              transform='translate(4.986)'
            >
              <path
                id='Path_9759'
                data-name='Path 9759'
                d='M91.8,222.284v10.528c0,.991,1.509,1.794,3.371,1.794s3.37-.8,3.37-1.794V222.284Z'
                transform='translate(-91.8 -220.491)'
                fill='#d3d3d3'
              />
              <path
                id='Path_9760'
                data-name='Path 9760'
                d='M95.171,221.085c-1.863,0-3.371.8-3.371,1.793s1.509,1.793,3.371,1.793,3.37-.8,3.37-1.793S97.033,221.085,95.171,221.085Z'
                transform='translate(-91.8 -221.085)'
                fill='#fff'
              />
            </g>
            <g
              id='Group_5219'
              data-name='Group 5219'
              transform='translate(4.738 8.329)'
            >
              <path
                id='Path_9761'
                data-name='Path 9761'
                d='M91.634,226.656v8.221c0,.2.3.356.668.356s.67-.159.67-.356v-8.221Z'
                transform='translate(-91.634 -226.656)'
                fill='#20b6c7'
              />
              <path
                id='Path_9762'
                data-name='Path 9762'
                d='M92.3,226.656c-.369,0-.668.158-.668.356s.3.356.668.356.67-.16.67-.356S92.673,226.656,92.3,226.656Z'
                transform='translate(-91.634 -226.656)'
                fill='#61dfe5'
              />
            </g>
            <g
              id='Group_5220'
              data-name='Group 5220'
              transform='translate(7.903 8.684)'
            >
              <path
                id='Path_9763'
                data-name='Path 9763'
                d='M93.751,226.894v8.222c0,.2.3.356.668.356s.67-.158.67-.356v-8.222Z'
                transform='translate(-93.751 -226.893)'
                fill='#20b6c7'
              />
              <ellipse
                id='Ellipse_103'
                data-name='Ellipse 103'
                cx='0.669'
                cy='0.356'
                rx='0.669'
                ry='0.356'
                fill='#61dfe5'
              />
            </g>
            <g
              id='Group_5221'
              data-name='Group 5221'
              transform='translate(14 5.107)'
            >
              <path
                id='Path_9764'
                data-name='Path 9764'
                d='M97.829,224.5v8.223c0,.2.3.356.67.356s.67-.16.67-.356V224.5Z'
                transform='translate(-97.829 -224.501)'
                fill='#20b6c7'
              />
              <path
                id='Path_9765'
                data-name='Path 9765'
                d='M98.5,224.5c-.369,0-.67.16-.67.356s.3.356.67.356.67-.158.67-.356S98.868,224.5,98.5,224.5Z'
                transform='translate(-97.829 -224.501)'
                fill='#61dfe5'
              />
            </g>
          </g>
          <g
            id='Group_5225'
            data-name='Group 5225'
            transform='translate(43.108 22.744)'
          >
            <path
              id='Path_9766'
              data-name='Path 9766'
              d='M110.11,218.607c-3.753-4.282-7.588-6.278-11.254-6.487a12,12,0,0,0-5.81,1.172c-2.4,1.12-3.923,1.6-6.424,5.654a10.088,10.088,0,0,0-.963,9.679,13.481,13.481,0,0,0,4.144,4.789,15.489,15.489,0,0,0,7.324,3.074c1.066.167,4.989.181,7.007-1.015a14.665,14.665,0,0,0,6.484-7.154C112.948,222.877,110.626,219.2,110.11,218.607Z'
              transform='translate(-84.852 -212.102)'
              fill='#fff'
            />
            <g
              id='Group_5224'
              data-name='Group 5224'
              transform='translate(2.994 13.984)'
            >
              <path
                id='Path_9767'
                data-name='Path 9767'
                d='M91.84,225.475a4.75,4.75,0,0,0-1.984-3.79,1.272,1.272,0,0,0-1.25-.115l-1.317.737.616,1.344a5.114,5.114,0,0,0,1.265,2.755l.653,1.423,1.429-.816h0A1.859,1.859,0,0,0,91.84,225.475Z'
                transform='translate(-86.639 -221.455)'
                fill='#1c1c1c'
              />
              <path
                id='Path_9768'
                data-name='Path 9768'
                d='M90.822,225.948c0,1.461-.89,2.132-1.984,1.5a4.75,4.75,0,0,1-1.984-3.79c0-1.461.888-2.132,1.984-1.5A4.754,4.754,0,0,1,90.822,225.948Z'
                transform='translate(-86.854 -221.225)'
                fill='#61dfe5'
              />
              <g
                id='Group_5223'
                data-name='Group 5223'
                transform='translate(0.317 1.285)'
              >
                <path
                  id='Path_9769'
                  data-name='Path 9769'
                  d='M88.851,226.544a3.8,3.8,0,0,1-1.588-3.035,1.6,1.6,0,0,1,.363-1.123,1.385,1.385,0,0,0-.561,1.278,3.806,3.806,0,0,0,1.588,3.035.936.936,0,0,0,1.224-.078A1.006,1.006,0,0,1,88.851,226.544Z'
                  transform='translate(-87.066 -222.279)'
                  fill='#11a3aa'
                />
                <path
                  id='Path_9770'
                  data-name='Path 9770'
                  d='M88.588,222.5a1.006,1.006,0,0,0-1.027-.078,1.6,1.6,0,0,0-.363,1.123,3.8,3.8,0,0,0,1.588,3.035,1.006,1.006,0,0,0,1.027.078,1.6,1.6,0,0,0,.365-1.123A3.805,3.805,0,0,0,88.588,222.5Z'
                  transform='translate(-87.001 -222.314)'
                  fill='#20b6c7'
                />
              </g>
            </g>
            <path
              id='Path_9771'
              data-name='Path 9771'
              d='M97.918,236.072a9.493,9.493,0,0,0,.579-5.007c-.7-5.154-1.733-7-6.178-9.314s-6.036-4.47-2.084-6.967,9.594-2.515,14.33,1.513c5.212,4.431,5.524,9.815,3.82,12.956a15.854,15.854,0,0,1-1.975,2.733s2.252-5.725,1.072-9.119-4.733-8.447-10.857-8.447c-4.8,0-7.644,1.64-6.834,3.5.585,1.347,3.591,2.38,6.079,4.209,4.113,3.025,4.609,6.435,4.672,9.543a11.262,11.262,0,0,1-.619,4.188A3.672,3.672,0,0,1,97.918,236.072Z'
              transform='translate(-83.303 -211.627)'
              fill='#20b6c7'
            />
          </g>
          <g
            id='Group_5232'
            data-name='Group 5232'
            transform='translate(38.141 86.956)'
          >
            <g
              id='Group_5227'
              data-name='Group 5227'
              transform='translate(1.671 21.693)'
            >
              <path
                id='Path_9772'
                data-name='Path 9772'
                d='M95.578,289.369c-.1-6.234.609-14.386-.921-18.3-2.329-1.843-8.471-.3-9.314,1.5-.908,1.939-1.734,6.306-2.095,10.839-.5,6.3-.776,10.6-.477,14.357.35,4.4,4.745,4.59,8.082,3.833,2.74-.622,3.745-1.66,4.394-5.583A38.215,38.215,0,0,0,95.578,289.369Z'
                transform='translate(-82.647 -269.225)'
                fill='#20b6c7'
              />
              <path
                id='Path_9773'
                data-name='Path 9773'
                d='M86.939,270.626c-2.658,1.305-2.99,2.591-1.63,3.546a6.047,6.047,0,0,0,6.67.046c1.991-1.193,2.314-2.947.872-3.724C91.028,269.512,90.337,268.958,86.939,270.626Z'
                transform='translate(-81.717 -269.56)'
                fill='#11a3aa'
              />
              <g
                id='Group_5226'
                data-name='Group 5226'
                transform='translate(1.543 5.326)'
              >
                <path
                  id='Path_9774'
                  data-name='Path 9774'
                  d='M83.792,299.466c-.423-3.4.423-14.42,1.253-18.977.758-4.173,2.1-4.4,4.224-4.414a10.014,10.014,0,0,0,1.861-.138,5.5,5.5,0,0,0,3.774-2.815L95,274.13c-.036.1-.635,1.932-3.732,2.524a10.9,10.9,0,0,1-1.991.151c-1.821.015-2.824.024-3.513,3.815-.809,4.446-1.6,16.066-1.2,19.318Z'
                  transform='translate(-83.68 -273.122)'
                  fill='#11a3aa'
                />
              </g>
              <path
                id='Path_9775'
                data-name='Path 9775'
                d='M89.511,277.328c0,.647.393.945.879.665a2.111,2.111,0,0,0,.879-1.68c0-.647-.395-.945-.879-.664A2.1,2.1,0,0,0,89.511,277.328Z'
                transform='translate(-79.249 -266.598)'
                fill='#282828'
              />
            </g>
            <g
              id='Group_5228'
              data-name='Group 5228'
              transform='translate(5.986 6.187)'
            >
              <path
                id='Path_9776'
                data-name='Path 9776'
                d='M91.62,271.389a3.043,3.043,0,1,1-3.042-3.044A3.042,3.042,0,0,1,91.62,271.389Z'
                transform='translate(-85.533 -254.656)'
                fill='#1c1c1c'
              />
              <path
                id='Path_9777'
                data-name='Path 9777'
                d='M87.03,259.189V273.9a.806.806,0,0,0,1.612,0V259.189Z'
                transform='translate(-84.792 -259.189)'
                fill='#20b6c7'
              />
            </g>
            <g
              id='Group_5229'
              data-name='Group 5229'
              transform='translate(3.765)'
            >
              <path
                id='Path_9778'
                data-name='Path 9778'
                d='M90.974,261.247a7.313,7.313,0,0,0-3.051-5.832,2,2,0,0,0-1.9-.206v0l-.009.006a1.579,1.579,0,0,0-.138.081l-1.093.649.26.561a4.216,4.216,0,0,0-.166,1.22,7.31,7.31,0,0,0,3.051,5.831,3.149,3.149,0,0,0,.484.224l.3.656,1.15-.688.01-.006.057-.034h0A2.708,2.708,0,0,0,90.974,261.247Z'
                transform='translate(-83.687 -255.051)'
                fill='#1c1c1c'
              />
              <path
                id='Path_9779'
                data-name='Path 9779'
                d='M90.152,261.721c0,2.247-1.367,3.28-3.053,2.307a7.311,7.311,0,0,1-3.051-5.832c0-2.247,1.367-3.28,3.051-2.307A7.308,7.308,0,0,1,90.152,261.721Z'
                transform='translate(-84.048 -254.817)'
                fill='#61dfe5'
              />
            </g>
            <g
              id='Group_5231'
              data-name='Group 5231'
              transform='translate(0 1.675)'
            >
              <path
                id='Path_9780'
                data-name='Path 9780'
                d='M84.156,256.771a17.869,17.869,0,0,1,4.55,3.518c2.261,2.871,1.169,11.207.606,14.157a3.66,3.66,0,0,1-1.205,2.219,8.936,8.936,0,0,1-1.66,1c-1,.52-1.776-2.918-2.154-4.771a80.931,80.931,0,0,1-.975-12.294C83.28,258.285,82.121,255.717,84.156,256.771Z'
                transform='translate(-80.824 -255.995)'
                fill='#ededed'
              />
              <path
                id='Path_9781'
                data-name='Path 9781'
                d='M84.111,256.527c3.8,1.971,5.064,4.769,4.881,9.039s0,7.166-.64,10.521c-.629,3.3-4.823,2.213-5.711-2.135a91.924,91.924,0,0,1-1.108-13.985C81.49,257.333,81.8,255.328,84.111,256.527Z'
                transform='translate(-81.53 -256.171)'
                fill='#fff'
              />
              <g
                id='Group_5230'
                data-name='Group 5230'
                transform='translate(0.457 2.343)'
              >
                <path
                  id='Path_9782'
                  data-name='Path 9782'
                  d='M85.144,275.244a1.857,1.857,0,0,1-.589-.1,3.706,3.706,0,0,1-2.071-2.169,10.341,10.341,0,0,1-.626-4.34c0,.052.5,5.157,2.884,5.943a.91.91,0,0,0,.873-.081c1.275-.924,1.274-5.665,1.272-10.685v-.577c0-2.159-.084-4.01-.19-5.494.163.208.163.208.65.8.081,1.337.136,2.912.136,4.693v.577c0,5.625,0,10.066-1.518,11.168A1.368,1.368,0,0,1,85.144,275.244Z'
                  transform='translate(-81.835 -257.738)'
                  fill='#d3d3d3'
                />
              </g>
              <path
                id='Path_9783'
                data-name='Path 9783'
                d='M84.125,267.254c0,.647-.393.945-.879.664a2.1,2.1,0,0,1-.879-1.679c0-.647.393-.945.879-.665A2.107,2.107,0,0,1,84.125,267.254Z'
                transform='translate(-81.115 -251.568)'
                fill='#282828'
              />
            </g>
          </g>
          <g
            id='Group_5233'
            data-name='Group 5233'
            transform='translate(28.47 58.07)'
          >
            <path
              id='Path_9784'
              data-name='Path 9784'
              d='M82.635,252.056a3.787,3.787,0,1,1-3.787-3.789A3.787,3.787,0,0,1,82.635,252.056Z'
              transform='translate(-75.061 -229.523)'
              fill='#1c1c1c'
            />
            <path
              id='Path_9785'
              data-name='Path 9785'
              d='M87.555,236.693,76.76,254.517c-.783,1.317-1.272,3.22.861,4.346,1.879.99,3.149-.179,3.932-1.5l10.8-17.825Z'
              transform='translate(-74.55 -235.253)'
              fill='#20b6c7'
            />
            <path
              id='Path_9786'
              data-name='Path 9786'
              d='M92.267,241.686a5.956,5.956,0,1,1-5.956-5.956A5.957,5.957,0,0,1,92.267,241.686Z'
              transform='translate(-72.44 -235.73)'
              fill='#1c1c1c'
            />
          </g>
        </g>
      </g>
    </svg>
  )
}

export default Svg5
