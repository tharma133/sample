import React from 'react'

// xmlns:xlink='http://www.w3.org/1999/xlink'

function Svg11() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='119.885'
      height='93.76'
      viewBox='0 0 119.885 93.76'
    >
      <defs>
        <clipPath id='clip-path'>
          <path
            id='Path_9800'
            data-name='Path 9800'
            d='M1523.2-509.452l-.005,2.079c-.009,2.947-1.945,5.889-5.811,8.136-7.788,4.525-20.454,4.525-28.292,0-3.942-2.276-5.912-5.265-5.9-8.25l.007-2.079c-.009,2.984,1.96,5.972,5.9,8.25,7.837,4.525,20.5,4.525,28.291,0C1521.26-503.565,1523.2-506.505,1523.2-509.452Z'
            transform='translate(-1483.191 509.566)'
            fill='#1f1f24'
          />
        </clipPath>
        <clipPath id='clip-path-2'>
          <path
            id='Path_9825'
            data-name='Path 9825'
            d='M1523.2-517.809l-.005,2.079c-.009,2.945-1.945,5.887-5.811,8.134-7.788,4.526-20.454,4.526-28.292,0-3.942-2.276-5.912-5.264-5.9-8.248l.007-2.079c-.009,2.983,1.96,5.972,5.9,8.248,7.837,4.525,20.5,4.525,28.291,0C1521.26-511.922,1523.2-514.864,1523.2-517.809Z'
            transform='translate(-1483.191 517.923)'
            fill='#1f1f24'
          />
        </clipPath>
        <clipPath id='clip-path-3'>
          <path
            id='Path_9839'
            data-name='Path 9839'
            d='M1477.6-532.71l-.015,5.05a2,2,0,0,1-1.168,1.636,6.267,6.267,0,0,1-5.687,0,2.012,2.012,0,0,1-1.187-1.659l.015-5.05a2.011,2.011,0,0,0,1.186,1.659,6.267,6.267,0,0,0,5.687,0A2,2,0,0,0,1477.6-532.71Z'
            transform='translate(-1469.545 532.734)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-4'>
          <path
            id='Path_9844'
            data-name='Path 9844'
            d='M1475.695-534.921l-.007,2.079a1.328,1.328,0,0,1-.78,1.089,4.158,4.158,0,0,1-3.779,0,1.336,1.336,0,0,1-.788-1.1l.005-2.079a1.335,1.335,0,0,0,.788,1.1,4.162,4.162,0,0,0,3.781,0A1.334,1.334,0,0,0,1475.695-534.921Z'
            transform='translate(-1470.341 534.935)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-5'>
          <path
            id='Path_9849'
            data-name='Path 9849'
            d='M1475.693-536.232l0,1.187a1.327,1.327,0,0,1-.778,1.087,4.156,4.156,0,0,1-3.781,0,1.341,1.341,0,0,1-.79-1.1l0-1.187a1.341,1.341,0,0,0,.79,1.1,4.167,4.167,0,0,0,3.781,0A1.329,1.329,0,0,0,1475.693-536.232Z'
            transform='translate(-1470.341 536.246)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-6'>
          <path
            id='Path_9872'
            data-name='Path 9872'
            d='M1523.381-532.662l-.015,5.05a2,2,0,0,1-1.168,1.636,6.267,6.267,0,0,1-5.687,0,2.013,2.013,0,0,1-1.187-1.658l.015-5.05a2.011,2.011,0,0,0,1.186,1.658,6.267,6.267,0,0,0,5.687,0A2,2,0,0,0,1523.381-532.662Z'
            transform='translate(-1515.323 532.684)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-7'>
          <path
            id='Path_9877'
            data-name='Path 9877'
            d='M1521.473-534.872l-.007,2.079a1.332,1.332,0,0,1-.78,1.089,4.158,4.158,0,0,1-3.779,0,1.336,1.336,0,0,1-.788-1.1l.005-2.079a1.336,1.336,0,0,0,.788,1.1,4.161,4.161,0,0,0,3.781,0A1.332,1.332,0,0,0,1521.473-534.872Z'
            transform='translate(-1516.119 534.886)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-8'>
          <path
            id='Path_9882'
            data-name='Path 9882'
            d='M1521.471-536.183l0,1.187a1.325,1.325,0,0,1-.778,1.087,4.156,4.156,0,0,1-3.781,0,1.341,1.341,0,0,1-.79-1.1l0-1.189a1.341,1.341,0,0,0,.79,1.1,4.161,4.161,0,0,0,3.781,0A1.328,1.328,0,0,0,1521.471-536.183Z'
            transform='translate(-1516.119 536.197)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-9'>
          <path
            id='Path_9905'
            data-name='Path 9905'
            d='M1477.6-506.3l-.015,5.05a2,2,0,0,1-1.168,1.634,6.256,6.256,0,0,1-5.687,0,2.009,2.009,0,0,1-1.187-1.658l.015-5.05a2.009,2.009,0,0,0,1.186,1.658,6.257,6.257,0,0,0,5.687,0A2,2,0,0,0,1477.6-506.3Z'
            transform='translate(-1469.545 506.324)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-10'>
          <path
            id='Path_9910'
            data-name='Path 9910'
            d='M1475.695-508.512l-.007,2.079a1.333,1.333,0,0,1-.78,1.09,4.163,4.163,0,0,1-3.779,0,1.337,1.337,0,0,1-.788-1.1l.005-2.081a1.335,1.335,0,0,0,.788,1.1,4.162,4.162,0,0,0,3.781,0A1.333,1.333,0,0,0,1475.695-508.512Z'
            transform='translate(-1470.341 508.526)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-11'>
          <path
            id='Path_9915'
            data-name='Path 9915'
            d='M1475.693-509.823l0,1.189a1.327,1.327,0,0,1-.778,1.087,4.161,4.161,0,0,1-3.781,0,1.341,1.341,0,0,1-.79-1.1l0-1.189a1.341,1.341,0,0,0,.79,1.1,4.156,4.156,0,0,0,3.781,0A1.327,1.327,0,0,0,1475.693-509.823Z'
            transform='translate(-1470.341 509.837)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-12'>
          <path
            id='Path_9938'
            data-name='Path 9938'
            d='M1523.381-506.713l-.015,5.05a2,2,0,0,1-1.168,1.636,6.267,6.267,0,0,1-5.687,0,2.011,2.011,0,0,1-1.187-1.658l.015-5.05a2.011,2.011,0,0,0,1.186,1.658,6.267,6.267,0,0,0,5.687,0A2,2,0,0,0,1523.381-506.713Z'
            transform='translate(-1515.323 506.735)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-13'>
          <path
            id='Path_9943'
            data-name='Path 9943'
            d='M1521.473-508.923l-.007,2.079a1.332,1.332,0,0,1-.78,1.089,4.158,4.158,0,0,1-3.779,0,1.336,1.336,0,0,1-.788-1.1l.005-2.079a1.336,1.336,0,0,0,.788,1.1,4.161,4.161,0,0,0,3.781,0A1.332,1.332,0,0,0,1521.473-508.923Z'
            transform='translate(-1516.119 508.937)'
            fill='none'
          />
        </clipPath>
        <clipPath id='clip-path-14'>
          <path
            id='Path_9948'
            data-name='Path 9948'
            d='M1521.471-510.234l0,1.187a1.329,1.329,0,0,1-.778,1.089,4.162,4.162,0,0,1-3.781,0,1.341,1.341,0,0,1-.79-1.1l0-1.189a1.341,1.341,0,0,0,.79,1.1,4.161,4.161,0,0,0,3.781,0A1.328,1.328,0,0,0,1521.471-510.234Z'
            transform='translate(-1516.119 510.248)'
            fill='none'
          />
        </clipPath>
      </defs>
      <g
        id='Group_5596'
        data-name='Group 5596'
        transform='translate(-1459.627 537.914)'
      >
        <g
          id='Group_5238'
          data-name='Group 5238'
          transform='translate(1511.969 -498.699)'
        >
          <path
            id='Path_9787'
            data-name='Path 9787'
            d='M1511.588-468.679l-10.075-5.817c-5.461-3.152-9.758-11.12-9.2-17.051l1.918-20.282,23.631,13.644.093,1.084,1.8-.311-.16-1.875-26.915-15.538-2.184,23.107c-.63,6.667,3.995,15.276,10.1,18.8l10.076,5.817a12.722,12.722,0,0,0,1.491.735,17.883,17.883,0,0,1,.841-1.622A10.563,10.563,0,0,1,1511.588-468.679Z'
            transform='translate(-1490.443 514.826)'
            fill='#28292f'
          />
        </g>
        <g
          id='Group_5239'
          data-name='Group 5239'
          transform='translate(1498.056 -498.699)'
        >
          <path
            id='Path_9788'
            data-name='Path 9788'
            d='M1511.485-491.719l-2.186-23.107-26.913,15.538-.134,1.573c.606-.017,1.221-.07,1.839-.136l.029-.335,23.633-13.644,1.918,20.282c.56,5.931-3.737,13.9-9.2,17.051l-10.077,5.817.457.79-.457-.79a9.882,9.882,0,0,1-2.018.9c.484.486,1,.932,1.495,1.389a12.793,12.793,0,0,0,1.435-.713l10.076-5.817C1507.49-476.443,1512.115-485.053,1511.485-491.719Z'
            transform='translate(-1482.252 514.826)'
            fill='#28292f'
          />
        </g>
        <g
          id='Group_5251'
          data-name='Group 5251'
          transform='translate(1495.498 -496.254)'
        >
          <g id='Group_5247' data-name='Group 5247'>
            <g
              id='Group_5246'
              data-name='Group 5246'
              style='isolation: isolate'
            >
              <g
                id='Group_5241'
                data-name='Group 5241'
                transform='translate(24.274 13.937)'
              >
                <g id='Group_5240' data-name='Group 5240'>
                  <path
                    id='Path_9789'
                    data-name='Path 9789'
                    d='M1518.979-505.181l-.125,24.391-23.817,13.772.125-24.39Z'
                    transform='translate(-1495.037 505.181)'
                    fill='#e0b477'
                  />
                </g>
              </g>
              <g
                id='Group_5243'
                data-name='Group 5243'
                transform='translate(0 13.608)'
              >
                <g id='Group_5242' data-name='Group 5242'>
                  <path
                    id='Path_9790'
                    data-name='Path 9790'
                    d='M1505.145-491.273l-.125,24.39-24.274-14.1.125-24.39Z'
                    transform='translate(-1480.746 505.375)'
                    fill='#bb884f'
                  />
                </g>
              </g>
              <g
                id='Group_5245'
                data-name='Group 5245'
                transform='translate(0.228)'
              >
                <g id='Group_5244' data-name='Group 5244'>
                  <path
                    id='Path_9791'
                    data-name='Path 9791'
                    d='M1528.971-499.285l-23.817,13.772-24.274-14.1,23.817-13.772Z'
                    transform='translate(-1480.88 513.387)'
                    fill='#efcd90'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5249'
            data-name='Group 5249'
            transform='translate(9.747 5.194)'
          >
            <g id='Group_5248' data-name='Group 5248'>
              <path
                id='Path_9792'
                data-name='Path 9792'
                d='M1491.413-510.329l-4.928,2.85,24.273,14.1,4.928-2.85Z'
                transform='translate(-1486.484 510.329)'
                fill='#f7ecea'
              />
            </g>
          </g>
          <g
            id='Group_5250'
            data-name='Group 5250'
            transform='translate(34.019 19.296)'
          >
            <path
              id='Path_9793'
              data-name='Path 9793'
              d='M1500.786-499.177l4.918-2.849-.01,4.967-4.919,2.825Z'
              transform='translate(-1500.775 502.026)'
              fill='#c2ab9d'
            />
          </g>
        </g>
        <g
          id='Group_5256'
          data-name='Group 5256'
          transform='translate(1496.044 -481.724)'
        >
          <g
            id='Group_5253'
            data-name='Group 5253'
            transform='translate(37.65 0.438)'
          >
            <g id='Group_5252' data-name='Group 5252'>
              <path
                id='Path_9794'
                data-name='Path 9794'
                d='M1510.825-504.574l-1.8.311,1.918,22.505c.263,3.084-.54,5.459-2.206,6.517a5.051,5.051,0,0,1-4.659.092,17.881,17.881,0,0,0-.841,1.622,8.645,8.645,0,0,0,3.3.722,5.842,5.842,0,0,0,3.178-.9c2.257-1.435,3.367-4.428,3.044-8.211Z'
                transform='translate(-1503.234 504.574)'
                fill='#28292f'
              />
            </g>
          </g>
          <g id='Group_5255' data-name='Group 5255'>
            <g id='Group_5254' data-name='Group 5254'>
              <path
                id='Path_9795'
                data-name='Path 9795'
                d='M1485.143-475.061c-1.664-1.057-2.468-3.433-2.2-6.517l1.98-23.254c-.618.066-1.233.119-1.839.136l-1.957,22.964c-.323,3.783.787,6.775,3.044,8.211a5.842,5.842,0,0,0,3.178.9,8.719,8.719,0,0,0,3.356-.744c-.493-.457-1.01-.9-1.495-1.389A4.759,4.759,0,0,1,1485.143-475.061Z'
                transform='translate(-1481.068 504.832)'
                fill='#28292f'
              />
            </g>
          </g>
        </g>
        <g
          id='Group_5266'
          data-name='Group 5266'
          transform='translate(1499.651 -501.295)'
        >
          <g id='Group_5265' data-name='Group 5265' style='isolation: isolate'>
            <g
              id='Group_5262'
              data-name='Group 5262'
              transform='translate(0 11.53)'
              style='isolation: isolate'
            >
              <path
                id='Path_9796'
                data-name='Path 9796'
                d='M1523.2-509.452l-.005,2.079c-.009,2.947-1.945,5.889-5.811,8.136-7.788,4.525-20.454,4.525-28.292,0-3.942-2.276-5.912-5.265-5.9-8.25l.007-2.079c-.009,2.984,1.96,5.972,5.9,8.25,7.837,4.525,20.5,4.525,28.291,0C1521.26-503.565,1523.2-506.505,1523.2-509.452Z'
                transform='translate(-1483.191 509.566)'
                fill='#1f1f24'
              />
              <g
                id='Group_5261'
                data-name='Group 5261'
                clipPath='url(#clip-path)'
              >
                <g
                  id='Group_5260'
                  data-name='Group 5260'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5257'
                    data-name='Group 5257'
                    transform='translate(37.878 0.114)'
                  >
                    <path
                      id='Path_9797'
                      data-name='Path 9797'
                      d='M1507.626-509.5l-.005,2.079a7.926,7.926,0,0,1-2.128,5.186l.005-2.079a7.909,7.909,0,0,0,2.128-5.186Z'
                      transform='translate(-1505.492 509.499)'
                      fill='#1f1f24'
                    />
                  </g>
                  <g
                    id='Group_5258'
                    data-name='Group 5258'
                    transform='translate(24.253 5.299)'
                  >
                    <path
                      id='Path_9798'
                      data-name='Path 9798'
                      d='M1511.1-506.446l-.005,2.079a14.536,14.536,0,0,1-3.682,2.95,26.411,26.411,0,0,1-9.943,3.139l.007-2.081a26.414,26.414,0,0,0,9.941-3.139,14.471,14.471,0,0,0,3.682-2.949Z'
                      transform='translate(-1497.47 506.446)'
                      fill='#1f1f24'
                    />
                  </g>
                  <g id='Group_5259' data-name='Group 5259'>
                    <path
                      id='Path_9799'
                      data-name='Path 9799'
                      d='M1507.451-498.179l-.007,2.081c-6.4.79-13.355-.257-18.349-3.139-3.942-2.276-5.912-5.265-5.9-8.25l.007-2.079c-.009,2.984,1.96,5.972,5.9,8.25,4.994,2.882,11.946,3.929,18.349,3.137Z'
                      transform='translate(-1483.191 509.566)'
                      fill='#1f1f24'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5264'
              data-name='Group 5264'
              transform='translate(0.006)'
            >
              <g id='Group_5263' data-name='Group 5263'>
                <path
                  id='Path_9801'
                  data-name='Path 9801'
                  d='M1517.294-512.962c7.839,4.525,7.879,11.862.1,16.386s-20.453,4.526-28.29,0-7.878-11.861-.09-16.387S1509.459-517.485,1517.294-512.962Z'
                  transform='translate(-1483.195 516.354)'
                  fill='#28292f'
                />
              </g>
            </g>
          </g>
        </g>
        <g
          id='Group_5286'
          data-name='Group 5286'
          transform='translate(1474.668 -527.491)'
        >
          <g
            id='Group_5267'
            data-name='Group 5267'
            transform='translate(87.48 4.186)'
          >
            <path
              id='Path_9802'
              data-name='Path 9802'
              d='M1522.24-529.313l-.019,6.67a3.84,3.84,0,0,1-2.235,3.113l.021-6.672A3.835,3.835,0,0,0,1522.24-529.313Z'
              transform='translate(-1519.986 529.313)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5268'
            data-name='Group 5268'
            transform='translate(88.893 4.186)'
          >
            <path
              id='Path_9803'
              data-name='Path 9803'
              d='M1521.659-529.313l-.019,6.67a3.045,3.045,0,0,1-.822,1.981l.019-6.672a3.028,3.028,0,0,0,.822-1.979Z'
              transform='translate(-1520.818 529.313)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5269'
            data-name='Group 5269'
            transform='translate(87.48 6.165)'
          >
            <path
              id='Path_9804'
              data-name='Path 9804'
              d='M1521.418-528.148l-.019,6.672a5.611,5.611,0,0,1-1.413,1.133l.021-6.672a5.523,5.523,0,0,0,1.411-1.133Z'
              transform='translate(-1519.986 528.148)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5270'
            data-name='Group 5270'
            transform='translate(0 4.2)'
          >
            <path
              id='Path_9805'
              data-name='Path 9805'
              d='M1470.644-526.3l-.019,6.67a3.64,3.64,0,0,1-2.143-3.006l.019-6.67a3.642,3.642,0,0,0,2.143,3.006Z'
              transform='translate(-1468.482 529.305)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5271'
            data-name='Group 5271'
            transform='translate(84.643 7.298)'
          >
            <path
              id='Path_9806'
              data-name='Path 9806'
              d='M1521.173-527.481l-.019,6.671-2.837,1.649.019-6.671Z'
              transform='translate(-1518.316 527.481)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5272'
            data-name='Group 5272'
            transform='translate(2.143 7.206)'
          >
            <path
              id='Path_9807'
              data-name='Path 9807'
              d='M1472.779-525.794l-.019,6.671-3.016-1.741.019-6.671Z'
              transform='translate(-1469.744 527.535)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5273'
            data-name='Group 5273'
            transform='translate(72.552 8.947)'
          >
            <path
              id='Path_9808'
              data-name='Path 9808'
              d='M1511.2-502.911l.019-6.672c.017-6.13,4.046-12.251,12.092-16.927l-.019,6.67C1515.241-515.162,1511.214-509.043,1511.2-502.911Z'
              transform='translate(-1511.197 526.51)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5274'
            data-name='Group 5274'
            transform='translate(76.982 8.947)'
          >
            <path
              id='Path_9809'
              data-name='Path 9809'
              d='M1521.486-526.51l-.019,6.67a30.139,30.139,0,0,0-7.662,6.138l.019-6.672a30.169,30.169,0,0,1,7.662-6.137Z'
              transform='translate(-1513.805 526.51)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5275'
            data-name='Group 5275'
            transform='translate(72.552 15.084)'
          >
            <path
              id='Path_9810'
              data-name='Path 9810'
              d='M1515.646-522.9l-.019,6.672a16.478,16.478,0,0,0-4.43,10.791l.019-6.672a16.478,16.478,0,0,1,4.43-10.791Z'
              transform='translate(-1511.197 522.897)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5276'
            data-name='Group 5276'
            transform='translate(5.16 8.947)'
          >
            <path
              id='Path_9811'
              data-name='Path 9811'
              d='M1483.826-509.343l-.019,6.67c.017-6.21-4.082-12.428-12.287-17.165l.019-6.672C1479.744-521.771,1483.843-515.553,1483.826-509.343Z'
              transform='translate(-1471.52 526.51)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5277'
            data-name='Group 5277'
            transform='translate(5.16 8.947)'
          >
            <path
              id='Path_9812'
              data-name='Path 9812'
              d='M1483.826-509.343l-.019,6.67c.017-6.21-4.082-12.428-12.287-17.165l.019-6.672c8.206,4.739,12.3,10.957,12.287,17.167Z'
              transform='translate(-1471.52 526.51)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5278'
            data-name='Group 5278'
            transform='translate(15.696 39.661)'
          >
            <path
              id='Path_9813'
              data-name='Path 9813'
              d='M1536.607-499.072l-.019,6.426c-7.374-5.7-19.7-7.253-30.787-7.052-10.208.187-21.05,1.707-28.078,7.049l.02-6.424c8.073-4.691,19.135-9.338,29.751-9.354C1518.183-508.444,1528.425-503.8,1536.607-499.072Z'
              transform='translate(-1477.723 508.427)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5279'
            data-name='Group 5279'
            transform='translate(74.561 49.016)'
          >
            <path
              id='Path_9814'
              data-name='Path 9814'
              d='M1515.415-501.178l-.019,6.425-3.016-1.742.019-6.425Z'
              transform='translate(-1512.38 502.919)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5280'
            data-name='Group 5280'
            transform='translate(12.7 49.014)'
          >
            <path
              id='Path_9815'
              data-name='Path 9815'
              d='M1478.975-502.92l-.019,6.425-3,1.742.019-6.425Z'
              transform='translate(-1475.959 502.92)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5281'
            data-name='Group 5281'
            transform='translate(77.578 47.846)'
          >
            <path
              id='Path_9816'
              data-name='Path 9816'
              d='M1526.558-503.608l-.019,6.425a3.262,3.262,0,0,1-1.585,2.6c-2.753,1.946-7.786,2.052-10.8.313l.019-6.425c3.013,1.739,8.046,1.636,10.8-.311A3.261,3.261,0,0,0,1526.558-503.608Z'
              transform='translate(-1514.156 503.608)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5282'
            data-name='Group 5282'
            transform='translate(0.282 47.802)'
          >
            <path
              id='Path_9817'
              data-name='Path 9817'
              d='M1468.648-497.209l.019-6.425a3.29,3.29,0,0,0,1.619,2.645c.156.11.321.214.492.313a11.39,11.39,0,0,0,10.307,0l-.019,6.425a11.4,11.4,0,0,1-10.308,0c-.17-.1-.335-.2-.491-.314A3.286,3.286,0,0,1,1468.648-497.209Z'
              transform='translate(-1468.648 503.634)'
              fill='#bcc0d2'
            />
          </g>
          <g
            id='Group_5283'
            data-name='Group 5283'
            transform='translate(0.018)'
          >
            <path
              id='Path_9818'
              data-name='Path 9818'
              d='M1556.1-530.547c.172.1.336.2.492.314,2.415,1.7,2.053,4.2-.616,5.753l-2.838,1.649c-16.2,9.413-16.112,24.681.2,34.1l2.857,1.649c2.685,1.551,3.074,4.056.681,5.754-2.753,1.947-7.786,2.05-10.8.311l-3.016-1.741a58.222,58.222,0,0,0-30.38-7.929,63.04,63.04,0,0,0-28.484,7.927l-3,1.741a11.39,11.39,0,0,1-10.307,0c-.172-.1-.336-.2-.492-.313-2.415-1.7-2.059-4.2.613-5.756l2.838-1.649c16.2-9.415,16.115-24.678-.192-34.094l-3.016-1.741c-3.135-1.811-2.833-4.887.915-6.439a11.739,11.739,0,0,1,9.522.559l2.857,1.649c16.307,9.415,42.663,9.413,58.86,0l3-1.744A11.406,11.406,0,0,1,1556.1-530.547Z'
              transform='translate(-1468.493 531.778)'
              fill='#f3f4f8'
            />
          </g>
          <g
            id='Group_5284'
            data-name='Group 5284'
            transform='translate(1.476 1.458)'
          >
            <path
              id='Path_9819'
              data-name='Path 9819'
              d='M1475.44-481.846h0a9.144,9.144,0,0,1-4.425-1.034c-.134-.078-.263-.16-.382-.243-.385-.27-1.028-.824-1-1.49a2.613,2.613,0,0,1,1.505-1.814l2.838-1.649c8.277-4.81,12.831-11.292,12.821-18.251-.01-7-4.632-13.525-13.016-18.366l-3.017-1.741c-.924-.533-1.452-1.208-1.413-1.805.037-.562.623-1.389,2.157-2.025a9.342,9.342,0,0,1,3.536-.64,9.7,9.7,0,0,1,4.7,1.114l2.857,1.649c8.1,4.679,18.828,7.256,30.2,7.256s22.069-2.578,30.125-7.259l3-1.743a9.074,9.074,0,0,1,4.415-1.033,9.148,9.148,0,0,1,4.431,1.036c.136.078.265.16.382.243.384.268,1.026.82,1,1.486a2.613,2.613,0,0,1-1.507,1.814l-2.838,1.649c-8.275,4.81-12.827,11.29-12.819,18.25.01,7,4.634,13.529,13.017,18.369l2.857,1.648a2.664,2.664,0,0,1,1.536,1.836,1.917,1.917,0,0,1-.968,1.468,8.669,8.669,0,0,1-4.795,1.274h0a9.167,9.167,0,0,1-4.432-1.036l-3.016-1.741a59.69,59.69,0,0,0-29.76-8.141q-.69,0-1.384.017a64.812,64.812,0,0,0-29.182,8.124l-3,1.741A9.083,9.083,0,0,1,1475.44-481.846Zm-.394-48.329a8.634,8.634,0,0,0-3.258.584c-1.219.5-1.688,1.114-1.707,1.4-.017.241.306.7,1.05,1.126l3.017,1.743c8.616,4.975,13.369,11.721,13.379,19,.01,7.229-4.671,13.935-13.182,18.881l-2.838,1.649c-.885.515-1.135,1.021-1.143,1.216-.01.222.26.56.69.861a3.828,3.828,0,0,0,.326.207,8.4,8.4,0,0,0,4.061.938,8.342,8.342,0,0,0,4.054-.938l3-1.741a65.585,65.585,0,0,1,29.529-8.222c.469-.012.938-.017,1.4-.017a60.433,60.433,0,0,1,30.125,8.238l3.017,1.741a8.408,8.408,0,0,0,4.066.939h0a7.922,7.922,0,0,0,4.374-1.14c.418-.3.673-.618.662-.839s-.269-.715-1.174-1.238l-2.855-1.649c-8.62-4.975-13.372-11.723-13.382-19-.01-7.23,4.671-13.935,13.181-18.881l2.838-1.649c.887-.515,1.136-1.021,1.145-1.216.01-.221-.26-.559-.688-.858-.1-.071-.211-.141-.326-.209a8.421,8.421,0,0,0-4.068-.938,8.329,8.329,0,0,0-4.047.934l-3,1.744c-8.163,4.744-18.991,7.356-30.489,7.356h0c-11.5,0-22.351-2.612-30.565-7.353l-2.855-1.649A8.951,8.951,0,0,0,1475.046-530.175Z'
              transform='translate(-1469.351 530.919)'
              fill='#ffca05'
            />
          </g>
          <g
            id='Group_5285'
            data-name='Group 5285'
            transform='translate(23.683 13.687)'
          >
            <path
              id='Path_9820'
              data-name='Path 9820'
              d='M1518.816-520.114c-8.324-4.807-21.823-4.807-30.147,0s-8.324,12.6,0,17.406,21.823,4.807,30.147,0S1527.14-515.308,1518.816-520.114Z'
              transform='translate(-1482.425 523.719)'
              fill='#ffca05'
            />
          </g>
        </g>
        <g
          id='Group_5296'
          data-name='Group 5296'
          transform='translate(1499.651 -515.49)'
        >
          <g id='Group_5295' data-name='Group 5295' style='isolation: isolate'>
            <g
              id='Group_5292'
              data-name='Group 5292'
              transform='translate(0 11.531)'
              style='isolation: isolate'
            >
              <path
                id='Path_9821'
                data-name='Path 9821'
                d='M1523.2-517.809l-.005,2.079c-.009,2.945-1.945,5.887-5.811,8.134-7.788,4.526-20.454,4.526-28.292,0-3.942-2.276-5.912-5.264-5.9-8.248l.007-2.079c-.009,2.983,1.96,5.972,5.9,8.248,7.837,4.525,20.5,4.525,28.291,0C1521.26-511.922,1523.2-514.864,1523.2-517.809Z'
                transform='translate(-1483.191 517.923)'
                fill='#1f1f24'
              />
              <g
                id='Group_5291'
                data-name='Group 5291'
                clipPath='url(#clip-path-2)'
              >
                <g
                  id='Group_5290'
                  data-name='Group 5290'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5287'
                    data-name='Group 5287'
                    transform='translate(37.878 0.114)'
                  >
                    <path
                      id='Path_9822'
                      data-name='Path 9822'
                      d='M1507.626-517.856l-.005,2.079a7.929,7.929,0,0,1-2.128,5.186l.005-2.079a7.913,7.913,0,0,0,2.128-5.186Z'
                      transform='translate(-1505.492 517.856)'
                      fill='#1f1f24'
                    />
                  </g>
                  <g
                    id='Group_5288'
                    data-name='Group 5288'
                    transform='translate(24.253 5.299)'
                  >
                    <path
                      id='Path_9823'
                      data-name='Path 9823'
                      d='M1511.1-514.8l-.005,2.079a14.5,14.5,0,0,1-3.682,2.949,26.412,26.412,0,0,1-9.943,3.139l.007-2.079a26.41,26.41,0,0,0,9.941-3.139,14.473,14.473,0,0,0,3.682-2.949Z'
                      transform='translate(-1497.47 514.803)'
                      fill='#1f1f24'
                    />
                  </g>
                  <g id='Group_5289' data-name='Group 5289'>
                    <path
                      id='Path_9824'
                      data-name='Path 9824'
                      d='M1507.451-506.536l-.007,2.079c-6.4.791-13.355-.255-18.349-3.139-3.942-2.276-5.912-5.264-5.9-8.248l.007-2.079c-.009,2.983,1.96,5.972,5.9,8.248,4.994,2.882,11.946,3.929,18.349,3.139Z'
                      transform='translate(-1483.191 517.923)'
                      fill='#1f1f24'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5294'
              data-name='Group 5294'
              transform='translate(0.006)'
            >
              <g id='Group_5293' data-name='Group 5293'>
                <path
                  id='Path_9826'
                  data-name='Path 9826'
                  d='M1517.294-521.318c7.839,4.525,7.879,11.862.1,16.385s-20.453,4.525-28.29,0-7.878-11.861-.09-16.385S1509.459-525.843,1517.294-521.318Z'
                  transform='translate(-1483.195 524.712)'
                  fill='#28292f'
                />
              </g>
            </g>
          </g>
        </g>
        <g
          id='Group_5370'
          data-name='Group 5370'
          transform='translate(1459.627 -537.914)'
        >
          <g
            id='Group_5306'
            data-name='Group 5306'
            transform='translate(0.197 0.139)'
          >
            <g
              id='Group_5305'
              data-name='Group 5305'
              style='isolation: isolate'
            >
              <g
                id='Group_5298'
                data-name='Group 5298'
                transform='translate(0 1.215)'
              >
                <g id='Group_5297' data-name='Group 5297'>
                  <path
                    id='Path_9827'
                    data-name='Path 9827'
                    d='M1459.743-537.117l0,.753,7.951,6.461,0-.753Z'
                    transform='translate(-1459.743 537.117)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5300'
                data-name='Group 5300'
                transform='translate(7.952 7.676)'
              >
                <g id='Group_5299' data-name='Group 5299'>
                  <path
                    id='Path_9828'
                    data-name='Path 9828'
                    d='M1464.424-533.313l0,.753,9.391,4.289,0-.753Z'
                    transform='translate(-1464.424 533.313)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5302'
                data-name='Group 5302'
                transform='translate(17.342 10.081)'
              >
                <g id='Group_5301' data-name='Group 5301'>
                  <path
                    id='Path_9829'
                    data-name='Path 9829'
                    d='M1469.953-530.013l0,.753,3.264-1.884,0-.753Z'
                    transform='translate(-1469.953 531.897)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g id='Group_5304' data-name='Group 5304'>
                <g id='Group_5303' data-name='Group 5303'>
                  <path
                    id='Path_9830'
                    data-name='Path 9830'
                    d='M1459.743-536.618l7.952,6.461,9.391,4.289,3.264-1.884-7.373-5.453-11.129-4.627Z'
                    transform='translate(-1459.743 537.832)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5316'
            data-name='Group 5316'
            transform='translate(20.802 0.139)'
          >
            <g
              id='Group_5315'
              data-name='Group 5315'
              style='isolation: isolate'
            >
              <g
                id='Group_5308'
                data-name='Group 5308'
                transform='translate(12.654 1.215)'
              >
                <g id='Group_5307' data-name='Group 5307'>
                  <path
                    id='Path_9831'
                    data-name='Path 9831'
                    d='M1487.278-537.117l0,.753-7.952,6.461,0-.753Z'
                    transform='translate(-1479.324 537.117)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5310'
                data-name='Group 5310'
                transform='translate(3.264 7.676)'
              >
                <g id='Group_5309' data-name='Group 5309'>
                  <path
                    id='Path_9832'
                    data-name='Path 9832'
                    d='M1483.188-533.313l0,.753-9.391,4.289,0-.753Z'
                    transform='translate(-1473.796 533.313)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5312'
                data-name='Group 5312'
                transform='translate(0 10.081)'
              >
                <g id='Group_5311' data-name='Group 5311'>
                  <path
                    id='Path_9833'
                    data-name='Path 9833'
                    d='M1475.14-530.013l0,.753-3.264-1.884,0-.753Z'
                    transform='translate(-1471.874 531.897)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5314'
                data-name='Group 5314'
                transform='translate(0.002)'
              >
                <g id='Group_5313' data-name='Group 5313'>
                  <path
                    id='Path_9834'
                    data-name='Path 9834'
                    d='M1492.482-536.618l-7.952,6.461-9.391,4.289-3.264-1.884,7.373-5.453,11.129-4.627Z'
                    transform='translate(-1471.875 537.832)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5317'
            data-name='Group 5317'
            transform='translate(16.861 11.718)'
          >
            <path
              id='Path_9835'
              data-name='Path 9835'
              d='M1476.856-526.8a6.678,6.678,0,0,1-6.042-.012c-1.636-.944-1.681-2.459-.129-3.422l.109-.066.112-.061a6.7,6.7,0,0,1,5.931.073C1478.511-529.32,1478.519-527.759,1476.856-526.8Z'
              transform='translate(-1469.554 531.015)'
              fill='#d0d2d4'
            />
          </g>
          <g
            id='Group_5349'
            data-name='Group 5349'
            transform='translate(16.846)'
          >
            <g
              id='Group_5327'
              data-name='Group 5327'
              transform='translate(0 6.481)'
            >
              <g
                id='Group_5326'
                data-name='Group 5326'
                style='isolation: isolate'
              >
                <g
                  id='Group_5323'
                  data-name='Group 5323'
                  transform='translate(0 2.317)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5322'
                    data-name='Group 5322'
                    clipPath='url(#clip-path-3)'
                  >
                    <g
                      id='Group_5321'
                      data-name='Group 5321'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5318'
                        data-name='Group 5318'
                        transform='translate(7.616 0.024)'
                      >
                        <path
                          id='Path_9836'
                          data-name='Path 9836'
                          d='M1474.47-532.72l-.015,5.05a1.587,1.587,0,0,1-.426,1.041l.013-5.05a1.6,1.6,0,0,0,.428-1.041Z'
                          transform='translate(-1474.029 532.72)'
                          fill='#1f1f24'
                        />
                      </g>
                      <g
                        id='Group_5319'
                        data-name='Group 5319'
                        transform='translate(4.875 1.065)'
                      >
                        <path
                          id='Path_9837'
                          data-name='Path 9837'
                          d='M1475.17-532.107l-.013,5.05a2.978,2.978,0,0,1-.742.594,5.33,5.33,0,0,1-2,.63l.014-5.05a5.316,5.316,0,0,0,2-.63,2.937,2.937,0,0,0,.742-.594Z'
                          transform='translate(-1472.415 532.107)'
                          fill='#26272d'
                        />
                      </g>
                      <g id='Group_5320' data-name='Group 5320'>
                        <path
                          id='Path_9838'
                          data-name='Path 9838'
                          d='M1474.433-530.444l-.013,5.05a6.15,6.15,0,0,1-3.687-.63,2.012,2.012,0,0,1-1.187-1.659l.015-5.05a2.011,2.011,0,0,0,1.186,1.659,6.155,6.155,0,0,0,3.688.63Z'
                          transform='translate(-1469.545 532.734)'
                          fill='#1f1f24'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5325'
                  data-name='Group 5325'
                  transform='translate(0.014)'
                >
                  <g id='Group_5324' data-name='Group 5324'>
                    <path
                      id='Path_9840'
                      data-name='Path 9840'
                      d='M1476.408-533.415c1.578.91,1.586,2.383.019,3.293a6.267,6.267,0,0,1-5.687,0c-1.574-.91-1.583-2.385-.019-3.293A6.262,6.262,0,0,1,1476.408-533.415Z'
                      transform='translate(-1469.553 534.098)'
                      fill='#28292f'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5337'
              data-name='Group 5337'
              transform='translate(1.352 3.518)'
            >
              <g
                id='Group_5336'
                data-name='Group 5336'
                style='isolation: isolate'
              >
                <g
                  id='Group_5333'
                  data-name='Group 5333'
                  transform='translate(0 1.542)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5332'
                    data-name='Group 5332'
                    clipPath='url(#clip-path-4)'
                  >
                    <g
                      id='Group_5331'
                      data-name='Group 5331'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5328'
                        data-name='Group 5328'
                        transform='translate(5.063 0.014)'
                      >
                        <path
                          id='Path_9841'
                          data-name='Path 9841'
                          d='M1473.612-534.927l-.007,2.079a1.058,1.058,0,0,1-.284.693l.005-2.079a1.069,1.069,0,0,0,.286-.693Z'
                          transform='translate(-1473.322 534.927)'
                          fill='#1f1f24'
                        />
                      </g>
                      <g
                        id='Group_5329'
                        data-name='Group 5329'
                        transform='translate(3.239 0.707)'
                      >
                        <path
                          id='Path_9842'
                          data-name='Path 9842'
                          d='M1474.077-534.519l-.005,2.079a1.971,1.971,0,0,1-.5.4,3.517,3.517,0,0,1-1.328.419l0-2.079a3.534,3.534,0,0,0,1.33-.42,1.927,1.927,0,0,0,.494-.4Z'
                          transform='translate(-1472.248 534.519)'
                          fill='#26272d'
                        />
                      </g>
                      <g id='Group_5330' data-name='Group 5330'>
                        <path
                          id='Path_9843'
                          data-name='Path 9843'
                          d='M1473.585-533.413l0,2.079a4.092,4.092,0,0,1-2.451-.419,1.336,1.336,0,0,1-.788-1.1l.005-2.079a1.335,1.335,0,0,0,.788,1.1,4.094,4.094,0,0,0,2.451.419Z'
                          transform='translate(-1470.341 534.935)'
                          fill='#1f1f24'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5335'
                  data-name='Group 5335'
                  transform='translate(0.005)'
                >
                  <g id='Group_5334' data-name='Group 5334'>
                    <path
                      id='Path_9845'
                      data-name='Path 9845'
                      d='M1474.9-535.39a1.157,1.157,0,0,1,.01,2.191,4.162,4.162,0,0,1-3.781,0,1.157,1.157,0,0,1-.012-2.189A4.165,4.165,0,0,1,1474.9-535.39Z'
                      transform='translate(-1470.344 535.843)'
                      fill='#28292f'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5347'
              data-name='Group 5347'
              transform='translate(1.352 1.291)'
            >
              <g
                id='Group_5346'
                data-name='Group 5346'
                style='isolation: isolate'
              >
                <g
                  id='Group_5343'
                  data-name='Group 5343'
                  transform='translate(0 1.542)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5342'
                    data-name='Group 5342'
                    clipPath='url(#clip-path-5)'
                  >
                    <g
                      id='Group_5341'
                      data-name='Group 5341'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5338'
                        data-name='Group 5338'
                        transform='translate(5.065 0.014)'
                      >
                        <path
                          id='Path_9846'
                          data-name='Path 9846'
                          d='M1473.61-536.238l0,1.187a1.057,1.057,0,0,1-.284.693l0-1.189a1.05,1.05,0,0,0,.284-.691Z'
                          transform='translate(-1473.323 536.238)'
                          fill='#ffa705'
                        />
                      </g>
                      <g
                        id='Group_5339'
                        data-name='Group 5339'
                        transform='translate(3.239 0.705)'
                      >
                        <path
                          id='Path_9847'
                          data-name='Path 9847'
                          d='M1474.077-535.831l0,1.189a1.922,1.922,0,0,1-.494.394,3.5,3.5,0,0,1-1.331.421l0-1.187a3.566,3.566,0,0,0,1.332-.421,1.929,1.929,0,0,0,.494-.4Z'
                          transform='translate(-1472.248 535.831)'
                          fill='#ffb806'
                        />
                      </g>
                      <g id='Group_5340' data-name='Group 5340'>
                        <path
                          id='Path_9848'
                          data-name='Path 9848'
                          d='M1473.584-534.724l0,1.187a4.086,4.086,0,0,1-2.449-.419,1.341,1.341,0,0,1-.79-1.1l0-1.187a1.341,1.341,0,0,0,.79,1.1,4.088,4.088,0,0,0,2.449.42Z'
                          transform='translate(-1470.341 536.246)'
                          fill='#ffa705'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5345'
                  data-name='Group 5345'
                  transform='translate(0.004)'
                >
                  <g id='Group_5344' data-name='Group 5344'>
                    <path
                      id='Path_9850'
                      data-name='Path 9850'
                      d='M1474.9-536.7a1.156,1.156,0,0,1,.012,2.189,4.167,4.167,0,0,1-3.781,0,1.158,1.158,0,0,1-.01-2.191A4.164,4.164,0,0,1,1474.9-536.7Z'
                      transform='translate(-1470.344 537.154)'
                      fill='#ffca05'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5348'
              data-name='Group 5348'
              transform='translate(1.357)'
            >
              <path
                id='Path_9851'
                data-name='Path 9851'
                d='M1475.691-535.067a2.922,2.922,0,0,0-2.675-2.847,2.876,2.876,0,0,0-2.672,2.8Z'
                transform='translate(-1470.344 537.914)'
                fill='#ffca05'
              />
            </g>
          </g>
          <g
            id='Group_5359'
            data-name='Group 5359'
            transform='translate(0 12.104)'
          >
            <g
              id='Group_5358'
              data-name='Group 5358'
              style='isolation: isolate'
            >
              <g
                id='Group_5351'
                data-name='Group 5351'
                transform='translate(13.231 1.884)'
              >
                <g id='Group_5350' data-name='Group 5350'>
                  <path
                    id='Path_9852'
                    data-name='Path 9852'
                    d='M1474.792-529.678l0,.753-7.373,5.454,0-.753Z'
                    transform='translate(-1467.417 529.678)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5353'
                data-name='Group 5353'
                transform='translate(2.106 7.338)'
              >
                <g id='Group_5352' data-name='Group 5352'>
                  <path
                    id='Path_9853'
                    data-name='Path 9853'
                    d='M1471.994-526.468l0,.753-11.125,4.627,0-.753Z'
                    transform='translate(-1460.866 526.468)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5355'
                data-name='Group 5355'
                transform='translate(0 10.749)'
              >
                <g id='Group_5354' data-name='Group 5354'>
                  <path
                    id='Path_9854'
                    data-name='Path 9854'
                    d='M1461.735-523.243l0,.753-2.106-1.216,0-.753Z'
                    transform='translate(-1459.627 524.459)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5357'
                data-name='Group 5357'
                transform='translate(0.002)'
              >
                <g id='Group_5356' data-name='Group 5356'>
                  <path
                    id='Path_9855'
                    data-name='Path 9855'
                    d='M1480.232-528.9l-7.373,5.453-11.125,4.627-2.106-1.216,7.95-6.461,9.391-4.289Z'
                    transform='translate(-1459.628 530.788)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5369'
            data-name='Group 5369'
            transform='translate(21.524 11.983)'
          >
            <g
              id='Group_5368'
              data-name='Group 5368'
              style='isolation: isolate'
            >
              <g
                id='Group_5361'
                data-name='Group 5361'
                transform='translate(0 1.884)'
              >
                <g id='Group_5360' data-name='Group 5360'>
                  <path
                    id='Path_9856'
                    data-name='Path 9856'
                    d='M1472.3-529.75l0,.753,7.374,5.453,0-.753Z'
                    transform='translate(-1472.299 529.75)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5363'
                data-name='Group 5363'
                transform='translate(7.374 7.338)'
              >
                <g id='Group_5362' data-name='Group 5362'>
                  <path
                    id='Path_9857'
                    data-name='Path 9857'
                    d='M1476.64-526.539l0,.753,11.125,4.627,0-.753Z'
                    transform='translate(-1476.64 526.539)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5365'
                data-name='Group 5365'
                transform='translate(18.498 10.749)'
              >
                <g id='Group_5364' data-name='Group 5364'>
                  <path
                    id='Path_9858'
                    data-name='Path 9858'
                    d='M1483.19-523.315l0,.753,2.106-1.216,0-.753Z'
                    transform='translate(-1483.19 524.53)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g id='Group_5367' data-name='Group 5367'>
                <g id='Group_5366' data-name='Group 5366'>
                  <path
                    id='Path_9859'
                    data-name='Path 9859'
                    d='M1472.3-528.975l7.374,5.454,11.125,4.627,2.106-1.216-7.95-6.46-9.391-4.289Z'
                    transform='translate(-1472.299 530.859)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
        </g>
        <g
          id='Group_5444'
          data-name='Group 5444'
          transform='translate(1537.381 -537.831)'
        >
          <g
            id='Group_5380'
            data-name='Group 5380'
            transform='translate(0.197 0.139)'
          >
            <g
              id='Group_5379'
              data-name='Group 5379'
              style='isolation: isolate'
            >
              <g
                id='Group_5372'
                data-name='Group 5372'
                transform='translate(0 1.215)'
              >
                <g id='Group_5371' data-name='Group 5371'>
                  <path
                    id='Path_9860'
                    data-name='Path 9860'
                    d='M1505.521-537.068l0,.753,7.951,6.461,0-.753Z'
                    transform='translate(-1505.521 537.068)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5374'
                data-name='Group 5374'
                transform='translate(7.952 7.676)'
              >
                <g id='Group_5373' data-name='Group 5373'>
                  <path
                    id='Path_9861'
                    data-name='Path 9861'
                    d='M1510.2-533.264l0,.753,9.391,4.289,0-.753Z'
                    transform='translate(-1510.202 533.264)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5376'
                data-name='Group 5376'
                transform='translate(17.343 10.08)'
              >
                <g id='Group_5375' data-name='Group 5375'>
                  <path
                    id='Path_9862'
                    data-name='Path 9862'
                    d='M1515.731-529.964l0,.753L1519-531.1l0-.753Z'
                    transform='translate(-1515.731 531.848)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g id='Group_5378' data-name='Group 5378'>
                <g id='Group_5377' data-name='Group 5377'>
                  <path
                    id='Path_9863'
                    data-name='Path 9863'
                    d='M1505.521-536.568l7.952,6.461,9.391,4.289,3.264-1.885-7.373-5.453-11.129-4.627Z'
                    transform='translate(-1505.521 537.783)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5390'
            data-name='Group 5390'
            transform='translate(20.802 0.139)'
          >
            <g
              id='Group_5389'
              data-name='Group 5389'
              style='isolation: isolate'
            >
              <g
                id='Group_5382'
                data-name='Group 5382'
                transform='translate(12.654 1.215)'
              >
                <g id='Group_5381' data-name='Group 5381'>
                  <path
                    id='Path_9864'
                    data-name='Path 9864'
                    d='M1533.056-537.068l0,.753-7.952,6.461,0-.753Z'
                    transform='translate(-1525.103 537.068)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5384'
                data-name='Group 5384'
                transform='translate(3.264 7.676)'
              >
                <g id='Group_5383' data-name='Group 5383'>
                  <path
                    id='Path_9865'
                    data-name='Path 9865'
                    d='M1528.967-533.264l0,.753-9.391,4.289,0-.753Z'
                    transform='translate(-1519.574 533.264)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5386'
                data-name='Group 5386'
                transform='translate(0 10.08)'
              >
                <g id='Group_5385' data-name='Group 5385'>
                  <path
                    id='Path_9866'
                    data-name='Path 9866'
                    d='M1520.918-529.964l0,.753-3.264-1.884,0-.753Z'
                    transform='translate(-1517.652 531.848)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5388'
                data-name='Group 5388'
                transform='translate(0.002)'
              >
                <g id='Group_5387' data-name='Group 5387'>
                  <path
                    id='Path_9867'
                    data-name='Path 9867'
                    d='M1538.259-536.568l-7.952,6.461-9.391,4.289-3.264-1.885,7.373-5.453,11.129-4.627Z'
                    transform='translate(-1517.653 537.783)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5391'
            data-name='Group 5391'
            transform='translate(16.861 11.718)'
          >
            <path
              id='Path_9868'
              data-name='Path 9868'
              d='M1522.634-526.749a6.678,6.678,0,0,1-6.042-.012c-1.636-.944-1.681-2.458-.129-3.422.036-.022.071-.044.11-.066s.071-.041.111-.059a6.7,6.7,0,0,1,5.931.071C1524.289-529.271,1524.3-527.71,1522.634-526.749Z'
              transform='translate(-1515.332 530.966)'
              fill='#d0d2d4'
            />
          </g>
          <g
            id='Group_5423'
            data-name='Group 5423'
            transform='translate(16.846)'
          >
            <g
              id='Group_5401'
              data-name='Group 5401'
              transform='translate(0 6.482)'
            >
              <g
                id='Group_5400'
                data-name='Group 5400'
                style='isolation: isolate'
              >
                <g
                  id='Group_5397'
                  data-name='Group 5397'
                  transform='translate(0 2.318)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5396'
                    data-name='Group 5396'
                    clipPath='url(#clip-path-6)'
                  >
                    <g
                      id='Group_5395'
                      data-name='Group 5395'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5392'
                        data-name='Group 5392'
                        transform='translate(7.616 0.022)'
                      >
                        <path
                          id='Path_9869'
                          data-name='Path 9869'
                          d='M1520.249-532.671l-.015,5.05a1.582,1.582,0,0,1-.426,1.041l.013-5.05a1.593,1.593,0,0,0,.428-1.041Z'
                          transform='translate(-1519.807 532.671)'
                          fill='#1f1f24'
                        />
                      </g>
                      <g
                        id='Group_5393'
                        data-name='Group 5393'
                        transform='translate(4.875 1.063)'
                      >
                        <path
                          id='Path_9870'
                          data-name='Path 9870'
                          d='M1520.948-532.058l-.014,5.05a2.964,2.964,0,0,1-.742.595,5.358,5.358,0,0,1-2,.632l.014-5.05a5.347,5.347,0,0,0,2-.632,2.969,2.969,0,0,0,.742-.594Z'
                          transform='translate(-1518.193 532.058)'
                          fill='#26272d'
                        />
                      </g>
                      <g id='Group_5394' data-name='Group 5394'>
                        <path
                          id='Path_9871'
                          data-name='Path 9871'
                          d='M1520.211-530.394l-.014,5.05a6.164,6.164,0,0,1-3.687-.632,2.013,2.013,0,0,1-1.187-1.658l.015-5.05a2.011,2.011,0,0,0,1.186,1.658,6.168,6.168,0,0,0,3.688.632Z'
                          transform='translate(-1515.323 532.684)'
                          fill='#1f1f24'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5399'
                  data-name='Group 5399'
                  transform='translate(0.015)'
                >
                  <g id='Group_5398' data-name='Group 5398'>
                    <path
                      id='Path_9873'
                      data-name='Path 9873'
                      d='M1522.186-533.366c1.578.91,1.587,2.383.019,3.293a6.267,6.267,0,0,1-5.687,0c-1.574-.909-1.583-2.385-.019-3.293A6.261,6.261,0,0,1,1522.186-533.366Z'
                      transform='translate(-1515.332 534.049)'
                      fill='#28292f'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5411'
              data-name='Group 5411'
              transform='translate(1.352 3.518)'
            >
              <g
                id='Group_5410'
                data-name='Group 5410'
                style='isolation: isolate'
              >
                <g
                  id='Group_5407'
                  data-name='Group 5407'
                  transform='translate(0 1.542)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5406'
                    data-name='Group 5406'
                    clipPath='url(#clip-path-7)'
                  >
                    <g
                      id='Group_5405'
                      data-name='Group 5405'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5402'
                        data-name='Group 5402'
                        transform='translate(5.063 0.014)'
                      >
                        <path
                          id='Path_9874'
                          data-name='Path 9874'
                          d='M1519.391-534.878l-.007,2.079a1.057,1.057,0,0,1-.284.693l.005-2.079a1.068,1.068,0,0,0,.285-.693Z'
                          transform='translate(-1519.1 534.878)'
                          fill='#1f1f24'
                        />
                      </g>
                      <g
                        id='Group_5403'
                        data-name='Group 5403'
                        transform='translate(3.239 0.707)'
                      >
                        <path
                          id='Path_9875'
                          data-name='Path 9875'
                          d='M1519.855-534.47l-.005,2.079a1.968,1.968,0,0,1-.5.4,3.518,3.518,0,0,1-1.328.42l.005-2.079a3.537,3.537,0,0,0,1.33-.42,1.927,1.927,0,0,0,.494-.4Z'
                          transform='translate(-1518.026 534.47)'
                          fill='#26272d'
                        />
                      </g>
                      <g id='Group_5404' data-name='Group 5404'>
                        <path
                          id='Path_9876'
                          data-name='Path 9876'
                          d='M1519.363-533.364l-.005,2.079a4.092,4.092,0,0,1-2.451-.42,1.336,1.336,0,0,1-.788-1.1l.005-2.079a1.336,1.336,0,0,0,.788,1.1,4.094,4.094,0,0,0,2.451.419Z'
                          transform='translate(-1516.119 534.886)'
                          fill='#1f1f24'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5409'
                  data-name='Group 5409'
                  transform='translate(0.005)'
                >
                  <g id='Group_5408' data-name='Group 5408'>
                    <path
                      id='Path_9878'
                      data-name='Path 9878'
                      d='M1520.681-535.341a1.157,1.157,0,0,1,.01,2.191,4.161,4.161,0,0,1-3.781,0,1.157,1.157,0,0,1-.012-2.189A4.165,4.165,0,0,1,1520.681-535.341Z'
                      transform='translate(-1516.122 535.794)'
                      fill='#28292f'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5421'
              data-name='Group 5421'
              transform='translate(1.352 1.291)'
            >
              <g
                id='Group_5420'
                data-name='Group 5420'
                style='isolation: isolate'
              >
                <g
                  id='Group_5417'
                  data-name='Group 5417'
                  transform='translate(0 1.542)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5416'
                    data-name='Group 5416'
                    clipPath='url(#clip-path-8)'
                  >
                    <g
                      id='Group_5415'
                      data-name='Group 5415'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5412'
                        data-name='Group 5412'
                        transform='translate(5.065 0.014)'
                      >
                        <path
                          id='Path_9879'
                          data-name='Path 9879'
                          d='M1519.388-536.189l0,1.187a1.057,1.057,0,0,1-.284.693l0-1.189a1.05,1.05,0,0,0,.284-.691Z'
                          transform='translate(-1519.101 536.189)'
                          fill='#ffa705'
                        />
                      </g>
                      <g
                        id='Group_5413'
                        data-name='Group 5413'
                        transform='translate(3.239 0.705)'
                      >
                        <path
                          id='Path_9880'
                          data-name='Path 9880'
                          d='M1519.855-535.782l0,1.189a1.925,1.925,0,0,1-.494.394,3.506,3.506,0,0,1-1.332.421l0-1.187a3.535,3.535,0,0,0,1.332-.421,1.929,1.929,0,0,0,.494-.4Z'
                          transform='translate(-1518.026 535.782)'
                          fill='#ffb806'
                        />
                      </g>
                      <g id='Group_5414' data-name='Group 5414'>
                        <path
                          id='Path_9881'
                          data-name='Path 9881'
                          d='M1519.361-534.675l0,1.187a4.086,4.086,0,0,1-2.449-.42,1.341,1.341,0,0,1-.79-1.1l0-1.189a1.341,1.341,0,0,0,.79,1.1,4.087,4.087,0,0,0,2.449.419Z'
                          transform='translate(-1516.119 536.197)'
                          fill='#ffa705'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5419'
                  data-name='Group 5419'
                  transform='translate(0.004)'
                >
                  <g id='Group_5418' data-name='Group 5418'>
                    <path
                      id='Path_9883'
                      data-name='Path 9883'
                      d='M1520.68-536.652a1.156,1.156,0,0,1,.012,2.189,4.162,4.162,0,0,1-3.781,0,1.157,1.157,0,0,1-.01-2.191A4.164,4.164,0,0,1,1520.68-536.652Z'
                      transform='translate(-1516.121 537.105)'
                      fill='#ffca05'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5422'
              data-name='Group 5422'
              transform='translate(1.357)'
            >
              <path
                id='Path_9884'
                data-name='Path 9884'
                d='M1521.469-535.018a2.923,2.923,0,0,0-2.675-2.847,2.876,2.876,0,0,0-2.672,2.8Z'
                transform='translate(-1516.122 537.865)'
                fill='#ffca05'
              />
            </g>
          </g>
          <g
            id='Group_5433'
            data-name='Group 5433'
            transform='translate(0 12.104)'
          >
            <g
              id='Group_5432'
              data-name='Group 5432'
              style='isolation: isolate'
            >
              <g
                id='Group_5425'
                data-name='Group 5425'
                transform='translate(13.231 1.884)'
              >
                <g id='Group_5424' data-name='Group 5424'>
                  <path
                    id='Path_9885'
                    data-name='Path 9885'
                    d='M1520.57-529.629l0,.753-7.373,5.453,0-.753Z'
                    transform='translate(-1513.194 529.629)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5427'
                data-name='Group 5427'
                transform='translate(2.106 7.338)'
              >
                <g id='Group_5426' data-name='Group 5426'>
                  <path
                    id='Path_9886'
                    data-name='Path 9886'
                    d='M1517.772-526.419l0,.753-11.125,4.627,0-.753Z'
                    transform='translate(-1506.645 526.419)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5429'
                data-name='Group 5429'
                transform='translate(0 10.749)'
              >
                <g id='Group_5428' data-name='Group 5428'>
                  <path
                    id='Path_9887'
                    data-name='Path 9887'
                    d='M1507.513-523.194l0,.753-2.106-1.216,0-.753Z'
                    transform='translate(-1505.405 524.41)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5431'
                data-name='Group 5431'
                transform='translate(0.002)'
              >
                <g id='Group_5430' data-name='Group 5430'>
                  <path
                    id='Path_9888'
                    data-name='Path 9888'
                    d='M1526.01-528.854l-7.373,5.453-11.125,4.627-2.106-1.216,7.95-6.461,9.391-4.289Z'
                    transform='translate(-1505.406 530.739)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5443'
            data-name='Group 5443'
            transform='translate(21.524 11.983)'
          >
            <g
              id='Group_5442'
              data-name='Group 5442'
              style='isolation: isolate'
            >
              <g
                id='Group_5435'
                data-name='Group 5435'
                transform='translate(0 1.885)'
              >
                <g id='Group_5434' data-name='Group 5434'>
                  <path
                    id='Path_9889'
                    data-name='Path 9889'
                    d='M1518.077-529.7l0,.753,7.373,5.454,0-.753Z'
                    transform='translate(-1518.077 529.7)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5437'
                data-name='Group 5437'
                transform='translate(7.373 7.338)'
              >
                <g id='Group_5436' data-name='Group 5436'>
                  <path
                    id='Path_9890'
                    data-name='Path 9890'
                    d='M1522.418-526.49l0,.753,11.125,4.627,0-.753Z'
                    transform='translate(-1522.418 526.49)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5439'
                data-name='Group 5439'
                transform='translate(18.498 10.749)'
              >
                <g id='Group_5438' data-name='Group 5438'>
                  <path
                    id='Path_9891'
                    data-name='Path 9891'
                    d='M1528.968-523.265l0,.753,2.106-1.216,0-.753Z'
                    transform='translate(-1528.968 524.481)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g id='Group_5441' data-name='Group 5441'>
                <g id='Group_5440' data-name='Group 5440'>
                  <path
                    id='Path_9892'
                    data-name='Path 9892'
                    d='M1518.077-528.925l7.373,5.453,11.125,4.627,2.106-1.216-7.95-6.461-9.391-4.289Z'
                    transform='translate(-1518.077 530.81)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
        </g>
        <g
          id='Group_5518'
          data-name='Group 5518'
          transform='translate(1459.627 -493.056)'
        >
          <g
            id='Group_5454'
            data-name='Group 5454'
            transform='translate(0.197 0.138)'
          >
            <g
              id='Group_5453'
              data-name='Group 5453'
              style='isolation: isolate'
            >
              <g
                id='Group_5446'
                data-name='Group 5446'
                transform='translate(0 1.215)'
              >
                <g id='Group_5445' data-name='Group 5445'>
                  <path
                    id='Path_9893'
                    data-name='Path 9893'
                    d='M1459.743-510.708l0,.753,7.951,6.462,0-.753Z'
                    transform='translate(-1459.743 510.708)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5448'
                data-name='Group 5448'
                transform='translate(7.952 7.676)'
              >
                <g id='Group_5447' data-name='Group 5447'>
                  <path
                    id='Path_9894'
                    data-name='Path 9894'
                    d='M1464.424-506.9l0,.753,9.391,4.289,0-.753Z'
                    transform='translate(-1464.424 506.903)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5450'
                data-name='Group 5450'
                transform='translate(17.342 10.08)'
              >
                <g id='Group_5449' data-name='Group 5449'>
                  <path
                    id='Path_9895'
                    data-name='Path 9895'
                    d='M1469.953-503.6l0,.753,3.264-1.884,0-.753Z'
                    transform='translate(-1469.953 505.488)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g id='Group_5452' data-name='Group 5452'>
                <g id='Group_5451' data-name='Group 5451'>
                  <path
                    id='Path_9896'
                    data-name='Path 9896'
                    d='M1459.743-510.208l7.952,6.462,9.391,4.289,3.264-1.884-7.373-5.453-11.129-4.627Z'
                    transform='translate(-1459.743 511.423)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5464'
            data-name='Group 5464'
            transform='translate(20.802 0.138)'
          >
            <g
              id='Group_5463'
              data-name='Group 5463'
              style='isolation: isolate'
            >
              <g
                id='Group_5456'
                data-name='Group 5456'
                transform='translate(12.654 1.215)'
              >
                <g id='Group_5455' data-name='Group 5455'>
                  <path
                    id='Path_9897'
                    data-name='Path 9897'
                    d='M1487.278-510.708l0,.753-7.952,6.462,0-.753Z'
                    transform='translate(-1479.324 510.708)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5458'
                data-name='Group 5458'
                transform='translate(3.264 7.676)'
              >
                <g id='Group_5457' data-name='Group 5457'>
                  <path
                    id='Path_9898'
                    data-name='Path 9898'
                    d='M1483.188-506.9l0,.753-9.391,4.289,0-.753Z'
                    transform='translate(-1473.796 506.903)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5460'
                data-name='Group 5460'
                transform='translate(0 10.08)'
              >
                <g id='Group_5459' data-name='Group 5459'>
                  <path
                    id='Path_9899'
                    data-name='Path 9899'
                    d='M1475.14-503.6l0,.753-3.264-1.884,0-.753Z'
                    transform='translate(-1471.874 505.488)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5462'
                data-name='Group 5462'
                transform='translate(0.002)'
              >
                <g id='Group_5461' data-name='Group 5461'>
                  <path
                    id='Path_9900'
                    data-name='Path 9900'
                    d='M1492.482-510.208l-7.952,6.462-9.391,4.289-3.264-1.884,7.373-5.453,11.129-4.627Z'
                    transform='translate(-1471.875 511.423)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5465'
            data-name='Group 5465'
            transform='translate(16.861 11.718)'
          >
            <path
              id='Path_9901'
              data-name='Path 9901'
              d='M1476.856-500.389a6.673,6.673,0,0,1-6.042-.01c-1.636-.944-1.681-2.459-.129-3.424l.109-.065.112-.061a6.709,6.709,0,0,1,5.931.071C1478.511-502.911,1478.519-501.349,1476.856-500.389Z'
              transform='translate(-1469.554 504.605)'
              fill='#d0d2d4'
            />
          </g>
          <g
            id='Group_5497'
            data-name='Group 5497'
            transform='translate(16.846)'
          >
            <g
              id='Group_5475'
              data-name='Group 5475'
              transform='translate(0 6.48)'
            >
              <g
                id='Group_5474'
                data-name='Group 5474'
                style='isolation: isolate'
              >
                <g
                  id='Group_5471'
                  data-name='Group 5471'
                  transform='translate(0 2.318)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5470'
                    data-name='Group 5470'
                    clipPath='url(#clip-path-9)'
                  >
                    <g
                      id='Group_5469'
                      data-name='Group 5469'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5466'
                        data-name='Group 5466'
                        transform='translate(7.616 0.024)'
                      >
                        <path
                          id='Path_9902'
                          data-name='Path 9902'
                          d='M1474.47-506.31l-.015,5.05a1.583,1.583,0,0,1-.426,1.039l.013-5.05a1.594,1.594,0,0,0,.428-1.039Z'
                          transform='translate(-1474.029 506.31)'
                          fill='#1f1f24'
                        />
                      </g>
                      <g
                        id='Group_5467'
                        data-name='Group 5467'
                        transform='translate(4.875 1.063)'
                      >
                        <path
                          id='Path_9903'
                          data-name='Path 9903'
                          d='M1475.17-505.7l-.013,5.05a2.938,2.938,0,0,1-.742.595,5.3,5.3,0,0,1-2,.632l.014-5.05a5.288,5.288,0,0,0,2-.632,2.9,2.9,0,0,0,.742-.594Z'
                          transform='translate(-1472.415 505.698)'
                          fill='#26272d'
                        />
                      </g>
                      <g id='Group_5468' data-name='Group 5468'>
                        <path
                          id='Path_9904'
                          data-name='Path 9904'
                          d='M1474.433-504.034l-.013,5.05a6.138,6.138,0,0,1-3.687-.632,2.009,2.009,0,0,1-1.187-1.658l.015-5.05a2.009,2.009,0,0,0,1.186,1.658,6.144,6.144,0,0,0,3.688.632Z'
                          transform='translate(-1469.545 506.324)'
                          fill='#1f1f24'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5473'
                  data-name='Group 5473'
                  transform='translate(0.014)'
                >
                  <g id='Group_5472' data-name='Group 5472'>
                    <path
                      id='Path_9906'
                      data-name='Path 9906'
                      d='M1476.408-507.006c1.578.91,1.586,2.383.019,3.293a6.257,6.257,0,0,1-5.687,0c-1.574-.909-1.583-2.383-.019-3.293A6.267,6.267,0,0,1,1476.408-507.006Z'
                      transform='translate(-1469.553 507.689)'
                      fill='#28292f'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5485'
              data-name='Group 5485'
              transform='translate(1.352 3.518)'
            >
              <g
                id='Group_5484'
                data-name='Group 5484'
                style='isolation: isolate'
              >
                <g
                  id='Group_5481'
                  data-name='Group 5481'
                  transform='translate(0 1.541)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5480'
                    data-name='Group 5480'
                    clipPath='url(#clip-path-10)'
                  >
                    <g
                      id='Group_5479'
                      data-name='Group 5479'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5476'
                        data-name='Group 5476'
                        transform='translate(5.063 0.014)'
                      >
                        <path
                          id='Path_9907'
                          data-name='Path 9907'
                          d='M1473.612-508.518l-.007,2.079a1.058,1.058,0,0,1-.284.693l.005-2.079a1.069,1.069,0,0,0,.286-.693Z'
                          transform='translate(-1473.322 508.518)'
                          fill='#1f1f24'
                        />
                      </g>
                      <g
                        id='Group_5477'
                        data-name='Group 5477'
                        transform='translate(3.239 0.707)'
                      >
                        <path
                          id='Path_9908'
                          data-name='Path 9908'
                          d='M1474.077-508.11l-.005,2.079a2.01,2.01,0,0,1-.5.4,3.55,3.55,0,0,1-1.328.42l0-2.079a3.54,3.54,0,0,0,1.33-.421,1.93,1.93,0,0,0,.494-.4Z'
                          transform='translate(-1472.248 508.11)'
                          fill='#26272d'
                        />
                      </g>
                      <g id='Group_5478' data-name='Group 5478'>
                        <path
                          id='Path_9909'
                          data-name='Path 9909'
                          d='M1473.585-507l0,2.079a4.092,4.092,0,0,1-2.451-.42,1.337,1.337,0,0,1-.788-1.1l.005-2.081a1.335,1.335,0,0,0,.788,1.1,4.083,4.083,0,0,0,2.451.421Z'
                          transform='translate(-1470.341 508.526)'
                          fill='#1f1f24'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5483'
                  data-name='Group 5483'
                  transform='translate(0.005)'
                >
                  <g id='Group_5482' data-name='Group 5482'>
                    <path
                      id='Path_9911'
                      data-name='Path 9911'
                      d='M1474.9-508.98a1.156,1.156,0,0,1,.01,2.189,4.162,4.162,0,0,1-3.781,0,1.157,1.157,0,0,1-.012-2.189A4.17,4.17,0,0,1,1474.9-508.98Z'
                      transform='translate(-1470.344 509.433)'
                      fill='#28292f'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5495'
              data-name='Group 5495'
              transform='translate(1.352 1.289)'
            >
              <g
                id='Group_5494'
                data-name='Group 5494'
                style='isolation: isolate'
              >
                <g
                  id='Group_5491'
                  data-name='Group 5491'
                  transform='translate(0 1.542)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5490'
                    data-name='Group 5490'
                    clipPath='url(#clip-path-11)'
                  >
                    <g
                      id='Group_5489'
                      data-name='Group 5489'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5486'
                        data-name='Group 5486'
                        transform='translate(5.065 0.014)'
                      >
                        <path
                          id='Path_9912'
                          data-name='Path 9912'
                          d='M1473.61-509.829l0,1.189a1.051,1.051,0,0,1-.284.691l0-1.187a1.057,1.057,0,0,0,.284-.693Z'
                          transform='translate(-1473.323 509.829)'
                          fill='#ffa705'
                        />
                      </g>
                      <g
                        id='Group_5487'
                        data-name='Group 5487'
                        transform='translate(3.239 0.707)'
                      >
                        <path
                          id='Path_9913'
                          data-name='Path 9913'
                          d='M1474.077-509.421l0,1.187a1.929,1.929,0,0,1-.494.4,3.539,3.539,0,0,1-1.331.421l0-1.189a3.508,3.508,0,0,0,1.332-.421,1.926,1.926,0,0,0,.494-.394Z'
                          transform='translate(-1472.248 509.421)'
                          fill='#ffb806'
                        />
                      </g>
                      <g id='Group_5488' data-name='Group 5488'>
                        <path
                          id='Path_9914'
                          data-name='Path 9914'
                          d='M1473.584-508.315l0,1.189a4.086,4.086,0,0,1-2.449-.42,1.341,1.341,0,0,1-.79-1.1l0-1.189a1.341,1.341,0,0,0,.79,1.1,4.087,4.087,0,0,0,2.449.42Z'
                          transform='translate(-1470.341 509.837)'
                          fill='#ffa705'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5493'
                  data-name='Group 5493'
                  transform='translate(0.004)'
                >
                  <g id='Group_5492' data-name='Group 5492'>
                    <path
                      id='Path_9916'
                      data-name='Path 9916'
                      d='M1474.9-510.292a1.156,1.156,0,0,1,.012,2.189,4.156,4.156,0,0,1-3.781,0,1.157,1.157,0,0,1-.01-2.191A4.163,4.163,0,0,1,1474.9-510.292Z'
                      transform='translate(-1470.344 510.745)'
                      fill='#ffca05'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5496'
              data-name='Group 5496'
              transform='translate(1.357)'
            >
              <path
                id='Path_9917'
                data-name='Path 9917'
                d='M1475.691-508.659a2.921,2.921,0,0,0-2.675-2.845,2.874,2.874,0,0,0-2.672,2.8Z'
                transform='translate(-1470.344 511.504)'
                fill='#ffca05'
              />
            </g>
          </g>
          <g
            id='Group_5507'
            data-name='Group 5507'
            transform='translate(0 12.103)'
          >
            <g
              id='Group_5506'
              data-name='Group 5506'
              style='isolation: isolate'
            >
              <g
                id='Group_5499'
                data-name='Group 5499'
                transform='translate(13.231 1.884)'
              >
                <g id='Group_5498' data-name='Group 5498'>
                  <path
                    id='Path_9918'
                    data-name='Path 9918'
                    d='M1474.792-503.269l0,.753-7.373,5.453,0-.753Z'
                    transform='translate(-1467.417 503.269)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5501'
                data-name='Group 5501'
                transform='translate(2.106 7.338)'
              >
                <g id='Group_5500' data-name='Group 5500'>
                  <path
                    id='Path_9919'
                    data-name='Path 9919'
                    d='M1471.994-500.058l0,.753-11.125,4.627,0-.753Z'
                    transform='translate(-1460.866 500.058)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5503'
                data-name='Group 5503'
                transform='translate(0 10.749)'
              >
                <g id='Group_5502' data-name='Group 5502'>
                  <path
                    id='Path_9920'
                    data-name='Path 9920'
                    d='M1461.735-496.834l0,.753-2.106-1.216,0-.753Z'
                    transform='translate(-1459.627 498.05)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5505'
                data-name='Group 5505'
                transform='translate(0.002)'
              >
                <g id='Group_5504' data-name='Group 5504'>
                  <path
                    id='Path_9921'
                    data-name='Path 9921'
                    d='M1480.232-502.494l-7.373,5.454-11.125,4.627-2.106-1.216,7.95-6.46,9.391-4.289Z'
                    transform='translate(-1459.628 504.378)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5517'
            data-name='Group 5517'
            transform='translate(21.524 11.983)'
          >
            <g
              id='Group_5516'
              data-name='Group 5516'
              style='isolation: isolate'
            >
              <g
                id='Group_5509'
                data-name='Group 5509'
                transform='translate(0 1.884)'
              >
                <g id='Group_5508' data-name='Group 5508'>
                  <path
                    id='Path_9922'
                    data-name='Path 9922'
                    d='M1472.3-503.34l0,.753,7.374,5.453,0-.753Z'
                    transform='translate(-1472.299 503.34)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5511'
                data-name='Group 5511'
                transform='translate(7.374 7.338)'
              >
                <g id='Group_5510' data-name='Group 5510'>
                  <path
                    id='Path_9923'
                    data-name='Path 9923'
                    d='M1476.64-500.129l0,.753,11.125,4.627,0-.753Z'
                    transform='translate(-1476.64 500.129)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5513'
                data-name='Group 5513'
                transform='translate(18.498 10.749)'
              >
                <g id='Group_5512' data-name='Group 5512'>
                  <path
                    id='Path_9924'
                    data-name='Path 9924'
                    d='M1483.19-496.905l0,.753,2.106-1.216,0-.753Z'
                    transform='translate(-1483.19 498.121)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g id='Group_5515' data-name='Group 5515'>
                <g id='Group_5514' data-name='Group 5514'>
                  <path
                    id='Path_9925'
                    data-name='Path 9925'
                    d='M1472.3-502.565l7.374,5.453,11.125,4.627,2.106-1.216-7.95-6.46-9.391-4.289Z'
                    transform='translate(-1472.299 504.449)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
        </g>
        <g
          id='Group_5592'
          data-name='Group 5592'
          transform='translate(1537.381 -493.756)'
        >
          <g
            id='Group_5528'
            data-name='Group 5528'
            transform='translate(0.197 0.139)'
          >
            <g
              id='Group_5527'
              data-name='Group 5527'
              style='isolation: isolate'
            >
              <g
                id='Group_5520'
                data-name='Group 5520'
                transform='translate(0 1.215)'
              >
                <g id='Group_5519' data-name='Group 5519'>
                  <path
                    id='Path_9926'
                    data-name='Path 9926'
                    d='M1505.521-511.119l0,.753,7.951,6.462,0-.753Z'
                    transform='translate(-1505.521 511.119)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5522'
                data-name='Group 5522'
                transform='translate(7.952 7.676)'
              >
                <g id='Group_5521' data-name='Group 5521'>
                  <path
                    id='Path_9927'
                    data-name='Path 9927'
                    d='M1510.2-507.315l0,.753,9.391,4.289,0-.753Z'
                    transform='translate(-1510.202 507.315)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5524'
                data-name='Group 5524'
                transform='translate(17.343 10.081)'
              >
                <g id='Group_5523' data-name='Group 5523'>
                  <path
                    id='Path_9928'
                    data-name='Path 9928'
                    d='M1515.731-504.015l0,.753,3.264-1.884,0-.753Z'
                    transform='translate(-1515.731 505.899)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g id='Group_5526' data-name='Group 5526'>
                <g id='Group_5525' data-name='Group 5525'>
                  <path
                    id='Path_9929'
                    data-name='Path 9929'
                    d='M1505.521-510.619l7.952,6.461,9.391,4.289,3.264-1.884-7.373-5.453-11.129-4.627Z'
                    transform='translate(-1505.521 511.834)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5538'
            data-name='Group 5538'
            transform='translate(20.802 0.139)'
          >
            <g
              id='Group_5537'
              data-name='Group 5537'
              style='isolation: isolate'
            >
              <g
                id='Group_5530'
                data-name='Group 5530'
                transform='translate(12.654 1.215)'
              >
                <g id='Group_5529' data-name='Group 5529'>
                  <path
                    id='Path_9930'
                    data-name='Path 9930'
                    d='M1533.056-511.119l0,.753L1525.1-503.9l0-.753Z'
                    transform='translate(-1525.103 511.119)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5532'
                data-name='Group 5532'
                transform='translate(3.264 7.676)'
              >
                <g id='Group_5531' data-name='Group 5531'>
                  <path
                    id='Path_9931'
                    data-name='Path 9931'
                    d='M1528.967-507.315l0,.753-9.391,4.289,0-.753Z'
                    transform='translate(-1519.574 507.315)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5534'
                data-name='Group 5534'
                transform='translate(0 10.081)'
              >
                <g id='Group_5533' data-name='Group 5533'>
                  <path
                    id='Path_9932'
                    data-name='Path 9932'
                    d='M1520.918-504.015l0,.753-3.264-1.884,0-.753Z'
                    transform='translate(-1517.652 505.899)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5536'
                data-name='Group 5536'
                transform='translate(0.002)'
              >
                <g id='Group_5535' data-name='Group 5535'>
                  <path
                    id='Path_9933'
                    data-name='Path 9933'
                    d='M1538.259-510.619l-7.952,6.461-9.391,4.289-3.264-1.884,7.373-5.453,11.129-4.627Z'
                    transform='translate(-1517.653 511.834)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5539'
            data-name='Group 5539'
            transform='translate(16.861 11.719)'
          >
            <path
              id='Path_9934'
              data-name='Path 9934'
              d='M1522.634-500.8a6.678,6.678,0,0,1-6.042-.012c-1.636-.944-1.681-2.458-.129-3.422.036-.022.071-.044.11-.066s.071-.041.111-.06a6.7,6.7,0,0,1,5.931.071C1524.289-503.322,1524.3-501.76,1522.634-500.8Z'
              transform='translate(-1515.332 505.017)'
              fill='#d0d2d4'
            />
          </g>
          <g
            id='Group_5571'
            data-name='Group 5571'
            transform='translate(16.846)'
          >
            <g
              id='Group_5549'
              data-name='Group 5549'
              transform='translate(0 6.481)'
            >
              <g
                id='Group_5548'
                data-name='Group 5548'
                style='isolation: isolate'
              >
                <g
                  id='Group_5545'
                  data-name='Group 5545'
                  transform='translate(0 2.319)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5544'
                    data-name='Group 5544'
                    clipPath='url(#clip-path-12)'
                  >
                    <g
                      id='Group_5543'
                      data-name='Group 5543'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5540'
                        data-name='Group 5540'
                        transform='translate(7.616 0.022)'
                      >
                        <path
                          id='Path_9935'
                          data-name='Path 9935'
                          d='M1520.249-506.722l-.015,5.05a1.583,1.583,0,0,1-.426,1.041l.013-5.05a1.592,1.592,0,0,0,.428-1.041Z'
                          transform='translate(-1519.807 506.722)'
                          fill='#1f1f24'
                        />
                      </g>
                      <g
                        id='Group_5541'
                        data-name='Group 5541'
                        transform='translate(4.875 1.063)'
                      >
                        <path
                          id='Path_9936'
                          data-name='Path 9936'
                          d='M1520.948-506.109l-.014,5.05a2.962,2.962,0,0,1-.742.594,5.356,5.356,0,0,1-2,.632l.014-5.05a5.348,5.348,0,0,0,2-.632,2.969,2.969,0,0,0,.742-.594Z'
                          transform='translate(-1518.193 506.109)'
                          fill='#26272d'
                        />
                      </g>
                      <g id='Group_5542' data-name='Group 5542'>
                        <path
                          id='Path_9937'
                          data-name='Path 9937'
                          d='M1520.211-504.445l-.014,5.05a6.164,6.164,0,0,1-3.687-.632,2.011,2.011,0,0,1-1.187-1.658l.015-5.05a2.011,2.011,0,0,0,1.186,1.658,6.168,6.168,0,0,0,3.688.632Z'
                          transform='translate(-1515.323 506.735)'
                          fill='#1f1f24'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5547'
                  data-name='Group 5547'
                  transform='translate(0.015)'
                >
                  <g id='Group_5546' data-name='Group 5546'>
                    <path
                      id='Path_9939'
                      data-name='Path 9939'
                      d='M1522.186-507.417c1.578.91,1.587,2.383.019,3.293a6.267,6.267,0,0,1-5.687,0c-1.574-.909-1.583-2.385-.019-3.293A6.261,6.261,0,0,1,1522.186-507.417Z'
                      transform='translate(-1515.332 508.1)'
                      fill='#28292f'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5559'
              data-name='Group 5559'
              transform='translate(1.352 3.518)'
            >
              <g
                id='Group_5558'
                data-name='Group 5558'
                style='isolation: isolate'
              >
                <g
                  id='Group_5555'
                  data-name='Group 5555'
                  transform='translate(0 1.542)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5554'
                    data-name='Group 5554'
                    clipPath='url(#clip-path-13)'
                  >
                    <g
                      id='Group_5553'
                      data-name='Group 5553'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5550'
                        data-name='Group 5550'
                        transform='translate(5.063 0.014)'
                      >
                        <path
                          id='Path_9940'
                          data-name='Path 9940'
                          d='M1519.391-508.929l-.007,2.079a1.056,1.056,0,0,1-.284.693l.005-2.079a1.068,1.068,0,0,0,.285-.693Z'
                          transform='translate(-1519.1 508.929)'
                          fill='#1f1f24'
                        />
                      </g>
                      <g
                        id='Group_5551'
                        data-name='Group 5551'
                        transform='translate(3.239 0.707)'
                      >
                        <path
                          id='Path_9941'
                          data-name='Path 9941'
                          d='M1519.855-508.521l-.005,2.079a1.969,1.969,0,0,1-.5.4,3.488,3.488,0,0,1-1.328.419l.005-2.079a3.539,3.539,0,0,0,1.33-.42,1.93,1.93,0,0,0,.494-.4Z'
                          transform='translate(-1518.026 508.521)'
                          fill='#26272d'
                        />
                      </g>
                      <g id='Group_5552' data-name='Group 5552'>
                        <path
                          id='Path_9942'
                          data-name='Path 9942'
                          d='M1519.363-507.415l-.005,2.079a4.079,4.079,0,0,1-2.451-.42,1.336,1.336,0,0,1-.788-1.1l.005-2.079a1.336,1.336,0,0,0,.788,1.1,4.094,4.094,0,0,0,2.451.42Z'
                          transform='translate(-1516.119 508.937)'
                          fill='#1f1f24'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5557'
                  data-name='Group 5557'
                  transform='translate(0.005)'
                >
                  <g id='Group_5556' data-name='Group 5556'>
                    <path
                      id='Path_9944'
                      data-name='Path 9944'
                      d='M1520.681-509.392a1.157,1.157,0,0,1,.01,2.191,4.161,4.161,0,0,1-3.781,0,1.157,1.157,0,0,1-.012-2.189A4.166,4.166,0,0,1,1520.681-509.392Z'
                      transform='translate(-1516.122 509.845)'
                      fill='#28292f'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5569'
              data-name='Group 5569'
              transform='translate(1.352 1.291)'
            >
              <g
                id='Group_5568'
                data-name='Group 5568'
                style='isolation: isolate'
              >
                <g
                  id='Group_5565'
                  data-name='Group 5565'
                  transform='translate(0 1.542)'
                  style='isolation: isolate'
                >
                  <g
                    id='Group_5564'
                    data-name='Group 5564'
                    clipPath='url(#clip-path-14)'
                  >
                    <g
                      id='Group_5563'
                      data-name='Group 5563'
                      style='isolation: isolate'
                    >
                      <g
                        id='Group_5560'
                        data-name='Group 5560'
                        transform='translate(5.065 0.014)'
                      >
                        <path
                          id='Path_9945'
                          data-name='Path 9945'
                          d='M1519.388-510.24l0,1.187a1.057,1.057,0,0,1-.284.693l0-1.187a1.057,1.057,0,0,0,.284-.693Z'
                          transform='translate(-1519.101 510.24)'
                          fill='#ffa705'
                        />
                      </g>
                      <g
                        id='Group_5561'
                        data-name='Group 5561'
                        transform='translate(3.239 0.707)'
                      >
                        <path
                          id='Path_9946'
                          data-name='Path 9946'
                          d='M1519.855-509.832l0,1.187a1.965,1.965,0,0,1-.494.4,3.533,3.533,0,0,1-1.332.42l0-1.187a3.537,3.537,0,0,0,1.332-.421,1.961,1.961,0,0,0,.494-.394Z'
                          transform='translate(-1518.026 509.832)'
                          fill='#ffb806'
                        />
                      </g>
                      <g id='Group_5562' data-name='Group 5562'>
                        <path
                          id='Path_9947'
                          data-name='Path 9947'
                          d='M1519.361-508.726l0,1.187a4.073,4.073,0,0,1-2.449-.42,1.341,1.341,0,0,1-.79-1.1l0-1.189a1.341,1.341,0,0,0,.79,1.1,4.086,4.086,0,0,0,2.449.42Z'
                          transform='translate(-1516.119 510.248)'
                          fill='#ffa705'
                        />
                      </g>
                    </g>
                  </g>
                </g>
                <g
                  id='Group_5567'
                  data-name='Group 5567'
                  transform='translate(0.004)'
                >
                  <g id='Group_5566' data-name='Group 5566'>
                    <path
                      id='Path_9949'
                      data-name='Path 9949'
                      d='M1520.68-510.7a1.156,1.156,0,0,1,.012,2.189,4.161,4.161,0,0,1-3.781,0,1.157,1.157,0,0,1-.01-2.191A4.164,4.164,0,0,1,1520.68-510.7Z'
                      transform='translate(-1516.121 511.156)'
                      fill='#ffca05'
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id='Group_5570'
              data-name='Group 5570'
              transform='translate(1.357)'
            >
              <path
                id='Path_9950'
                data-name='Path 9950'
                d='M1521.469-509.069a2.923,2.923,0,0,0-2.675-2.847,2.876,2.876,0,0,0-2.672,2.8Z'
                transform='translate(-1516.122 511.916)'
                fill='#ffca05'
              />
            </g>
          </g>
          <g
            id='Group_5581'
            data-name='Group 5581'
            transform='translate(0 12.104)'
          >
            <g
              id='Group_5580'
              data-name='Group 5580'
              style='isolation: isolate'
            >
              <g
                id='Group_5573'
                data-name='Group 5573'
                transform='translate(13.231 1.884)'
              >
                <g id='Group_5572' data-name='Group 5572'>
                  <path
                    id='Path_9951'
                    data-name='Path 9951'
                    d='M1520.57-503.68l0,.753-7.373,5.454,0-.753Z'
                    transform='translate(-1513.194 503.68)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5575'
                data-name='Group 5575'
                transform='translate(2.106 7.338)'
              >
                <g id='Group_5574' data-name='Group 5574'>
                  <path
                    id='Path_9952'
                    data-name='Path 9952'
                    d='M1517.772-500.47l0,.753-11.125,4.627,0-.753Z'
                    transform='translate(-1506.645 500.47)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5577'
                data-name='Group 5577'
                transform='translate(0 10.749)'
              >
                <g id='Group_5576' data-name='Group 5576'>
                  <path
                    id='Path_9953'
                    data-name='Path 9953'
                    d='M1507.513-497.246l0,.753-2.106-1.216,0-.753Z'
                    transform='translate(-1505.405 498.461)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5579'
                data-name='Group 5579'
                transform='translate(0.002)'
              >
                <g id='Group_5578' data-name='Group 5578'>
                  <path
                    id='Path_9954'
                    data-name='Path 9954'
                    d='M1526.01-502.905l-7.373,5.454-11.125,4.627-2.106-1.216,7.95-6.46,9.391-4.289Z'
                    transform='translate(-1505.406 504.79)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
          <g
            id='Group_5591'
            data-name='Group 5591'
            transform='translate(21.524 11.984)'
          >
            <g
              id='Group_5590'
              data-name='Group 5590'
              style='isolation: isolate'
            >
              <g
                id='Group_5583'
                data-name='Group 5583'
                transform='translate(0 1.884)'
              >
                <g id='Group_5582' data-name='Group 5582'>
                  <path
                    id='Path_9955'
                    data-name='Path 9955'
                    d='M1518.077-503.751l0,.753,7.373,5.453,0-.753Z'
                    transform='translate(-1518.077 503.751)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5585'
                data-name='Group 5585'
                transform='translate(7.373 7.338)'
              >
                <g id='Group_5584' data-name='Group 5584'>
                  <path
                    id='Path_9956'
                    data-name='Path 9956'
                    d='M1522.418-500.541l0,.753,11.125,4.627,0-.753Z'
                    transform='translate(-1522.418 500.541)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g
                id='Group_5587'
                data-name='Group 5587'
                transform='translate(18.498 10.749)'
              >
                <g id='Group_5586' data-name='Group 5586'>
                  <path
                    id='Path_9957'
                    data-name='Path 9957'
                    d='M1528.968-497.316l0,.753,2.106-1.216,0-.753Z'
                    transform='translate(-1528.968 498.532)'
                    fill='#bcc0d2'
                  />
                </g>
              </g>
              <g id='Group_5589' data-name='Group 5589'>
                <g id='Group_5588' data-name='Group 5588'>
                  <path
                    id='Path_9958'
                    data-name='Path 9958'
                    d='M1518.077-502.976l7.373,5.454,11.125,4.627,2.106-1.216-7.95-6.46-9.391-4.289Z'
                    transform='translate(-1518.077 504.861)'
                    fill='#fff'
                  />
                </g>
              </g>
            </g>
          </g>
        </g>
        <g
          id='Group_5594'
          data-name='Group 5594'
          transform='translate(1499.654 -523.679)'
        >
          <g id='Group_5593' data-name='Group 5593'>
            <path
              id='Path_9959'
              data-name='Path 9959'
              d='M1503.2-529.533a20,20,0,0,0-20,20H1523.2A20,20,0,0,0,1503.2-529.533Z'
              transform='translate(-1483.193 529.533)'
              fill='#28292f'
            />
          </g>
        </g>
        <g
          id='Group_5595'
          data-name='Group 5595'
          transform='translate(1505.622 -517.043)'
        >
          <path
            id='Path_9960'
            data-name='Path 9960'
            d='M1491.383-521.4c-.912,1.486-2.568,2.128-3.7,1.435s-1.308-2.459-.4-3.946,2.566-2.13,3.7-1.437S1492.294-522.886,1491.383-521.4Z'
            transform='translate(-1486.707 525.626)'
            fill='#38393f'
          />
        </g>
      </g>
    </svg>
  )
}

export default Svg11
