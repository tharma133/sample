import React from 'react'

// xmlns:xlink='http://www.w3.org/1999/xlink'
function Svg12() {
  return (
    <svg
      xmlns='http://www.w3.org/2000/svg'
      width='1692.988'
      height='1050.272'
      viewBox='0 0 1692.988 1050.272'
    >
      <defs>
        <filter
          id='Path_6442'
          x='0'
          y='51.296'
          width='1692.988'
          height='998.976'
          filterUnits='userSpaceOnUse'
        >
          <feOffset dx='10' dy='25' input='SourceAlpha' />
          <feGaussianBlur stdDeviation='7.5' result='blur' />
          <feFlood floodOpacity='0.161' />
          <feComposite operator='in' in2='blur' />
          <feComposite in='SourceGraphic' />
        </filter>
      </defs>
      <g
        id='Group_3661'
        data-name='Group 3661'
        transform='translate(-194.481 -418.496)'
      >
        <g
          id='Group_3657'
          data-name='Group 3657'
          transform='translate(206.981 418.496)'
        >
          <g id='Group_3604' data-name='Group 3604'>
            <g id='Group_3603' data-name='Group 3603'>
              <g
                transform='matrix(1, 0, 0, 1, -12.5, 0)'
                filter='url(#Path_6442)'
              >
                <path
                  id='Path_6442-2'
                  data-name='Path 6442'
                  d='M282.277,565.91l474-273.655c37.484-21.635,98.249-21.635,135.734,0l937,540.981,64.956,9.163v51.3c0,14.186-9.366,28.372-28.1,39.181l-482.184,278.389c-37.484,21.652-98.249,21.652-135.734,0L274.1,649.023c-18.733-10.826-28.117-25.012-28.117-39.2v-51.3Z'
                  transform='translate(-233.48 -224.73)'
                  fill='#222344'
                />
              </g>
              <g
                id='Group_3602'
                data-name='Group 3602'
                transform='translate(0.004)'
              >
                <path
                  id='Path_6443'
                  data-name='Path 6443'
                  d='M274.094,646l973.853,562.243c37.484,21.652,98.249,21.652,135.733,0L1865.864,929.87c37.484-21.652,37.484-56.744,0-78.379L892.011,289.232c-37.484-21.635-98.249-21.635-135.734,0L274.094,567.621C236.61,589.273,236.61,624.347,274.094,646Z'
                  transform='translate(-245.981 -273.005)'
                  fill='#686a9a'
                />
                <path
                  id='Path_6444'
                  data-name='Path 6444'
                  d='M1234.789,1154.793c15.526,8.96,40.793,8.96,56.319,0L1773.293,876.4c15.51-8.96,15.51-23.536-.017-32.512L799.44,281.649c-15.527-8.976-40.794-8.976-56.32,0L260.936,560.021c-15.526,8.96-15.526,23.553,0,32.513Z'
                  transform='translate(-193.117 -242.482)'
                  fill='#fff'
                />
                <path
                  id='Path_6445'
                  data-name='Path 6445'
                  d='M1228.221,1128.076c4.582,2.647,12.031,2.647,16.612.017L1727.017,849.7c4.565-2.647,4.565-6.94-.017-9.587L753.164,277.857c-4.581-2.647-12.031-2.647-16.613,0L254.368,556.246c-4.564,2.647-4.564,6.94.017,9.587Z'
                  transform='translate(-166.694 -227.235)'
                  fill='rgba(131,210,222,0.8)'
                />
              </g>
            </g>
          </g>
          <g
            id='Group_3656'
            data-name='Group 3656'
            transform='translate(178.443 97.065)'
          >
            <path
              id='Path_6542'
              data-name='Path 6542'
              d='M256.5,501.577l386-222.851c.628-.373,887.737,511.794,887.737,511.794l1.917,1.1L1146.151,1014.49C1145.506,1014.864,256.5,501.577,256.5,501.577ZM643.13,281.288,261.587,501.577l883.919,510.335,381.542-220.288Z'
              transform='translate(-256.497 -278.726)'
              fill='#fff'
            />
            <path
              id='Path_6543'
              data-name='Path 6543'
              d='M298.53,323.939l55.81-32.223L481.1,364.9l-55.81,32.224Zm55.81-29.288-50.72,29.288,121.666,70.234,50.72-29.271Z'
              transform='translate(414.717 -71.292)'
              fill='#fff'
            />
            <g
              id='Group_3654'
              data-name='Group 3654'
              transform='translate(928.105 535.466)'
            >
              <path
                id='Path_6544'
                data-name='Path 6544'
                d='M327.655,324.739l25.029-14.457L370.06,320.31l-25.029,14.458Zm25.029-11.5-19.939,11.5,12.285,7.093,19.921-11.522Z'
                transform='translate(-48.298 -310.282)'
                fill='#fff'
              />
              <path
                id='Path_6545'
                data-name='Path 6545'
                d='M319.423,329.475l25.029-14.44,17.376,10.028L336.8,339.521Zm25.029-11.5-19.922,11.5,12.268,7.093,19.939-11.5Z'
                transform='translate(-179.753 -234.383)'
                fill='#fff'
              />
              <path
                id='Path_6546'
                data-name='Path 6546'
                d='M311.192,334.244l25.029-14.458L353.6,329.815l-25.029,14.458Zm25.029-11.5-19.938,11.5,12.285,7.093,19.921-11.522Z'
                transform='translate(-311.192 -158.499)'
                fill='#fff'
              />
            </g>
            <path
              id='Path_6547'
              data-name='Path 6547'
              d='M281.282,319.752l25.028-14.457,48.531,28.015-25.029,14.457Zm25.028-11.505L286.39,319.752l43.423,25.063,19.922-11.5Z'
              transform='translate(139.288 145.548)'
              fill='#fff'
            />
            <path
              id='Path_6548'
              data-name='Path 6548'
              d='M300.1,302.773l13.8-7.958,48.531,28.015-13.8,7.958Zm13.8-5.022-8.688,5.022,43.423,25.08,8.688-5.022Z'
              transform='translate(439.724 -21.805)'
              fill='#fff'
            />
            <g
              id='Group_3655'
              data-name='Group 3655'
              transform='translate(25.809 14.525)'
            >
              <path
                id='Path_6549'
                data-name='Path 6549'
                d='M274.481,294.039l25.029-14.457,17.359,10.029L291.84,304.068Zm25.029-11.521-19.938,11.521,12.268,7.076,19.938-11.505Z'
                transform='translate(4.875 -279.582)'
                fill='#fff'
              />
              <path
                id='Path_6550'
                data-name='Path 6550'
                d='M266.249,298.776l25.029-14.441,17.376,10.028L283.625,308.8Zm25.029-11.505L271.34,298.776l12.285,7.093,19.921-11.505Z'
                transform='translate(-126.579 -203.683)'
                fill='#fff'
              />
              <path
                id='Path_6551'
                data-name='Path 6551'
                d='M258.018,303.544l25.029-14.457,17.376,10.028-25.046,14.457Zm25.029-11.521-19.939,11.521,12.268,7.076,19.939-11.5Z'
                transform='translate(-258.018 -127.799)'
                fill='#fff'
              />
            </g>
            <path
              id='Path_6552'
              data-name='Path 6552'
              d='M321.242,311.241l68.964,39.817,8.072-4.661L329.314,306.58Z'
              transform='translate(777.4 166.069)'
              fill='#fff'
            />
            <path
              id='Path_6553'
              data-name='Path 6553'
              d='M321.5,312.43l49.264,28.442,8.074-4.661L329.576,307.77Z'
              transform='translate(781.566 185.072)'
              fill='#fff'
            />
            <path
              id='Path_6554'
              data-name='Path 6554'
              d='M320.6,312.951l49.264,28.443,8.072-4.661L328.674,308.29Z'
              transform='translate(767.194 193.369)'
              fill='#fff'
            />
            <path
              id='Path_6555'
              data-name='Path 6555'
              d='M288.432,292.3l68.963,39.817,8.074-4.661L296.5,287.637Z'
              transform='translate(253.462 -136.427)'
              fill='#fff'
            />
            <path
              id='Path_6556'
              data-name='Path 6556'
              d='M285.521,291.658,388.6,351.167l8.072-4.661L293.593,287Z'
              transform='translate(206.988 -146.662)'
              fill='#fff'
            />
            <path
              id='Path_6557'
              data-name='Path 6557'
              d='M287.792,294.007l49.264,28.443,8.074-4.661-49.264-28.443Z'
              transform='translate(243.254 -109.127)'
              fill='#fff'
            />
          </g>
        </g>
      </g>
    </svg>
  )
}

export default Svg12
