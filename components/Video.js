import React from 'react'
import styles from '../styles/Work.module.css'
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'

function Video() {
  return (
    <div className={styles.work_top_content}>
      <button className={styles.play_circle}>
        <PlayCircleFilledIcon />
      </button>
      <div className={styles.play_btn}>
        <div>
          <button className={styles.play_arrow}>
            <PlayArrowIcon />
          </button>
        </div>
        <div className={styles.line}></div>
      </div>
    </div>
  )
}

export default Video
