/* eslint-disable @next/next/no-img-element */
import React from 'react'

function Loading() {
  return (
    <div className='loading'>
      <div className='loading_container'>
       <img src='/svg/box.svg' alt='box' />
      </div>
    </div>
  )
}

export default Loading
