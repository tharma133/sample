import Navbar from './Navbar'
import AppProvider from '../context/context'
import Links from './Links'

const Layout = ({ children, indexValue }) => {
  return (
    <AppProvider>
      <div className='container'>
        <Navbar />
        <Links indexValue={indexValue} />
        {children}
      </div>
    </AppProvider>
  )
}

export default Layout
