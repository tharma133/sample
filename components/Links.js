/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState } from 'react'
import Link from 'next/link'
import { links } from '../utils/data'
import styles from '../styles/Home.module.css'
import { useGlobalContext } from '../context/context'
import { useEffect } from 'react'
import AOS from 'aos'
import 'aos/dist/aos.css'
// import withRouter from 'next/dist/client/with-router'

function Links({ indexValue }) {
  const { handleNavigate } = useGlobalContext()
  const [value, setValue] = useState(0)

  useEffect(() => {
    setTimeout(() => {
      AOS.init({ duration: 500, easing: 'linear' })
    }, 500)
    // window.addEventListener('load', () => {
    //   AOS.refresh()
    // })
    // const handleScroll = () => {
    // window.addEventListener('scroll', () => {
    //   if (
    //     homeRef.current.getBoundingClientRect().bottom >=
    //     window.innerHeight / 2
    //   ) {
    //     setValue(0)
    //   }

    // if (
    //   workRef.current.getBoundingClientRect().top <=
    //   window.innerHeight / 2
    // ) {
    //   setValue(1)
    // }
    // })
    // }
    // handleScroll()
    // return () => window.removeEventListener('scroll', handleScroll)
    setValue(indexValue)
  }, [])

  return (
    <div className={styles.right_nav_links}>
      <div
        className={styles.nav_links}
        data-aos='fade-left'
        data-aos-anchor='#example-anchor'
        data-aos-offset='500'
        data-aos-duration='1000'
      >
        {links.map((links, index) => {
          const { link, url } = links
          return (
            <button
              key={index}
              className={`${value === index && styles.link_active}`}
              onClick={handleNavigate}
            >
              {link}
            </button>
          )
        })}
      </div>
    </div>
  )
}

export default Links
