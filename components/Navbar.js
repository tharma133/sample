/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/jsx-no-comment-textnodes */
import React, { useEffect } from 'react'
import styles from '../styles/Home.module.css'
import Link from 'next/link'
import { social_links } from '../utils/data'
import { useGlobalContext } from '../context/context'
import AOS from 'aos'
import 'aos/dist/aos.css'
import CloseIcon from '@material-ui/icons/Close'
const Navbar = () => {
  const { nav_links, path, closePageLink } = useGlobalContext()

  useEffect(() => {
    setTimeout(() => {
      AOS.init({ duration: 500, easing: 'linear' })
    }, 500)
  }, [])

  return (
    <nav className={styles.nav}>
      <div
        className={styles.navbar}
        data-aos='fade-down'
        data-aos-easing='linear'
        data-aos-duration='1000'
      >
        <img src='/svg/svg1.svg' alt='logo' />
        <div className={styles.right_links} ref={nav_links}>
          {path !== '/' ? (
            <button className={styles.quotes_btn} onClick={closePageLink}>
              <CloseIcon />{' '}
            </button>
          ) : (
            <button className={styles.quotes_btn}>get quote</button>
          )}
          <ul className={styles.links}>
            {social_links.map((links) => {
              const { id, link, url } = links
              return (
                <li key={id}>
                  <Link href={url}>
                    <a>{link}</a>
                  </Link>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
