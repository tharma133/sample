/* eslint-disable @next/next/no-img-element */
import Link from 'next/link'
import styles from '../styles/Work.module.css'
import Footer from './Footer'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import AOS from 'aos'
import 'aos/dist/aos.css'
import { useEffect } from 'react'
import Video from './Video'
import PageLinks from './PageLinks'
import { work_links } from '../utils/data'

const index = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useEffect(() => {
    // setTimeout(() => {
    AOS.init({ duration: 500, easing: 'linear' })
    // }, 500)
  }, [])

  return (
    <section id='work' className={styles.work}>
      {/* <div> */}
      <div className={styles.work_container}>
        <div
          className={styles.work_left}
          data-aos-delay='500'
          data-aos='zoom-in'
        >
          <Video />
          <div
            className={styles.work_bottom_content}
            data-aos='fade-up'
            data-aos-anchor-placement='bottom bottom'
            data-aos-delay='1000'
          >
            <div className={styles.work_text}>
              <h2>Work</h2>
              <h4>
                In this day and age with Zettabytes of data generated every day,
                skipping Digital transformation is no longer an option
              </h4>
            </div>
          </div>
        </div>
        <div className='page_links'>
          {work_links.map((link, index) => {
            return (
              <div key={index} data-aos='flip-up' data-aos-delay='500'>
                <Link href={link.url}>
                  <a>{link.link}</a>
                </Link>
              </div>
            )
          })}
        </div>
        <div className='svg_14'>
          <img src='/svg/svg14.svg' alt='mobile' />
        </div>
      </div>
      <div className={styles.brands}>
        <h2 data-aos='fade-up'>brands worked with</h2>
        <button className={styles.left_btn}>
          <ChevronLeftIcon />
        </button>
        <button className={styles.right_btn}>
          <ChevronRightIcon />
        </button>
        <div className={styles.brand_container} data-aos='fade-up'>
          <div className={styles.brand_1}>
            <div>
              <img src='/svg/face.svg' alt='brand' />
            </div>
          </div>
          <div className={styles.brand_2}>
            <div>
              <img src='/svg/face.svg' alt='brand' />
            </div>
          </div>
          <div className={styles.brand_3}>
            <div>
              <img src='/svg/faceBorder.svg' alt='brand' />
            </div>
          </div>
          <div className={styles.brand_4}>
            <div>
              <img src='/svg/face.svg' alt='brand' />
            </div>
          </div>
          <div className={styles.brand_5}>
            <div>
              <img src='/svg/face.svg' alt='brand' />
            </div>
          </div>
        </div>
      </div>
      <div className={styles.testmonials}>
        <h2 data-aos='fade-up'>testmonials</h2>
        <p data-aos='fade-up'>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iste,
          laborum?
        </p>
        <button className={styles.left_btn}>
          <ChevronLeftIcon />
        </button>
        <button className={styles.right_btn}>
          <ChevronRightIcon />
        </button>
        <div className={styles.testmonials_container} data-aos='fade-up'>
          <div
            className={styles.testmonials_content_1}
            data-aos='flip-left'
            data-aos-delay='500'
          >
            <h3>ahmed elsayed</h3>
            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Hic
              repellat beatae provident voluptatem atque temporibus cum dolore
              molestias sed error.
            </p>
            <div>
              <img src='/svg/face.svg' alt='brand' />
            </div>
          </div>
          <div
            className={styles.testmonials_content_2}
            data-aos='flip-left'
            data-aos-delay='500'
          >
            <h3>ahmed elsayed</h3>
            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Hic
              repellat beatae provident voluptatem atque temporibus cum dolore
              molestias sed error.
            </p>
            <div>
              <img src='/svg/face.svg' alt='brand' />
            </div>
          </div>
          <div
            className={styles.testmonials_content_3}
            data-aos='flip-left'
            data-aos-delay='500'
          >
            <h3>ahmed elsayed</h3>
            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Hic
              repellat beatae provident voluptatem atque temporibus cum dolore
              molestias sed error.
            </p>
            <div>
              <img src='/svg/face.svg' alt='brand' />
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </section>
  )
}

export default index
