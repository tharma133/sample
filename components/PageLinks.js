import React from 'react'
import Link from 'next/link'
import { useGlobalContext } from '../context/context'

function PageLinks({ links }) {
  const { handlePageLink } = useGlobalContext()

  return (
    <div className='page_links'>
      {links.map((link, index) => {
        return (
          <div
            key={index}
            data-aos='flip-up'
            data-aos-delay='500'
            onClick={handlePageLink}
          >
            {/* <Link href={link.url}> */}
            <p>{link.link}</p>
            {/* </Link> */}
          </div>
        )
      })}
    </div>
  )
}

export default PageLinks
