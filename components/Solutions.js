/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react'
import styles from '../styles/Solutions.module.css'
import { solutions_content, solutions_links } from '../utils/data'
import PageLinks from './PageLinks'
import AOS from 'aos'
import 'aos/dist/aos.css'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'

function Solutions() {
  const [show, setShow] = useState(0)

  useEffect(() => {
    // setTimeout(() => {
    AOS.init({ duration: 500, easing: 'linear' })
    // }, 500)
  }, [])

  return (
    <section className={styles.solutions}>
      <div className={styles.solutions_container}>
        <div
          className={styles.solutions_left}
          data-aos='fade-right'
          data-aos-delay='500'
        >
          <div className={styles.solutions_left_content}>
            <h2>Build Emotional connection to the buyers with the property.</h2>
            <h2>
              Immersive experience of the property will create potential buyers
              for the real estate companies.
            </h2>
            <div className={styles.solutions_btn}>
              <button>Know More...</button>
            </div>
          </div>
          <div className={styles.solutions_left_title}>
            <h1 data-aos='flip-up' data-aos-delay='1000'>
              Automation
            </h1>
          </div>
        </div>
        <PageLinks links={solutions_links} />
      </div>
      <div className={styles.solutions1_container}>
        <h1>solutions</h1>
        <div
          className={styles.solutions1_left}
          data-aos='fade-right'
          data-aos-delay='500'
        >
          <div className={styles.solutions1_left_content}>
            {solutions_content.map((text, index) => {
              return (
                <div key={index} className={styles.solutions1_left_text}>
                  <div
                    className={styles.solutions1_header}
                    onClick={() => setShow(index)}
                  >
                    <button>
                      <ChevronRightIcon />{' '}
                    </button>
                    <h3>{text.title}</h3>
                  </div>
                  <div
                    className={`${styles.solutions1_content} ${
                      show === index && styles.solutions1_content_show
                    }`}
                  >
                    <img src={text.image} alt='image' />
                    <h3>{text.text}</h3>
                  </div>
                </div>
              )
            })}
          </div>
          <div className={styles.solutions1_btn}>
            <button>Know more</button>
          </div>
        </div>
        <div className={styles.page_links}>
          {solutions_links.map((link, index) => {
            return (
              <div key={index} data-aos='flip-up' data-aos-delay='500'>
                {/* <Link href={link.url}> */}
                <button>
                  <ChevronRightIcon />{' '}
                </button>
                <p>{link.link}</p>
                {/* </Link> */}
              </div>
            )
          })}
        </div>
      </div>
    </section>
  )
}

export default Solutions
