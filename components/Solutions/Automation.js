import React from 'react'
import { useGlobalContext } from '../../context/context'
import styles from '../../styles/Solutions.module.css'

function Automation() {
  const { pageLink } = useGlobalContext()

  return (
    <section
      className={`${
        pageLink === 'automation' ? styles.automation_show : styles.automation
      }`}
    >
      <div className={styles.automation_container}>
        <h1>Automation</h1>
        <article className={styles.automation_article}>
          <div className={styles.automation_article_text}>
            <p>
              Virtual reality in Real estate enables the buyers to walk through
              the property virtually allowing them to take a closer look at the
              interior and exterior architecture even before the completion of
              construction.
            </p>
            <br />
            <p>
              Virtual Walkthrough in real estate opens up the bridge between
              imagination and execution. Through VR headset real estate
              professionals can showcase their collection of properties to
              depict a clearcut visualization to the clients
            </p>
            <br />
            <p>
              Typically clients go through multiple properties before deciding
              the final choice. So this VR Real estate method has opportunity in
              every vertical of architecture and construction allowing users to
              go one further in competition.
            </p>
            <div className={styles.automation_btns}>
              <button className={styles.see_work}>see work</button>
              <button className={styles.contact_us}>contact us</button>
            </div>
          </div>
        </article>
      </div>
    </section>
  )
}

export default Automation
