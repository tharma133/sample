import { useState, createContext, useContext, useRef } from 'react'
import Loading from '../components/Loading'
import { useRouter } from 'next/router'

const AppContext = createContext()

const AppProvider = ({ children }) => {
  const history = useRouter()
  const router = useRouter()
  const path = router.pathname
  const [pageLink, setPageLink] = useState('')

  const [loading, setLoading] = useState(false)

  const handleNavigate = (e) => {
    setLoading(true)
    setTimeout(() => {
      if (e.target.textContent === 'home') {
        history.push('/')
        setLoading(false)
        return
      }
      history.push(`/${e.target.textContent}`)
    }, 3000)
  }
  const handlePageLink = (e) => {
    console.log(e.target.childNodes[0])
    setPageLink(e.target.childNodes[0].textContent)
  }

  const closePageLink = (e) => {
    setPageLink('')
  }

  if (loading) {
    return <Loading />
  }

  return (
    <AppContext.Provider
      value={{
        loading,
        setLoading,
        handleNavigate,
        handlePageLink,
        pageLink,
        path,
        closePageLink,
      }}
    >
      {children}
    </AppContext.Provider>
  )
}
export const useGlobalContext = () => {
  return useContext(AppContext)
}
export default AppProvider
