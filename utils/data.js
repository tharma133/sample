import FacebookIcon from '@material-ui/icons/Facebook'
import TwitterIcon from '@material-ui/icons/Twitter'
import InstagramIcon from '@material-ui/icons/Instagram'

export const links = [
  {
    url: '/',
    link: 'home',
  },
  {
    url: '/work',
    link: 'work',
  },
  {
    url: '/solutions',
    link: 'solutions',
  },
  {
    url: '/about',
    link: 'about',
  },
  {
    url: '/news',
    link: 'news',
  },
  {
    url: '/contact',
    link: 'contact',
  },
]

export const social_links = [
  {
    id: '1',
    url: '/',
    link: <FacebookIcon />,
  },
  {
    id: '2',
    url: '/',
    link: <TwitterIcon />,
  },
  {
    id: '3',
    url: '/',
    link: <InstagramIcon />,
  },
]

export const work_links = [
  {
    url: '/work/manufacturing',
    link: 'manufacturing',
  },
  {
    url: '/work/smart-city',
    link: 'smart city',
  },
  {
    url: '/work/technology',
    link: 'technology',
  },
  {
    url: '/work/education',
    link: 'education',
  },
  {
    url: '/work/retail',
    link: 'retail',
  },
  {
    url: '/work/aviation',
    link: 'aviation',
  },
  {
    url: '/work/others',
    link: 'others',
  },
]

export const solutions_links = [
  {
    url: '/solutions/automation',
    link: 'automation',
  },
  {
    url: '/solutions/immersive',
    link: 'immersive',
  },
  {
    url: '/solutions/analytics',
    link: 'analytics',
  },
  {
    url: '/solutions/enterprises',
    link: 'enterprises',
  },
  {
    url: '/solutions/branding',
    link: 'branding',
  },
  {
    url: '/solutions/consultation',
    link: 'consultation',
  },
]

export const solutions_content = [
  {
    title: 'industrial automation',
    text: ' Lorem ipsum, dolor sit amet consectetur adipisicing elit. Illocommodi saepe libero eum? Ullam esse consectetur quis adquisquam adipisci.',
    image: '/images/robot.jpg',
  },
  {
    title: 'artificial intelligence',
    text: 'Enhancing the student learing and engagement through virtual reality. Ensuring the students to envision the innovative ideas.',
    image: '/images/vr.jpg',
  },
]
