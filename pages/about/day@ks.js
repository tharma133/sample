import Layout from '../../components/Layout'
import Meta from '../../components/Meta'

const day = () => {
  return (
    <Layout>
      <Meta title='About/Day@KS' />
      <h1>Day@KS</h1>
    </Layout>
  )
}

export default day
