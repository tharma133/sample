import Meta from '../../components/Meta'
import Layout from '../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='About/Career' />
      <h1>Career</h1>
    </Layout>
  )
}

export default career
