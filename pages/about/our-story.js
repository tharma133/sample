import Meta from '../../components/Meta'
import Layout from '../../components/Layout'

const ourStory = () => {
  return (
    <Layout>
      <Meta title='About/Our Story' />
      <h1>Our Story</h1>
    </Layout>
  )
}

export default ourStory
