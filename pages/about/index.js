import Meta from '../../components/Meta'
import Link from 'next/link'
import Layout from '../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='About' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/about/career'>
              <a>Career</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/about/day@ks'>
              <a>Day@KS </a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/about/our-story'>
              <a>Our Story</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/about/team'>
              <a>Team</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
