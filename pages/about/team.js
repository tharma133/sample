import Meta from '../../components/Meta'
import Layout from '../../components/Layout'

const team = () => {
  return (
    <Layout>
      <Meta title='About/Team' />
      <h1>Team</h1>
    </Layout>
  )
}

export default team
