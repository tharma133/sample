import Layout from '../components/Layout'
import Main from '../components/Main'
import Meta from '../components/Meta'

export default function Home() {
  return (
    <Layout indexValue={0}>
      <Meta title='KS Smart' />
      <Main />
    </Layout>
  )
}
