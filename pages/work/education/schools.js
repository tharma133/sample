import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Education/Schools' />
      <h1>Schools</h1>
    </Layout>
  )
}

export default career
