import Meta from '../../../components/Meta'
// import Link from 'next/link'
import Layout from '../../../components/Layout'
import Education from '../../../components/Work/Education'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Education' />
      <Education />
      {/* <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/education/museum'>
              <a>Museum</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/education/schools'>
              <a>Schools</a>
            </Link>
          </div>
        </div>
      </div> */}
    </Layout>
  )
}

export default index
