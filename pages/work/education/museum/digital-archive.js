import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Education/Museum/Digital Archive' />
      <h1>Digital Archive</h1>
    </Layout>
  )
}

export default career
