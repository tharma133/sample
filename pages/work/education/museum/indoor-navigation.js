import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Education/Museum/Indoor Navigation' />
      <h1>Indoor Navigation</h1>
    </Layout>
  )
}

export default career
