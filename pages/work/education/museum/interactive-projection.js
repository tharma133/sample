import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Education/Museum/Interactive Projection' />
      <h1>Interactive Projection</h1>
    </Layout>
  )
}

export default career
