import Meta from '../../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Education/Museum' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/education/museum/digital-archive'>
              <a>Digital Archive</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/education/museum/indoor-navigation'>
              <a>Indoor Navigation</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/education/museum/interactive-projection'>
              <a>Interactive Projection</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/education/museum/multitaction'>
              <a>Multitaction</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/education/museum/video-wall'>
              <a>Video Wall</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/education/museum/virtual-walkthrough'>
              <a>Virtual Walkthrough</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
