import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Education/Museum/Virtual Walkthrough' />
      <h1>Virtual Walkthrough</h1>
    </Layout>
  )
}

export default career
