import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Education/Museum/Video Wall' />
      <h1>Video Wall</h1>
    </Layout>
  )
}

export default career
