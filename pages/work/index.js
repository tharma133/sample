import React from 'react'
import Layout from '../../components/Layout'
import Work from '../../components/Work'
import Meta from '../../components/Meta'
import Education from '../../components/Work/Education'

function index() {
  return (
    <Layout indexValue={1}>
      <Meta title='Work' />
      <Work />
      <Education />
    </Layout>
  )
}

export default index
