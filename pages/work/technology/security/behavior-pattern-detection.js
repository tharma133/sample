import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Technology/Security/Behavior Pattern Detection' />
      <h1>Behavior Pattern Detection</h1>
    </Layout>
  )
}

export default career
