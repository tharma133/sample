import Meta from '../../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Technology/Security' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/technology/security/biometric-facial-recognition'>
              <a>Biometric Facial Recognition</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/technology/security/behavior-pattern-detection'>
              <a>Behavior Pattern Detection</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/technology/security/smart-security-system'>
              <a>Smart Security System</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
