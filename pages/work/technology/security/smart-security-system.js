import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Technology/Security/Smart Security System' />
      <h1>Smart Security System</h1>
    </Layout>
  )
}

export default career
