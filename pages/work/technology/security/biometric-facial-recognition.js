import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Technology/Security/Biometric Facial Recognition' />
      <h1>Biometric Facial Recognition</h1>
    </Layout>
  )
}

export default career
