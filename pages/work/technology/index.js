import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Technology' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/technology/corporate-IT'>
              <a>Corporate IT</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/technology/security'>
              <a>Security</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
