import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Technology/Corporate IT/Industrial Paramter Forecasting' />
      <h1>Industrial Paramter Forecasting</h1>
    </Layout>
  )
}

export default career
