import Meta from '../../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Technology/Corporate IT' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/technology/corporate-IT/HRMS'>
              <a>HRMS</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/technology/corporate-IT/industrial-parameter-forecasting'>
              <a>Industrial Parameter Forecasting</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
