import Meta from '../../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Smart City/Eco Park' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/smart-city/eco-park/immersive-projection'>
              <a>Immerse Projection</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/smart-city/eco-park/touch&through'>
              <a>Touch & Through</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
