import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Smart City/Eco Park/Immersive Projection' />
      <h1>Immersive Projection</h1>
    </Layout>
  )
}

export default career
