import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Smart City/Energy Monitoring System' />
      <h1>Energy Monitoring System</h1>
    </Layout>
  )
}

export default career
