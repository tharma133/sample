import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Smart City' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/smart-city/eco-park'>
              <a>Eco Park</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/smart-city/automatic-number-plate-recognition'>
              <a>Automatic Number Plate Recognition</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/smart-city/construction-virtual-walkthrough'>
              <a>Construction Virtual Walkthrough</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/smart-city/energy-monitoring-system'>
              <a>Energy Monitoring System</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
