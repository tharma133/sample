import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Smart City/Automatic Number Plate Recognition' />
      <h1>Automatic Number Plate Recognition</h1>
    </Layout>
  )
}

export default career
