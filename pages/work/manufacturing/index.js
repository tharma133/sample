import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Manufacturing' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/manufacturing/factory-virtual-walkthrough'>
              <a>Factory Virtual Walkthrough</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
