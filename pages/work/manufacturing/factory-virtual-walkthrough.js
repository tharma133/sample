import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Manufaturing/Factory Virtual Walkthrough' />
      <h1>Factory Virtual Walkthrough</h1>
    </Layout>
  )
}

export default career
