import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Others/Web & App/Project Tracker' />
      <h1>Project Tracker</h1>
    </Layout>
  )
}

export default career
