import Meta from '../../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Others/Web & App' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/others/web&app/aavin-mobile-app'>
              <a>Aavin Mobile App</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/others/web&app/mopm-mobile-app'>
              <a>MOPM Mobile App</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/others/web&app/museum-management-app'>
              <a>Museum Management App</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/others/web&app/project-tracker'>
              <a>Project Tracker</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
