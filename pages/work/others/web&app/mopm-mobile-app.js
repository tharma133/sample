import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Others/Web & App/MOPM Mobile App' />
      <h1>MOPM Mobile App</h1>
    </Layout>
  )
}

export default career
