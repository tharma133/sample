import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Others/Business Branding' />
      <h1>Business Branding</h1>
    </Layout>
  )
}

export default career
