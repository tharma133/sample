import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Others' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/others/business-branding'>
              <a>Business Branding</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/others/chennai-metro-water'>
              <a>Chennai Metro Water</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/others/digital-marketing'>
              <a>Digital Marketing</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/others/web&app'>
              <a>Web & app</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
