import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Retail' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/retail/dairy'>
              <a>Dairy</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
