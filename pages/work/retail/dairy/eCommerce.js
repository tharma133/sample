import Meta from '../../../../components/Meta'
import Layout from '../../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Retail/Dairy/E-Commerce' />
      <h1>E-Commerce</h1>
    </Layout>
  )
}

export default career
