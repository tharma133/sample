import Meta from '../../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Work/Retail/Dairy' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/work/retail/dairy/eCommerce'>
              <a>E-Commerce</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/work/retail/dairy/ERP'>
              <a>ERP</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
