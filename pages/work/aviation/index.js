import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Work/Aviation' />
      <h1>Aviation</h1>
    </Layout>
  )
}

export default career
