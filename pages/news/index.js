import Meta from '../../components/Meta'
import Link from 'next/link'
import Layout from '../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='News' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/news/blog'>
              <a>Blog</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/news/press-release'>
              <a>Press Release</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/news/media'>
              <a>Media</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
