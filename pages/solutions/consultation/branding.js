import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Consultation/Branding' />
      <h1>Branding</h1>
    </Layout>
  )
}

export default career
