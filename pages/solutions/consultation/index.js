import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Solutions/Consultation' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/solutions/consultation/branding'>
              <a>Branding</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/consultation/technology'>
              <a>Technology</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
