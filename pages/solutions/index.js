import Meta from '../../components/Meta'
import Layout from '../../components/Layout'
import Solutions from '../../components/Solutions'
import Automation from '../../components/Solutions/Automation'

const index = () => {
  return (
    <Layout indexValue={2}>
      <Meta title='Solutions' />
      <Solutions />
      {/* <div>
        <div className='content'>
          <div className='box'>
            <Link href='/solutions/analytics'>
              <a>Analytics</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/automation'>
              <a>Automation</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/branding-solutions'>
              <a>Branding Solutions</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/consultation'>
              <a>Consultation</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/enterprise-solutions'>
              <a>Enterprise Solutions</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/immersive-solutions'>
              <a>Immersive solutions</a>
            </Link>
          </div>
        </div>
      </div> */}
    </Layout>
  )
}

export default index
