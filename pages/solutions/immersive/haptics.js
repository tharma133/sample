import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Immersive Solutions/Haptics' />
      <h1>Haptics</h1>
    </Layout>
  )
}

export default career
