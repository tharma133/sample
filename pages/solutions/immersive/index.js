import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Solutions/Immersive Solutions' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/solutions/immersive-solutions/360'>
              <a>ERP</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/immersive-solutions/AR'>
              <a>AR</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/immersive-solutions/VR'>
              <a>VR</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/immersive-solutions/haptics'>
              <a>Haptics</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
