import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Immersive Solutions/VR' />
      <h1>VR</h1>
    </Layout>
  )
}

export default career
