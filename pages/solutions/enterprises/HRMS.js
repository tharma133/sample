import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Enterprise Solutions/HRMS' />
      <h1>HRMS</h1>
    </Layout>
  )
}

export default career
