import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Enterprise Solutions/Smart Security' />
      <h1>Smart Security</h1>
    </Layout>
  )
}

export default career
