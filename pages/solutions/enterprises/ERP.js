import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Enterprise Solutions/ERP' />
      <h1>ERP</h1>
    </Layout>
  )
}

export default career
