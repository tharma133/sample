import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Enterprise Solutions/E-commerce' />
      <h1>E-Commerce</h1>
    </Layout>
  )
}

export default career
