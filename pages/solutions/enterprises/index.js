import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Solutions/Enterprise Solutions' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/solutions/enterprise-solutions/ERP'>
              <a>ERP</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/enterprise-solutions/digital-archive'>
              <a>Digital Archive</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/enterprise-solutions/E-commerce'>
              <a>E-Commerce</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/enterprise-solutions/HRMS'>
              <a>HRMS</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/enterprise-solutions/smart-security'>
              <a>Smart Security</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
