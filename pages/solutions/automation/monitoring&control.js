import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Automation/Monitoring-control' />
      <h1>Monitoring & Control</h1>
    </Layout>
  )
}

export default career
