import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Automation/robotics' />
      <h1>Robotics</h1>
    </Layout>
  )
}

export default career
