import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Automation/AL-ML' />
      <h1>AL / ML</h1>
    </Layout>
  )
}

export default career
