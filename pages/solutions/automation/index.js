import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Solutions/Automation' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/solutions/automation/industrial-automation'>
              <a>Industrial Automation</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/automation/AL-ML'>
              <a>AL/ML</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/automation/robotics'>
              <a>Robotics</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/automation/monitoring&control'>
              <a>Monitoring & Control</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
