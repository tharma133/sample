import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Automation/Industrial-Automation' />
      <h1>Industrial Automation</h1>
    </Layout>
  )
}

export default career
