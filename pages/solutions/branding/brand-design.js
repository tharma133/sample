import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Branding Solutions/Brand Design' />
      <h1>Brand Design</h1>
    </Layout>
  )
}

export default career
