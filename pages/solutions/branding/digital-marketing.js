import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Branding Solutions/Digital Marketing' />
      <h1>Digital Marketing</h1>
    </Layout>
  )
}

export default career
