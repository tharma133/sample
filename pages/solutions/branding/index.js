import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Solutions/Branding Solutions' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/solutions/branding-solutions/brand-design'>
              <a>Brand Design</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/branding-solutions/digital-marketing'>
              <a>Digital Marketing</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/branding-solutions/web-app'>
              <a>Web App</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
