import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Analytics/Retail-Analysis' />
      <h1>Retail Analysis</h1>
    </Layout>
  )
}

export default career
