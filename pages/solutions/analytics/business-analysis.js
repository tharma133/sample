import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Analytics/business-Analysis' />
      <h1>Business Analysis</h1>
    </Layout>
  )
}

export default career
