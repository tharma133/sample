import Meta from '../../../components/Meta'
import Layout from '../../../components/Layout'

const career = () => {
  return (
    <Layout>
      <Meta title='Solutions/Analytics/Video-Analysis' />
      <h1>Video Analysis</h1>
    </Layout>
  )
}

export default career
