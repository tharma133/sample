import Meta from '../../../components/Meta'
import Link from 'next/link'
import Layout from '../../../components/Layout'
const index = () => {
  return (
    <Layout>
      <Meta title='Solutions/Analytics' />
      <div>
        <div className='content'>
          <div className='box'>
            <Link href='/solutions/analytics/video-analysis'>
              <a>Video Analysis</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/analytics/retail-analysis'>
              <a>Retail Analysis</a>
            </Link>
          </div>
          <div className='box'>
            <Link href='/solutions/analytics/business-analysis'>
              <a>Business Analysis</a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default index
